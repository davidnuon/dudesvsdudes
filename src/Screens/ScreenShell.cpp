#include "ScreenShell.h"

ScreenShell::ScreenShell(){
	world       = NULL;
	
	config      = NULL;
	
	ti          = NULL;
	duringScene = NULL;
	to          = NULL;
	
	transIn     = true;
	transOut    = false;

	pathToConfig = "";
}

ScreenShell::~ScreenShell(){
	if( world       != NULL ){
		delete world;
		world = NULL;
	}
	
	if( ti          != NULL ){
		delete ti;
		ti = NULL;
	}

	if( duringScene != NULL ){
		delete duringScene;
		duringScene = NULL;
	}

	if( to          != NULL ){
		delete to;
		to = NULL;
	}

}

bool ScreenShell::init(){
	world = new World(this);
	
	config = world->cf.getConfig(pathToConfig);
	if( config == NULL ){
		return false;
	}

	ti = new Cutscene( world );
	if( ti->init( config->getString("transIn") ) == false ){
		return false;
	}

	to = new Cutscene( world );
	if( to->init( config->getString("transOut") ) == false ){
		return false;
	}

	duringScene = new Cutscene( world );
	if( duringScene->init( config->getString("duringScene") ) == false ){
		return false;
	}

	transIn     = true;
	transOut    = false;

	return true;
}

void ScreenShell::update(){
	if( isActive() ){
		handleEvents();
		if( !transIn && !transOut ){
			duringScene->update();
		}
		if( transIn ){
			if( ti->update() == true ){
				transInEnd();
			}
		}
		else if( transOut ){
			if( to->update() == true ){
				transOutEnd();
			}
		}
		world->update();
	}
}

void ScreenShell::render(){
	if( isActive() && transTo == NONE || transOut == true ){
		if( !transIn && !transOut ){
			duringScene->render();
		}
		if( transIn && !transOut ){
			ti->render();
		}
		else if( transOut ){
			to->render();
		}
		world->render();
	}
}

void ScreenShell::setActive(bool value){
	if( value ){
		transIn = true;
		transTo = NONE;

		ti->start();
		duringScene->start();
	}
	else
		transOut = false;
	Screen::setActive(value);
}

void ScreenShell::transInEnd(){
	transIn = false;
	ti->start();
	duringScene->start();
	world->mp.playMusic( config->getString("music") );
}

void ScreenShell::transOutEnd(){
	subject.push_event( transTo );
	to->start();
	transOut = false;
	subject.notifyObservers();
}