#include "BestiaryScreen.h"

BestiaryScreen::BestiaryScreen(){
	index = 0;
}

BestiaryScreen::~BestiaryScreen(){}

bool BestiaryScreen::init(){
	pathToConfig = "res/Configs/BestiaryScreen.config";
	if( ScreenShell::init() == false ){
		return false;
	}

	portraits = world->af.getIterPtr("res/Animations/charSel.anim");
	desc = world->af.getIterPtr("res/Animations/bestiary_info.anim");
	specials = world->af.getIterPtr("res/Animations/specials.anim");
	startingOffset = config->getInt("startingOffset");
	offset = startingOffset;
	return true;
}

void BestiaryScreen::handleEvents(){
	while( eventHeap->getSize() > 0 ){
		Event event = eventHeap->pop();
		switch( event.getType() ){
			case KEY_ENTER:
				if( !transIn ){
					transOut = true;
					transTo.setEvent(RESET);
				}
				break;
			case MOUSE_BUTTON_LEFT:
				if( portraits->getLoc() <= 0 ){
					index = portraits->getSize() - 1;
				}
				else
					index -= 1;
				portraits->switchAnim( index );
				desc->switchAnim( index );
				specials->switchAnim( index );
				offset = -1 * 171 * index + startingOffset;
				break;
			case MOUSE_BUTTON_RIGHT:
				if( portraits->getLoc() + 1 >= portraits->getSize() ){
					index = 0;
				}
				else
					index += 1;
				portraits->switchAnim( index );
				desc->switchAnim( index );
				specials->switchAnim( index );
				offset = -1 * 171 * index + startingOffset;
				break;
		}
	}
	subject.notifyObservers();
}

void BestiaryScreen::update(){
	handleEvents();
	if( !transIn && !transOut ){
		duringScene->update();
		portraits->update();
		desc->update();
		specials->update();
	}
	if( transIn ){
		if( ti->update() == true ){
			transInEnd();
		}
	}
	else if( transOut ){
		if( to->update() == true ){
			transOutEnd();
		}
	}
	world->update();
}

void BestiaryScreen::render(){
	if( isActive() && transTo == NONE || transOut == true ){
		if( !transIn && !transOut ){
			duringScene->render();
			portraits->draw( world, offset , 0 );
			desc->draw( world );
			specials->draw( world, 410, 25 );
		}
		if( transIn && !transOut ){
			ti->render();
		}
		else if( transOut ){
			to->render();
		}
		world->render();
	}
}