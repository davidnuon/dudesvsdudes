#ifndef STORYSCREEN_H
#define STORYSCREEN_H

#include "ScreenShell.h"

class StoryScreen: public ScreenShell{
protected:
	virtual void handleEvents();
	AnimIter iter;
public:
	virtual ~StoryScreen();
	virtual bool init();
	virtual void update();
	virtual void render();
};

#endif