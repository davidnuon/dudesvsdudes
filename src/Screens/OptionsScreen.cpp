#include "OptionsScreen.h"

OptionsScreen::~OptionsScreen(){}

bool OptionsScreen::init(){
	scrolling = true;
	pathToConfig = "res/Configs/OptionsScreen.config";
	Constants::setMaxPan( 300, 300, 300, 300 );
	//Constants::setPan( 100, 100 );
	return ScreenShell::init();
}

void OptionsScreen::handleEvents(){
	if( isActive() ){
		while( eventHeap->getSize() > 0 ){
			Event event = eventHeap->pop();
			switch( event.getType() ){
				case KEY_ENTER:
					if( !transIn ){
						transOut = true;
						transTo.setEvent( RESET );
					}
					break;
			}
		}
		subject.notifyObservers();
	}
}