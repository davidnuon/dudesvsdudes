#ifndef SCREENMANAGER_H
#define SCREENMANAGER_H

#include "Screen.h"
#include <stack>
#include "../System/Events/Observer.h"
#include "TitleScreen.h"
//#include "GameScreen.h"

#include "PauseScreen.h"
#include "../System/Events/Subject.h"
#include "../System/Events/Subject.h"

#include "TitleScreen.h"
//#include "../../Screens/GameScreen.h"
#include "PauseScreen.h"
#include "CharacterSelect.h"
#include "OptionsScreen.h"
#include "HelpScreen.h"
#include "BestiaryScreen.h"
#include "GameOverScreen.h"
#include "VictoryScreen.h"
#include "StoryScreen.h"

class Factory;
class ScreenManager: public Observer{
private:
	Factory *factory;
	//Stores all screens currently in use in a stack.
	std::stack<Screen*> screenStack;

	//Pushes a screen onto the stack.
	void push_stack(Screen* screen);

	//Pops a screen off the top of the stack.
	void pop_stack();

	//Clears the screen stack.
	void wipeStack();

	//Handles events at the beginning of update.
	void handleEvents();

	void panScreen();
public:
	Subject subject;
	
	ScreenManager();

	//Set up this constructor for easier initialization.
	//ScreenManager( Screen* firstScreen );
	virtual ~ScreenManager();
	void update();
	void render();

	//Inherited virtual function from Observer. Used for event messaging.
	void notify(Event event);
};

#endif