#include "GameTestScreen.h"

#include "../System/Events/KeyInput.h"
#include "../System/Events/MouseInput.h"
#include "../System/Events/Subject.h"
#include "../System/Heap.h"

#include "../GameObjects/Tile.h"
#include "../GameObjects/Board.h"
#include "../GameObjects/GameObjectFactory.h"
//#include "../Graphics/TextSheet.h"
#include "../Graphics/Drawing/LTexture.h"

#include "../Components/Graphics_Components/Square.h"
#include "../Components/Graphics_Components/GraphState.h"
#include "../GameObjects/Unit.h"

#include "../Player/PlayerHuman.h"
#include "../Player/PlayerComputer.h"

#include <iostream>
#include <sstream>
#include <string>

GameTestScreen::~GameTestScreen(){
	KeyInput::subject.removeObserver(this);
	MouseInput::subject.removeObserver(worldBoard);
	if( worldBoard != NULL )
		delete worldBoard;
	if( bq != NULL )
		delete bq;
}

void GameTestScreen::cleanup(){
	//GameObjectFactory::cleanUp();
}

bool GameTestScreen::init(){
	healthPtr = NULL;
	stevePtr = NULL;
	//squadIndexPtr = NULL;
	scrolling = true;
	KeyInput::subject.addObserver(this);
	//KeyInput::subject.addObserver(this, Event(KEY_p));

	worldBoard = new Board(this);
	std::string path = "res/Configs/Board.config";
	if(worldBoard->init(path) == false)
		return false;

	pathToConfig = "res/Configs/GameTestScreen.config";

	config = worldBoard->cf.getConfig(pathToConfig);
	if( config == NULL ){
		return false;
	}

	ti = new Cutscene( worldBoard );
	if( ti->init( config->getString("transIn") ) == false ){
		return false;
	}

	duringScene = new Cutscene( worldBoard );
	if( duringScene->init( config->getString("duringScene") ) == false ){
		return false;
	}

	if( numberSheet.load( config->getString("numberSheet") ) == false ){
		return false;
	}

	//Setting up the AQState for drawing euforia to the screen.
	euforiaState.x = config->getInt("EuforiaX");
	euforiaState.y = config->getInt("EuforiaY");
	euforiaState.scale = config->getInt("TextScale");
	euforiaState.color.r = config->getInt("TextColorR");
	euforiaState.color.g = config->getInt("TextColorG");
	euforiaState.color.b = config->getInt("TextColorB");
	euforiaState.scale = 1.5;
	euforiaState.layer = 4;
	euforiaState.priority = 50;
	euforiaState.drawAbsolute = true;

	//Setting up the AQState for drawing gold values to the screen.
	goldState = euforiaState;
	goldState.scale = 1.1;
	
	goldState.color.r = 51;
	goldState.color.g = 51;
	goldState.color.b = 51;
	goldState.x = config->getInt("GoldX");
	goldState.y = config->getInt("GoldY");

	teamState = euforiaState;
	teamState.scale = 1.1;
	teamState.x = config->getInt("TeamStateX");
	teamState.y = config->getInt("TeamStateY");

	unitCount = euforiaState;
	unitCount.x = config->getInt("UnitCountX");
	unitCount.y = config->getInt("UnitCountY");

	squadIndexState = euforiaState;
	squadIndexState.x = config->getInt("SquadIndexStateX");
	squadIndexState.y = config->getInt("SquadIndexStateY");

	transIn     = true;
	transOut    = false;

	MouseInput::subject.addObserver(worldBoard);

	//GameObjectFactory::init(worldBoard);

	baseComputer = worldBoard->UnitFactory.makeBase(1,Point(0,8));
	baseHuman = worldBoard->UnitFactory.makeBase(2,Point(worldBoard->getColumns()-1,worldBoard->getRows()/2));

	healthPtr = static_cast<Unit*>( baseHuman.getPtr() )->getHealth();
	stevePtr = static_cast<Unit*>( baseComputer.getPtr() )->getHealth();
	//squadIndexPtr = static_cast<PlayerHuman>( human ).getSelectedSquad();
	//squadIndexPtr = static_cast<PlayerHuman>

	static_cast<Unit*>( baseHuman.getPtr() )->setHealth(1500);

	human = new PlayerHuman(worldBoard, Constants::PLAYER_TEAM);
	human->init();
		

	computer = new PlayerComputer(worldBoard, Constants::ENEMY_TEAM);
	computer->init();
	
	bq = new Basic_Quad;

	AQState state;
	state.w = Constants::getMaxPan().w + Constants::SCREEN_WIDTH;
	state.h = Constants::getMaxPan().h + Constants::SCREEN_HEIGHT;
	state.color.setColor( config->getInt("backgroundR"), config->getInt("backgroundG"), config->getInt("backgroundB") );
	bq->applyState( state );

	return true;
}

void GameTestScreen::update(){
	if( isActive() ){
		handleEvents();
		if( !transIn && !transOut ){
			//duringScene->update();
			gameLoop();
		}
		if( transIn ){
			if( ti->update() == true ){
				transInEnd();
			}
		}
		worldBoard->update();
	}
}

void GameTestScreen::render(){
	// UI is one image, lawl

	if( isActive() && transTo == NONE || transOut == true ){
		worldBoard->af.draw( "res/Animations/game_screen.anim", 0, 0, 0);
		if( !transIn && !transOut && baseHuman.getPtr() != NULL && baseComputer.getPtr() != NULL){
			bq->pushToDeck( 0, worldBoard );
			//duringScene->render();
			numberSheet.draw( worldBoard, euforiaState, StringManip::itoa( *healthPtr ) );
			numberSheet.draw( worldBoard, goldState, StringManip::itoa( human->getGold() ) );
			numberSheet.draw( worldBoard, teamState, StringManip::itoa( *stevePtr ) );
			numberSheet.draw( worldBoard, unitCount, StringManip::itoa( human->getUnitCount() ) );
			//numberSheet.draw( worldBoard, squadIndexState, StringManip::itoa( *squadIndexPtr ) );
		}
		if( transIn && !transOut ){
			ti->render();
		}
		worldBoard->render();
	}
}

void GameTestScreen::notify(Event event){
	if( isActive() ){
		eventHeap->push( event );
	}
}

void GameTestScreen::handleEvents(){

	while( eventHeap->getSize() > 0 ){

		Event event = eventHeap->pop();

		if(event.getType() == 27){
			subject.push_event(Event(RESET));
		}
		
		//if(event.getType() == KEY_p)
			//worldBoard->getCostGrid().print();
			//std::cout << "Computer: " << computer->getUnitCount() << "\n";

		if( event.getType() == KEY_ENTER){
			if( !transIn )
				subject.push_event( START_SCREEN_GAMEOVER );
			subject.notifyObservers();
		}

		if( event == MOUSE_BUTTON_LEFT ){
			int x, y;
			MouseInput::getMouseState( x, y );
			worldBoard->af.draw( config->getString("onClick"), 0, x, y);
		}

		if( event == MOUSE_BUTTON_RIGHT ){
			int x, y;
			MouseInput::getMouseState( x, y );
			worldBoard->af.draw( config->getString("onClick"), 1, x, y);
		}
		if( event == GAME_TEST_SCREEN ){
			human->setCharacter( event.data );
		}
	}
	subject.notifyObservers();
}

void GameTestScreen::gameLoop(){

	human->superUpdate();
	computer->superUpdate();

	human->update();

	computer->update();

	GameObject *base = baseComputer.getPtr();
	if(base == NULL) {
		Event event = START_SCREEN_VICTORY;
		event.data = human->getCharacter();
		subject.push_event( event );
		subject.notifyObservers();
		return;
	}
	base = baseHuman.getPtr();
	if(base == NULL) {
		subject.push_event(Event(START_SCREEN_GAMEOVER));
		subject.notifyObservers();
		return;
	}
}

void GameTestScreen::transInEnd(){
	transIn = false;
	ti->start();
	duringScene->start();
	worldBoard->mp.playMusic( config->getString("music") );
	Constants::setPan(Constants::SCREEN_WIDTH, 0);
}

void GameTestScreen::setActive( bool value ){
	if( value ){
		transIn = true;
		transTo = NONE;

		ti->start();
		duringScene->start();
	}
	else
		transOut = false;
	Screen::setActive(value);
}