#include "CharacterSelect.h"
#include "../System/World.h"

CharacterSelect::CharacterSelect(){
	world       = NULL;
	config      = NULL;
	transIn     = true;
	transOut    = false;
	ti          = NULL;
	to          = NULL;
	loc         = 0;
	startChecks = false;
	chose       = false;
	channel     = 0;
}

CharacterSelect::~CharacterSelect(){}

bool CharacterSelect::init(){
	channel     = 0;
	transIn     = true;
	transOut    = false;
	ti          = NULL;
	duringScene = NULL;

	world = new World(this);

	config = world->cf.getConfig("res/Configs/CharacterSelect.config");

	to = new Cutscene(world);
	if( to->init(config->getString("transOut")) == false ){
		return false;
	}

	duringScene = new Cutscene(world);
	if( duringScene->init(config->getString("duringScene")) == false ){
		return false;
	}

	hoverAnim = world->af.getIterPtr( config->getString("selection") );
	if( hoverAnim == NULL ){
		return false;
	}
	hoverAnim->switchAnim( 10 );
	finalLocation = 0;

	return true;
}

void CharacterSelect::handleEvents(){
	if( isActive() ){
		while( eventHeap->getSize() > 0 ){
			Event event = eventHeap->pop();
			int mouseX = 0, temp = 0;
			switch( event.getType() ){
				case MOUSE_MOTION:
					if( chose == false ){
						MouseInput::getMouseState( mouseX, temp );
						temp = mouseX / (Constants::SCREEN_WIDTH / 6.0);
						if( temp != loc ){
							if( loc < 0 ){
								loc = 0;
							}
							else if( loc > 5 ){
								loc = 5;
							}					
							loc = temp;
							startChecks = true;
						}
					}
					break;
				case MOUSE_BUTTON_LEFT:
					if( chose == false ){
						finalLocation = loc;
						chose = true;
						hoverAnim->reset();
						startChecks = true;
						channel = world->mp.playSFX( config->getString("VoiceHover" + StringManip::itoa(loc) ) );
					}
					break;
			}
		}
		subject.notifyObservers();
	}
}

void CharacterSelect::update(){
	if( isActive() ){
		handleEvents();
		if( transOut ){
			if( to->update() == true ){
				transOutEnd();
			}
		}
		else{
			if( hoverAnim->update() == true && chose && startChecks && Mix_Playing( channel ) == false ){
				transOut = true;
				transTo = GAME_TEST_SCREEN;
				transTo.data = finalLocation;
			}
			duringScene->update();
		}
		world->update();
	}
}

void CharacterSelect::render(){
	if( isActive() && transTo == NONE || transOut == true ){
		if( transOut ){
			to->render();
		}
		else{
			if( startChecks )
				hoverAnim->draw( world, loc*Constants::SCREEN_WIDTH / 6.0, 0 );
			duringScene->render();
		}
		world->render();
	}
}

void CharacterSelect::setActive( bool value ){
	if( value ){
		transTo = NONE;
		duringScene->start();
		world->mp.playMusic( config->getString("music") );
	}
	else
		transOut = false;
	Screen::setActive(value);
}

void CharacterSelect::transInEnd(){}