#include "VictoryScreen.h"

bool VictoryScreen::init(){
	player = 0;
	playerConfig = NULL;
	pathToConfig = "res/Configs/VictoryScreen.config";
	if( ScreenShell::init() == false ){
		return false;
	}

	playerConfig = world->cf.getConfig("res/Configs/Player.config");
	if( playerConfig == NULL ){
		return false;
	}

	return true;
}

void VictoryScreen::handleEvents(){
	if( isActive() ){
		while( eventHeap->getSize() > 0 ){
			Event event = eventHeap->pop();
			switch( event.getType() ){
				case KEY_ENTER:
				case MOUSE_BUTTON_LEFT:
				case MOUSE_BUTTON_RIGHT:
					if( !transIn ){
						transOut = true;
						transTo.setEvent(RESET);
					}
					break;
				case START_SCREEN_VICTORY:
					player = event.data;
					playVictory();
					break;
			}
		}
		subject.notifyObservers();
	}
}

void VictoryScreen::render(){
	ScreenShell::render();
}

void VictoryScreen::playVictory(){
	switch(player){
		case 0:
			playVoice("JackVictoryCount", "JackVictory");
			break;
		case 1:
			playVoice("MagnusVictoryCount", "MagnusVictory");
			break;
		case 2:
			playVoice("JolieVictoryCount", "JolieVictory");
			break;
		case 3:
			playVoice("KarlVictoryCount", "KarlVictory");
			break;
		case 4:
			playVoice("RobertVictoryCount", "RobertVictory");
			break;
		case 5:
			playVoice("ShamusVictoryCount", "ShamusVictory");
			break;
	}
}

void VictoryScreen::playVoice( std::string counter, std::string moveName ){
	world->mp.playSFX( playerConfig->getString( moveName + StringManip::itoa( rand() % playerConfig->getInt(counter) ) ) );
}