#ifndef CHARACTERSELECT_H
#define CHARACTERSELECT_H

#include "ScreenShell.h"
#include "../System/Factory/ConfigFactory.h"
#include "../Graphics/Animation/Cutscene.h"
#include "../System/Events/MouseInput.h"

class World;
class CharacterSelect: public ScreenShell{
protected:
	virtual void handleEvents();
	AnimIter* hoverAnim;
	int loc;
	int finalLocation;
	bool chose;
	bool startChecks;
	int channel;
public:
	CharacterSelect();
	virtual ~CharacterSelect();
	virtual bool init();
	virtual void update();
	virtual void render();
	virtual void transInEnd();
	virtual void setActive( bool value );
};

#endif