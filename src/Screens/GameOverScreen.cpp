#include "GameOverScreen.h"

bool GameOverScreen::init(){
	pathToConfig = "res/Configs/GameOverScreen.config";
	if( ScreenShell::init() == false ){
		return false;
	}
	Config *playerConfig = world->cf.getConfig("res/Configs/Player.config");
	world->mp.playSFX( playerConfig->getString( "SteveVictory" + StringManip::itoa( rand() % playerConfig->getInt("SteveVictoryCount") ) ) );
	return true;
}

void GameOverScreen::handleEvents(){
	if( isActive() ){
		while( eventHeap->getSize() > 0 ){
			Event event = eventHeap->pop();
			switch( event.getType() ){
				case KEY_ENTER:
				case MOUSE_BUTTON_LEFT:
				case MOUSE_BUTTON_RIGHT:
					if( !transIn ){
						transOut = true;
						transTo.setEvent(RESET);
					}
					break;
			}
		}
		subject.notifyObservers();
	}
}