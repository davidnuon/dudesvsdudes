#ifndef BESTIARYSCREEN_H
#define BESTIARYSCREEN_H

#include "ScreenShell.h"

class BestiaryScreen: public ScreenShell{
protected:
	virtual void handleEvents();
	int index;
	int offset;
	int startingOffset;
	AnimIter* portraits;
	AnimIter* desc;
	AnimIter* specials;
public:
	BestiaryScreen();
	virtual ~BestiaryScreen();
	virtual bool init();
	virtual void update();
	virtual void render();
};

#endif