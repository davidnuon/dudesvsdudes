#ifndef HELPSCREEN_H
#define HELPSCREEN_H

#include "ScreenShell.h"

class HelpScreen: public ScreenShell{
protected:
	virtual void handleEvents();
public:
	virtual ~HelpScreen();
	virtual bool init();
};

#endif