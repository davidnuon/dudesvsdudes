#include "ScreenManager.h"
#include "../System/Factory/Factory.h"

ScreenManager::ScreenManager(){
	factory = NULL;
	factory = new Factory;
	
	push_stack( Factory::getScreenSafe<TitleScreen>("title", this) );

	Event tempEvent( TRANSITION_IN );
	screenStack.top()->notify( tempEvent );
}

ScreenManager::~ScreenManager(){
	wipeStack();
	delete factory;
}

void ScreenManager::push_stack(Screen *screen){
	if(screenStack.empty() == false){
		screenStack.top()->setActive(false);
	}
	screenStack.push(screen);
	screenStack.top()->subject.addObserver(this);
	screenStack.top()->setActive(true);
}

void ScreenManager::pop_stack(){
	//Removes from the top of the stack.
	screenStack.top()->subject.removeObserver(this);
	screenStack.top()->setActive(false);
	if( screenStack.top()->isScrolling() ){
		Constants::resetPan();
		Constants::resetMaxPan();
	}
	
	Factory::destroy( screenStack.top() );
	screenStack.pop();

	//Handles if the stack is empty.
	if(screenStack.empty() == false)
		screenStack.top()->setActive(true);
}

void ScreenManager::wipeStack(){
	while(screenStack.empty() == false){
		pop_stack();
	}
}

void ScreenManager::update(){
	handleEvents();
	if( screenStack.size() > 0 )
		screenStack.top()->update();
}

void ScreenManager::render(){

	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();

	glScalef( Constants::screenScale.x, Constants::screenScale.y, 1.f );

	LFRect panned =  Constants::getPanned();
	glTranslatef( panned.x, panned.y, 1.f );

	if(screenStack.size() > 0){
		screenStack.top()->render();
	}

	SDL_GL_SwapBuffers();
}

void ScreenManager::notify(Event event){
	eventHeap->push(event);
}

void ScreenManager::handleEvents(){

	while( eventHeap->getSize() > 0 ){
		Event event = eventHeap->pop();
		Event tempEvent;
		switch(event.getType()){
			case KILL_SCREEN:
				pop_stack();
				if(screenStack.empty())
					push_stack( Factory::getScreenSafe<TitleScreen>("title", this) );
				break;
			case START_SCREEN_CHARSEL:
				pop_stack();
				push_stack( Factory::getScreenSafe<CharacterSelect>("charSel", this) );
				break;
			case START_SCREEN_OPTIONS:
				pop_stack();
				push_stack( Factory::getScreenSafe<OptionsScreen>("options", this) );
				break;
			case START_SCREEN_HELP:
				pop_stack();
				push_stack( Factory::getScreenSafe<HelpScreen>("help", this) );
				break;
			case START_SCREEN_BESTIARY:
				pop_stack();
				push_stack( Factory::getScreenSafe<BestiaryScreen>("bestiary", this) );
				break;
			case START_SCREEN_GAMEOVER:
				pop_stack();
				push_stack( Factory::getScreenSafe<GameOverScreen>("GameOver", this) );
				break;
			case START_SCREEN_VICTORY:
				pop_stack();
				push_stack( Factory::getScreenSafe<VictoryScreen>("Victory", this) );
				screenStack.top()->notify( event );
				break;
			case START_SCREEN_STORY:
				pop_stack();
				push_stack( Factory::getScreenSafe<StoryScreen>("Story", this) );
				break;
			case START_SCREEN_PAUSE:
				pop_stack();
				push_stack( Factory::getScreenSafe<PauseScreen>("title", this) );
				break;
			case GAME_TEST_SCREEN:
				pop_stack();
				push_stack( Factory::getScreenSafe<GameTestScreen>("GameTestScreen", this) );
				screenStack.top()->notify( event );
				break;
			/*
			case START_SCREEN_GAME_OVER:
				push_stack(new GameOverScreen);
				break;
			*/
			case RESET:
				wipeStack();
				push_stack( Factory::getScreenSafe<TitleScreen>("title", this) );

				break;
		}

	}
	if( screenStack.top()->isScrolling() ){
		panScreen();
	}
	subject.notifyObservers();
}

void ScreenManager::panScreen(){
	if( Constants::isPressed(KEY_d) == true ){
		Constants::movePan(0);
	}
	if( Constants::isPressed(KEY_w) == true ){
		Constants::movePan(1);
	}
	if( Constants::isPressed(KEY_a) == true ){
		Constants::movePan(2);
	}
	if( Constants::isPressed(KEY_s) == true ){
		Constants::movePan(3);
	}
}