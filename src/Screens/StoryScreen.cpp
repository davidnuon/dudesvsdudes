#include "StoryScreen.h"

StoryScreen::~StoryScreen(){}

bool StoryScreen::init(){
	scrolling = true;
	pathToConfig = "res/Configs/StoryScreen.config";
	//Constants::setMaxPan( 300, 300, 300, 300 );
	//Constants::setPan( 100, 100 );

	if( ScreenShell::init() == false ){
		return false;
	}

	iter = world->af.getIter( config->getString("slideShow") );

	return true;
}

void StoryScreen::handleEvents(){
	if( isActive() ){
		while( eventHeap->getSize() > 0 ){
			Event event = eventHeap->pop();
			switch( event.getType() ){
				case KEY_ENTER:
					if( !transIn ){
						transOut = true;
						transTo.setEvent( RESET );
					}
					break;
				case MOUSE_BUTTON_LEFT:
					if( iter.getLoc() <= 0 ){
						iter.switchAnim( iter.getSize() - 1 );
					}
					iter.switchAnim( iter.getLoc() - 1 );
					break;
				case MOUSE_BUTTON_RIGHT:
					if( iter.getLoc() + 1 >= iter.getSize() ){
						iter.switchAnim( 0 );
					}
					iter.switchAnim( iter.getLoc() + 1 );
					break;
			}
		}
		subject.notifyObservers();
	}
}

void StoryScreen::update(){
	if( isActive() ){
		handleEvents();
		if( !transIn && !transOut ){
			duringScene->update();
			iter.update();
		}
		if( transIn ){
			if( ti->update() == true ){
				transInEnd();
			}
		}
		else if( transOut ){
			if( to->update() == true ){
				transOutEnd();
			}
		}
		world->update();
	}
}

void StoryScreen::render(){
	if( isActive() && transTo == NONE || transOut == true ){
		if( !transIn && !transOut ){
			duringScene->render();
			iter.draw(world);
		}
		if( transIn && !transOut ){
			ti->render();
		}
		else if( transOut ){
			to->render();
		}
		world->render();
	}
}