#ifndef SCREENSHELL_H
#define SCREENSHELL_H

#include "Screen.h"
#include "../Graphics/Animation/Cutscene.h"

#include "../System/World.h"

class ScreenShell: public Screen{
protected:
	World *world;
	
	std::string pathToConfig;
	Config *config;

	Cutscene *ti;
	Cutscene *duringScene;
	Cutscene *to;

	bool transIn;
	bool transOut;

	Event transTo;
	virtual void handleEvents() = 0;

	// Executes when the transition in cutscene ends.
	virtual void transInEnd();

	// Executes when teh transition out cutscene ends.
	virtual void transOutEnd();
public:
	ScreenShell();
	virtual ~ScreenShell();
	virtual bool init();
	virtual void update();
	virtual void render();
	virtual void setActive(bool value);
};

#endif