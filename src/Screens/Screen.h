#ifndef SCREEN_H
#define SCREEN_H

#include "../System/Events/Subject.h"
#include "../System/Events/Observer.h"
#include "../System/LOpenGL.h"

class ScreenManager;
class Screen: public Observer{
protected:
	bool active;
	bool scrolling;
public:
	Subject subject;

	Screen();
	Screen( ScreenManager *sm );
	virtual ~Screen();

	void setSM( ScreenManager *sm );

	virtual bool init()    = 0;
	virtual void update()  = 0;
	virtual void render()  = 0;
	virtual bool isActive();
	virtual void setActive(bool value);
	virtual void notify(Event event);
	virtual bool isScrolling();
};

#endif