#ifndef VICTORYSCREEN_H
#define VICTORYSCREEN_H

#include "ScreenShell.h"
#include "../Graphics/Drawing/NumberSheet.h"
#include <sstream>
#include "../System/Factory/Config.h"

class VictoryScreen: public ScreenShell{
protected:
	int player;
	Config* playerConfig;
	virtual void handleEvents();
	void playVoice( std::string counter, std::string moveName );
	void playVictory();
public:
	virtual bool init();
	virtual void render();
};

#endif