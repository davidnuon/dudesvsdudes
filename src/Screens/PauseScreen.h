#ifndef PAUSESCREEN_H
#define PAUSESCREEN_H

#include "ScreenShell.h"

class PauseScreen: public ScreenShell{
private:
	//Handles events during update.
	virtual void handleEvents();
public:
	virtual ~PauseScreen();
	virtual bool init();
};

#endif