#include "HelpScreen.h"

HelpScreen::~HelpScreen(){}

bool HelpScreen::init(){
	pathToConfig = "res/Configs/HelpScreen.config";
	return ScreenShell::init();
}

void HelpScreen::handleEvents(){
	if( isActive() ){
		while( eventHeap->getSize() > 0 ){
			Event event = eventHeap->pop();
			switch( event.getType() ){
				case KEY_ENTER:
					if( !transIn ){
						transOut = true;
						transTo = RESET;
					}
					break;
			}
		}
		subject.notifyObservers();
	}
}