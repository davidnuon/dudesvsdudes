#ifndef GAMETESTSCREEN_H
#define GAMETESTSCREEN_H

#include "ScreenShell.h"
#include "../GameObjects/ObjectsManager.h"
#include "../System/Events/Observer.h"
#include "../System/Heap.h"
#include "../System/World.h"
#include "../Components/Graphics_Components/Graphics.h"
#include "../GameObjects/GameObjectPtr.h"
#include "../GameObjects/Unit.h"
#include "../GameObjects/Board.h"
#include "../Player/Squad.h"
#include "../Player/Player.h"
#include "../Graphics/Drawing/LTexture.h"
#include "../Graphics/Drawing/Basic_Quad.h"
#include "../Graphics/Drawing/NumberSheet.h"

class GameTestScreen: public ScreenShell{
private:
	Board *worldBoard;
	Basic_Quad *bq;

	Player *computer, *human;
	GameObjectPtr baseComputer, baseHuman;
	int* healthPtr;
	int* stevePtr;
	//int* squadIndexPtr;

	NumberSheet numberSheet;
	AQState euforiaState;
	AQState goldState;
	AQState teamState;
	AQState unitCount;
	AQState squadIndexState;
	
	virtual void handleEvents();
	virtual void gameLoop();
	virtual void transInEnd();
public:
	virtual ~GameTestScreen();
	virtual bool init();
	virtual void update();
	virtual void render();
	virtual void cleanup();
	virtual void notify(Event event);
	virtual void setActive( bool value );
};

#endif