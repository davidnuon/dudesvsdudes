/**
 *\class LFRect
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Struct that holds 4 GLfloat values.
 * These values are typically x, y, w, and h values.
 *
 */
#ifndef LFRECT_H
#define LFRECT_H

#include "GL/glew.h"

struct LFRect{
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

#endif