#include "World.h"

World::World(Screen *screen){
	this->screen = screen;

	af.setWorld( this );

	addToLog(this->screen->subject);
	addToLog(this->screen->subject, SCREEN_PAUSE);
}

World::~World(){}

std::vector<GameObjectPtr> World::getGameObjects(){
        return objectsManager.getObjectsVec();
}

GameObjectPtr World::addObject(GameObject* newObject){
        return objectsManager.addObject( newObject );
}

GameObjectPtr World::addObject(GameObjectPtr newObject){
        return objectsManager.addObject( newObject );
}

int World::removeObject(GameObject *removed){
        return objectsManager.removeObject( removed );
}

void World::update(){
	if( screen->isActive() == true ){
		handleEvents();
		objectsManager.update();
		af.update();
		subject.notifyObservers();
	}
}

void World::render(){
	if( screen->isActive() == true ){
		objectsManager.render();
		af.render();
		deck.render();
	}
}

void World::addToDeck( AQ* newAQ ){
	deck.addToLayer(newAQ);
}

void World::notify(Event event){
	if( screen->isActive() == true ){
		eventHeap->push( event );
	}
	if( event.getType() == SCREEN_PAUSE ){
		pauseWorld();
	}
	if( event.getType() == SCREEN_UNPAUSE ){
		resumeWorld();
	}
}

void World::handleEvents(){
	while( eventHeap->getSize() > 0 ){
		subject.push_event( eventHeap->pop() );
	}
	subject.notifyObservers();
}

void World::pauseWorld(){
	mp.stopAll();
}

void World::resumeWorld(){
	
}