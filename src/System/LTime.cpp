#include "LTime.h"

LTime::LTime(){
	startTicks = 0;
}

//Call start to start the timer.
void LTime::start(){
	startTicks = SDL_GetTicks();
}

int LTime::getTicks(){
	return SDL_GetTicks() - startTicks;
}

//Use to regulate the frame rate.
//If it is going above 60 fps, then it will be slowed down to 60 fps.
void LTime::delay(){
	if( getTicks() < 1000.f / Constants::FRAMES_PER_SECOND ){
		SDL_Delay( 1000.f / Constants::FRAMES_PER_SECOND - getTicks() );
	}
}