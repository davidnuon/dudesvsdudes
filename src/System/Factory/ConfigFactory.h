#ifndef CONFIGFACTORY_H
#define CONFIGFACTORY_H

#include "Config.h"
#include <map>

class ConfigFactory{
protected:
	/// Map of Config object pointers with strings as keys.
	std::map< std::string, Config* > configs;

	/**
	 * Clears the map.
	 */
	void clear();
public:
	ConfigFactory();
	virtual ~ConfigFactory();
	
	/**
	 * Use this to get a pointer to a config object,
	 * and use that pointer to query for information.
	 * \param path = The path to the config file.
	 * \return A pointer to a config object.
	 */
	Config* getConfig(std::string path);
};

#endif