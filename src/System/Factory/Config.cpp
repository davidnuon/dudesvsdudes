#include "Config.h"

Config::~Config(){}

bool Config::load(std::string path){

	std::ifstream file( path.c_str() );
	if( file.is_open() == false ){
		printf("Config Initialize Failed: Could not open file.\n");
		return false;
	}

	std::string line = "";
	while( getline( file, line ) ){
		//Removes everything on the line after the // and removes the //
		int found = line.find("//");
		if( found != -1 ){
			line = line.substr(0, found);
		}
		//TODO::Add this functionality to it. Allows user to place the exact string in quotes to be loaded.
		/*
		found = line.find(" \" ");
		if( found != -1 ){
			line = line.substr( 0, found );
		}

		found = line.find_last_of( " \" ");
		if( found != -1 ){
			line = line.substr( found );
		}
		*/
		if( line == "" ){
            continue;
        }
		
		//Splits up the string by '='.
        std::vector<std::string> paired = StringManip::split(line, "=");
        
        //Removes white spaces from the string to be used as a key.
        paired[0] = StringManip::removeWhitespace( paired[0] );

        //Makes sure that there are at least 2 values (one for a key, and the other for the value).
        if( paired.size() < 2 ){
        	printf("Config Initialize Failed: Not enough values. \n");
        	file.close();
        	return false;
        }

        //Splits value string by whitespaces to clean it up.
        std::vector< std::string> valuesSplit = StringManip::split( paired[1], " " );
        if( valuesSplit.size() < 1 ){
        	printf("Config Initialize Failed: No value found.\n");
        	file.close();
        	return false;
        }

        //If this results in an empty string, it returns false.
    	if( valuesSplit[0].size() == 0 ){
    		printf("Config Initialize Failed: Size of value is 0.\n");
    		file.close();
    		return false;
    	}

    	//printf("The value: %d\n", atof(valuesSplit[0].c_str()) );
    	//Checks if it is a number value.

		float numberFloat = atof( valuesSplit[0].c_str());
		int numberInt     = atoi(valuesSplit[0].c_str());
		//Checks if it should be an int or a float. 
		//If is it a float.
		if( numberInt != numberFloat || valuesSplit[0].find(".") != -1 ){
			floatMap[ paired[0] ] = numberFloat;
		}
		//If it is an int.
		else{
			intMap[ paired[0] ] = numberInt;
		}
		//It places the string value into the map.       
       	strMap[ paired[0] ] = StringManip::join( valuesSplit, " " );
	}
	file.close();

	return true;
}

std::string Config::getString( std::string name ){
	if( strMap.find( name ) != strMap.end() ){
		return strMap[ name ];
	}
	printf("Config Error: Didn't find string: %s\n", name.c_str());
	return "";
}

int Config::getInt( std::string name ){
	if( intMap.find( name ) != intMap.end() ){
		return intMap[ name ];
	}
	printf("Config Error: Didn't find int: %s\n", name.c_str());
	return 0;
}

float Config::getFloat( std::string name ){
	if( floatMap.find( name ) != floatMap.end() ){
		return floatMap[ name ];
	}
	printf("Config Error: Didn't find float: %s\n", name.c_str());
	return 0.f;
}