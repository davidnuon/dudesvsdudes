/**
 *\class Factory
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * A factory for the screens and basic SpriteSheets.
 *
 */
#ifndef FACTORY_H
#define FACTORY_H

#include "../../Screens/Screen.h"
#include "../../Screens/ScreenManager.h"
#include "../../Screens/GameTestScreen.h"
#include <map>

class Factory{
private:
	/**
	 * Initialize function for a screen object.
	 *
	 *\param screen = A pointer to a screen object that will be initialized.
	 *\return If the initialize was successful, the pointer that was passed in the parameter, else it will return NULL.
	 */
	static Screen* 		initScreen(Screen *screen, ScreenManager *sm);
	static std::map< std::string, Screen* > screens;

public:
	//Destructor deallocates memory if it was used.
	virtual ~Factory();

	template <class T>
	static bool add(std::string name, ScreenManager *sm){
		if( screens.find(name) != screens.end() ){
			return false;
		}

		T* obj = new T;
		Screen* newScreen = static_cast<Screen*>(obj);

		screens[ name ] = initScreen( newScreen, sm );
		return screens.find(name) != screens.end();
	}
	
	static bool destroy( std::string name );
	static bool destroy( Screen *screen );
	static Screen* getScreen(std::string name);

	template <class T>
	static Screen* getScreenSafe(std::string name, ScreenManager *sm){
		add<T>(name, sm);
		return getScreen( name );
	}

};

#endif