#include "ConfigFactory.h"

ConfigFactory::ConfigFactory(){}

ConfigFactory::~ConfigFactory(){
	clear();
}

Config* ConfigFactory::getConfig(std::string path){
	//Checks if the config file was not previously loaded.
	if( configs.find(path) == configs.end() ){

		//If not, it loads a new config object.
		Config *newConfig = new Config;
		if( newConfig->load( path ) == false ){
			printf("ConfigFactory Error: Failed to initialized, now returning NULL\n");
			return NULL;
		}

		//The config object previously created is now stored in the map.
		configs[ path ] = newConfig;
	}
	
	//Final check to make sure that the object pointer isn't NULL.
	if( configs[ path ] == NULL){
		printf("ConfigFactory Error: Pointer mapped to %s is NULL.\n", path.c_str());
	}

	//Returns the correct pointer.
	return configs[ path ];
}

void ConfigFactory::clear(){
	if( configs.size() > 0 ){
		for(std::map<std::string, Config* >::iterator iter = configs.begin(); iter != configs.end(); iter++){
			if( iter->second != NULL )
				delete iter->second;
		}
	}
}