#include "MusicPlayer.h"

const int MusicPlayer::SFXLIMIT = 15;

MusicPlayer::~MusicPlayer(){
	clear();
}

bool MusicPlayer::init(){
	int flags = MIX_INIT_MP3;
	if( Mix_Init(flags)&flags != flags ){
		return false;
	}

	if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) == -1){
		return false;
	}

	return true;
}

void MusicPlayer::cleanup(){
	Mix_CloseAudio();
	Mix_Quit();
}

void MusicPlayer::clear(){
	if( Mix_PlayingMusic() == true ){
		Mix_HaltMusic();
	}

	if( Mix_Playing(-1) > 0 ){
		Mix_HaltChannel(-1);
	}

	for(std::map<std::string, Mix_Music* >::iterator iter = music.begin(); iter != music.end(); iter++){
		Mix_FreeMusic( iter->second );
	}

	for(std::map<std::string, Mix_Chunk* >::iterator iter = chunks.begin(); iter != chunks.end(); iter++){
		Mix_FreeChunk( iter->second );
	}
}

void MusicPlayer::pauseAll(){
	Mix_Pause(-1);
	Mix_PauseMusic();
}

void MusicPlayer::resumeAll(){
	Mix_Resume(-1);
	Mix_ResumeMusic();
}

void MusicPlayer::stopAll(){
	Mix_HaltChannel(-1);
	Mix_HaltMusic();
}

bool MusicPlayer::playMusic(std::string name, int loops){
	if( music.find( name ) == music.end() ){
		Mix_Music* temp = Mix_LoadMUS( name.c_str() );
		if( temp == NULL ){
			printf("MusicPlayer could not load the Music: %s\n", name.c_str());
			printf("%s\n", Mix_GetError());
			return false;
		}
		
		music[ name ] = temp;
	}

	if( Mix_PlayingMusic() == true ){
		Mix_HaltMusic();
	}

	if( Mix_PlayMusic(music[name], loops) < 0 ){
		return false;
	}

	return true;
}

int MusicPlayer::playSFX(std::string name, int loops){
	if( chunks.find( name ) == chunks.end() ){
		Mix_Chunk* temp = Mix_LoadWAV( name.c_str() );
		if( temp == NULL ){
			printf("MusicPlayer could not load the Sound Effect: %s\n", name.c_str());
			return -1;
		}
		
		chunks[ name ] = temp;
	}

	if( Mix_Playing(-1) >= SFXLIMIT ){
		return -1;
	}

	return Mix_PlayChannel(-1, chunks[name], loops);
}