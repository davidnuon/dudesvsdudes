#include "Factory.h"

std::map< std::string, Screen* > Factory::screens;

Factory::~Factory(){
	for(std::map<std::string, Screen* >::iterator iter = screens.begin(); iter != screens.end(); iter++){
		delete iter->second;
	}
}

Screen* Factory::initScreen(Screen *screen, ScreenManager *sm){
	if( screen->init() == false ){
		delete screen;
		screen = NULL;
	}
	assert( screen != NULL );
	screen->setSM( sm );
	return screen;
}



bool Factory::destroy( std::string name ){
	if( screens.find(name) != screens.end() ){
		delete screens[ name ];
		screens[ name ] = NULL;
		screens.erase( name );
		return true;
	}
	return false;
}

bool Factory::destroy( Screen *screen ){
	for(std::map<std::string, Screen* >::iterator iter = screens.begin(); iter != screens.end(); iter++){
		if( iter->second == screen ){
			delete iter->second;
			screens.erase( iter );
			return true;
		}
	}
	return false;
}

Screen* Factory::getScreen(std::string name ){
	if( screens.find(name) != screens.end() ){
		return screens[ name ];
	}
	return NULL;
}