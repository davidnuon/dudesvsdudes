#include "AnimFactory.h"

AnimFactory::AnimFactory(){}

AnimFactory::AnimFactory(World *world){
	setWorld( world );
}

AnimFactory::~AnimFactory(){
	clear();
}

void AnimFactory::clear(){
	for(std::map<std::string, Animated* >::iterator iter = anims.begin(); iter != anims.end(); iter++){
		delete iter->second;
	}
	for(int i = 0; i < savedIters.size(); i++){
		delete savedIters[i];
	}
}

void AnimFactory::setWorld( World *world ){
	this->world = world;
}

bool AnimFactory::load( std::string name ){
	if( anims.find( name ) == anims.end() ){
		Animated* temp = new Animated;

		if( temp != NULL && temp->loadAnimation( name ) == false ){
			printf("Failed to load animation in AnimFactory: %s\n", name.c_str());
			return false;
		}
		
		anims[ name ] = temp;
	}
	return true;
}

AnimIter AnimFactory::getIter( std::string name ){
	if( load( name ) == false ){
		AnimIter failed;
		printf("AnimFactor Error: Failed to load %s\n", name.c_str());
		return failed;
	}

	AnimIter iter;
	if( iter.init( anims[name] ) == false ){
		printf("Failed to load animation in AnimFactory: %s\n", name.c_str());
		AnimIter failed;
		return failed;
	}
	return iter;
}

AnimIter* AnimFactory::getIterPtr( std::string name ){
	if( load( name ) == false ){
		printf("AnimFactory Error: Could not load %s\n", name.c_str());
		return NULL;
	}

	AnimIter *iter = new AnimIter;
	if( iter->init( anims[name] ) == false ){
		printf("Failed to load animation in AnimFactory: %s\n", name.c_str());
		return NULL;
	}

	savedIters.push_back( iter );
	return iter;
}

void AnimFactory::draw( std::string name, int index, GLfloat x, GLfloat y, bool drawFromCenter ){
	load( name );
	AnimIter iter;
	if( iter.init( anims[name] ) == true ){
		iter.switchAnim( index );
		iter.setOffset( x, y );
		iter.setDrawFromCenter( true );
		loadedAnims.push_back( iter );
	}
}

void AnimFactory::update(){
	for(int i = 0; i < loadedAnims.size(); i++){
		if( loadedAnims[i].update() == true ){
			loadedAnims.erase( loadedAnims.begin() + i );
			i--;
		}
	}
}

void AnimFactory::render(){
	for(int i = 0; i < loadedAnims.size(); i++){
		loadedAnims[i].draw( world );
	}
}