#ifndef ANIMFACTORY_H
#define ANIMFACTORY_H

#include "../../Graphics/Animation/Animated.h"
#include "../../Graphics/Animation/AnimIter.h"
#include "../../System/LFRect.h"

class AnimFactory{
private:
	World *world;
	std::map<std::string, Animated* > anims;
	std::vector<AnimIter> loadedAnims;

	//Stores the AnimIter pointers given out by getIterPtr.
	//Deallocates these pointers properly and automatically.
	std::vector<AnimIter*> savedIters;
	void clear();
	bool load( std::string name );
public:
	AnimFactory();
	AnimFactory(World *world);
	virtual ~AnimFactory();

	void setWorld( World *world );

	AnimIter getIter( std::string name );
	AnimIter *getIterPtr( std::string name );

	virtual void draw( std::string name, int index = 0, GLfloat x = 0.f, GLfloat y = 0.f, bool drawFromCenter = false );

	virtual void update();
	virtual void render();
};

#endif