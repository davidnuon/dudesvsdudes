#ifndef MUSICPLAYER_H
#define MUSICPLAYER_H

#include <iostream>
#include <SDL/SDL_mixer.h>
#include <map>

class MusicPlayer{
private:
	std::map<std::string, Mix_Music* > music;
	std::map<std::string, Mix_Chunk* > chunks;
	static const int SFXLIMIT;
public:
	virtual ~MusicPlayer();

	static bool init();
	static void cleanup();

	/**
	* Call this function to play the sound file.
	* If the corresponding music file is not already loaded,
	* it will be.
	* \param Name = Path to music file.
	* \param loops = Music will play n+1 times. -1 to infinitely loop.
	*/
	bool playMusic(std::string name, int loops = 0);

	/**
	 * Call this function to play sound effects.
	 * If the corresponding music file is not already loaded,
	 * it will be.
	 * \param Name = Path to music file.
	 * \param loops = SFX will play n+1 times. -1 to infinitely loop.
	 */
	int playSFX(std::string name, int loops = 0);

	void clear();

	static void pauseAll();
	//void pauseCurr();

	static void resumeAll();
	//void resumeCurr();

	static void stopAll();
	//void stopCurr();
};

#endif