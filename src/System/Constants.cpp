#include "Constants.h"

const unsigned int Constants::SCREEN_WIDTH      = 1024;
const unsigned int Constants::SCREEN_HEIGHT     = 576;
const unsigned int Constants::SCREEN_BPP        = 32;
const unsigned int Constants::FRAMES_PER_SECOND = 60;
const int Constants::velocity                   = 15;
unsigned int Constants::frames                  = 0;

Uint8* Constants::keystate = NULL;

LFRect Constants::panned = {0,0, SCREEN_WIDTH, SCREEN_HEIGHT};
LFRect Constants::maxPan = {0,0,0,0};

LFRect Constants::screenScale = {1,1,1,1};

void Constants::updateFrames(){
	frames++;
	if(frames >= INT_MAX - 1){
		frames = 0;
	}
	keystate = SDL_GetKeyState( NULL );
}

unsigned int Constants::getFrames(){
	return frames;
}

LFRect Constants::getPanned(){
	return panned;
}

void Constants::movePan(int direction){
	switch( direction ){
		case 0:
			panned.x -= velocity;
			checkMaxPan();
			break;
		case 1:
			panned.y += velocity;
			checkMaxPan();
			break;
		case 2:
			panned.x += velocity;
			checkMaxPan();
			break;
		case 3:
			panned.y -= velocity;
			checkMaxPan();
			break;
	}
}

bool Constants::isPressed( Event event ){
	return isPressed( event.getType() );
}

bool Constants::isPressed( Events event ){
	if( keystate == NULL || event < 0 || event > KEY_z ){
		return false;
	}
	return keystate[ event ];
}

void Constants::resetPan(){
	panned.x = 0;
	panned.y = 0;
	panned.w = 0;
	panned.h = 0;
}

void Constants::checkMaxPan(){
	if( panned.x >= maxPan.x ){
		panned.x = maxPan.x;
	}
	else if( panned.x <= -maxPan.w ){
		panned.x = -maxPan.w;	
	}
	if( panned.y >= maxPan.y ){
		panned.y = maxPan.y;
	}
	else if( panned.y <= -maxPan.h ){
		panned.y = -maxPan.h;
	}
}

LFRect Constants::getMaxPan(){
	return maxPan;
}

void Constants::setMaxPan( LFRect max ){
	setMaxPan( max.x, max.y, max.w, max.h );
}

void Constants::setMaxPan( int x, int y, int w, int h ){
	maxPan.x = x;
	maxPan.y = y;
	maxPan.w = w;
	maxPan.h = h;
}

void Constants::resetMaxPan(){
	maxPan.x = 0;
	maxPan.y = 0;
	maxPan.w = 0;
	maxPan.h = 0;
}

void Constants::setPan(int x, int y){
	panned.x = -x;
	panned.y = -y;
}

int Constants::ENEMY_TEAM = 1;
int Constants::PLAYER_TEAM = 2;

int Constants::UNIT_TYPE_BASE = 0;
int Constants::UNIT_TYPE_GRUNT = 1;
int Constants::UNIT_TYPE_JOCKEY = 2;
int Constants::UNIT_TYPE_RUSHER = 3;
int Constants::UNIT_TYPE_ARCHER = 4;
int Constants::UNIT_TYPE_TOWER = 5;