#include "Color.h"

Color::Color(GLubyte r, GLubyte g, GLubyte b, GLubyte a){
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

void Color::setColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a){
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}