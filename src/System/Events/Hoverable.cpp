#include "Hoverable.h"
#include "../World.h"

Hoverable::~Hoverable(){}

void Hoverable::notify( Event event ){
	if( event.getType() == MOUSE_MOTION && checkClick() == true ){
		onHover();
	}
	Clickable::notify( event );
}

void Hoverable::setWorld( World *world ){
	Clickable::setWorld( world );
	addToLog( world->subject, MOUSE_MOTION );	
}