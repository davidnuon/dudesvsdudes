#include "Event.h"

Event::Event(){
	type = NONE;
	data = 0;
	data1.push_back(0);
}

Event::Event( Events eventType ){
	setEvent( eventType );
	data = 0;
	data1.push_back(0);
}

Events Event::getType(){
	return type;
}

void Event::setEvent( Events eventType ){
	type = eventType;
}

Event& Event::operator=(Event newEvent){
	if(this == &newEvent)
		return *this;
	this->setEvent( newEvent.getType() );
	data = newEvent.data;
	data1 = newEvent.data1;
	return *this;
}
Event& Event::operator=(Events newEvent){
	this->setEvent( newEvent );
	return *this;
}

bool Event::operator==(Event newEvent){
	return this->getType() == newEvent.getType();
}
bool Event::operator==(Events newEvent){
	return this->getType() == newEvent;
}