#include "KeyInput.h"

Subject KeyInput::subject;

KeyInput::~KeyInput(){}

void KeyInput::handle_events(SDLKey key){
	Event tmpEvent;
	tmpEvent.setEvent(Events( int(key) ));
	subject.push_event( tmpEvent );
	subject.notifyObservers();
}

void KeyInput::getMouseState(int &x, int &y){
	SDL_GetMouseState(&x, &y);
}