#ifndef EVENT_HEAP_H
#define EVENT_HEAP_H

#include "Events.h"
#include "../Heap.h"

//Heap class used for storing events.
class Event_Heap: public Heap{
public:
	Event_Heap();
	virtual ~Event_Heap();
	void clear();
};

#endif