#include "ExtendedEvent.h"

ExtendedEvent::ExtendedEvent(Events type, GameObjectPtr dude, int data1, int data2):Event(type) {
	this->dude = dude;
	this->p = Point(-1,-1);
	this->data1 = data1;
	this->data2 = data2;
}

ExtendedEvent::ExtendedEvent(Events type, Point p, int data1, int data2):Event(type) {
	this->p = p;
	this->data1 = data1;
	this->data2 = data2;
}

ExtendedEvent::ExtendedEvent(Events type, int data1, int data2):Event(type) {
	this->p = Point(-1,-1);
	this->data1 = data1;
	this->data2 = data2;
}