/**
 *\class Events
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Enumeration for the events system.
 *
 */
#ifndef EVENTS_h
#define EVENTS_h

/**
 * Events are stored in this enum.
 * You can create new events by creating new values in this enum.
 */
enum Events{
	NONE               = 0,
	
	MOUSE_BUTTON_LEFT  = 8,
	MOUSE_BUTTON_RIGHT = 9,
	MOUSE_MOTION       = 10,
	MOUSE_WHEELUP      = 11,
	MOUSE_WHEELDOWN    = 12,
	
	KEY_ENTER          = 13,
	KEY_SPACE          = 32,

	KEY_0 = 48,
	KEY_1 = 49,
	KEY_2 = 50,
	KEY_3 = 51,
	KEY_4 = 52,
	KEY_5 = 53,
	KEY_6 = 54,
	KEY_7 = 55,
	KEY_8 = 56,
	KEY_9 = 57,

	KEY_a = 97,
	KEY_b = 98,
	KEY_c = 99,
	KEY_d = 100,
	KEY_e = 101,
	KEY_f = 102,
	KEY_g = 103,
	KEY_h = 104,
	KEY_i = 105,
	KEY_j = 106,
	KEY_k = 107,
	KEY_l = 108,
	KEY_m = 109,
	KEY_n = 110,
	KEY_o = 111,
	KEY_p = 112,
	KEY_q = 113,
	KEY_r = 114,
	KEY_s = 115,
	KEY_t = 116,
	KEY_u = 117,
	KEY_v = 118,
	KEY_w = 119,
	KEY_x = 120,
	KEY_y = 121,
	KEY_z = 122,

	KEY_UP    = 123,
	KEY_DOWN  = 124,
	KEY_LEFT  = 125,
	KEY_RIGHT = 126,
	
	TRANSITION_IN          = 148,
	TRANSITION_OUT         = 149,
	KILL_SCREEN            = 150,
	START_SCREEN_TITLE     = 151,
	START_SCREEN_PAUSE     = 152,
	START_SCREEN_GAME_OVER = 153,
	START_SCREEN_CHARSEL   = 154,
	START_SCREEN_OPTIONS   = 155,
	START_SCREEN_HELP      = 156,
	START_SCREEN_GAME      = 157,
	START_SCREEN_BESTIARY  = 158,
	GAME_TEST_SCREEN       = 159,
	START_SCREEN_GAMEOVER  = 160,
	START_SCREEN_VICTORY   = 161,
	SCREEN_PAUSE           = 170,
	SCREEN_UNPAUSE         = 171,
	RESET                  = 172,
	START_SCREEN_STORY     = 173,

	DAMAGE					= 500,
	HEAL					= 501,
	KILL 					= 502,
	UNIT_DEAD				= 503,

	DAMAGE_BUFF				= 550,
	RANGE_BUFF				= 551,
	SPEED_BUFF				= 552,
	ATTACK_COOLDOWN_BUFF	= 553,

	FRIENDLY_DAMAGE_BUFF			= 554,
	FRIENDLY_RANGE_BUFF				= 555,
	FRIENDLY_SPEED_BUFF				= 556,
	FRIENDLY_ATTACK_COOLDOWN_BUFF	= 557,
	FRIENDLY_HEAL	= 558,

	

	FRIENDLY_SPECIAL     			= 600,
	// Damange without a team
	WORLD_DAMAGE                     = 601
};

#endif