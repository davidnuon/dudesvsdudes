/**
 *\class KeyInput
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Class that handles key input through SDL.
 *
 */
#ifndef KEYINPUT_H
#define KEYINPUT_H

#include "../LOpenGL.h"
#include "Subject.h"

class KeyInput{
public:
	virtual ~KeyInput();
	/// This is used to send out event information.
	static Subject subject;

	/**
	 * This is used to collect events to later be passed around.
	 *
	 *\param key = The key that is being received.
	 */
	static void handle_events(SDLKey key);

	/**
	 * Places the x and y positions of the mouse in the parameters x and y that are passed in.
	 *
	 *\param x = The x position of mouse.
	 *\param y = The y position of the mouse.
	 */
	static void getMouseState(int &x, int &y);
};

#endif