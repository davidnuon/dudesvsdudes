#ifndef EVENT_H
#define EVENT_H

#include "Events.h"
#include <string>
#include <vector>
#include <inttypes.h>

class Event{
protected:
	Events type;
public:
	Event();
	Event( Events eventType );
	int64_t data;
	//because this seems like a great idea
	std::vector<int> data1;
	virtual Events getType();
	virtual void setEvent( Events eventType );
	virtual Event& operator=(Event newEvent);
	virtual Event& operator=(Events newEvent);

	virtual bool operator==(Event newEvent);
	virtual bool operator==(Events newEvent);
};

#endif

/*==========================================
 * Specefication for string usage for buffs
 * First 4 bytes are used to hold a float for
 * the buff value. Second four bytes hold 
 * an int for the cooldown time.
 */