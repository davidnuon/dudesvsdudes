/**
 *\class Observer
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Used for Event handling.
 * If a class inherits from this, then it can add itself to subject objects and then be notified of certain events.
 *
 */
#ifndef OBSERVER_H
#define OBSERVER_H

#include "Event.h"
#include "../Heap.h"
#include "../LOpenGL.h"
#include <utility>
//I could not include Subject.h because it caused a circular problem.
//#include "Subject.h"

class Subject;

class Observer{
protected:
	/// Heap that stores events
	Heap<Event> *eventHeap;

	/// A list a pairs, for which this object will be subscribed to the subject for the event.
	std::vector< std::pair<Subject*, Event*> > logged;
	std::vector< Subject* > loggedToAll;

	/// Static function for special comparison in the event heap. 
	static bool compare( Event &a, Event &b);
public:
	Observer();
	virtual ~Observer();
	/**
	 * This method sends events to be stored in the eventHeap.
	 *
	 *\param event = The event being received.
	 */
	virtual void notify(Event event);

	/**
	 * You don't need to call this function explicitly,
	 * because the it is set to be called during destruction.
	 */
	void removeLogs();

	/// Use these functions to easily add to the logged vector.
	void addToLog( Subject &sub, Event event);
	void addToLog( Subject &sub, Event *event);
	void addToLog( Subject &sub, Events event);

	/// Use this to subscribe to all events from a given subject.
	void addToLog( Subject &sub );

	virtual void push_event( Event event );
	virtual Event pop_event();
	int getSizeEvents();

};

#endif