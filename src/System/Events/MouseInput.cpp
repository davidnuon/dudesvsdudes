#include "MouseInput.h"

Subject MouseInput::subject;

MouseInput::~MouseInput(){}

void MouseInput::getMouseState( int &x, int &y, bool relative  ){
	SDL_GetMouseState( &x, &y );
	x /= Constants::screenScale.x;
	y /= Constants::screenScale.y;

	if( relative ){
		LFRect panned = Constants::getPanned();
		x -= panned.x;
		y -= panned.y;
	}
}

void MouseInput::handleEvents( SDL_Event event ){
	if( event.type == SDL_MOUSEBUTTONUP ){
		Event tmpEvent;
		switch( event.button.button ){
			case SDL_BUTTON_LEFT:
				tmpEvent.setEvent(MOUSE_BUTTON_LEFT);
				subject.push_event( tmpEvent );
				break;
			case SDL_BUTTON_RIGHT:
				tmpEvent.setEvent(MOUSE_BUTTON_RIGHT);
				subject.push_event( tmpEvent );
				break;
			case SDL_BUTTON_WHEELUP:
				subject.push_event( MOUSE_WHEELUP );
				break;
			case SDL_BUTTON_WHEELDOWN:
				subject.push_event( MOUSE_WHEELDOWN );
				break;
		}
		subject.notifyObservers();
	}
	if( event.type == SDL_MOUSEMOTION ){
		Event tmpEvent;
		tmpEvent.setEvent( MOUSE_MOTION );
		subject.push_event( tmpEvent );
		subject.notifyObservers();
	}
}

Event MouseInput::determineMouseEvent( SDL_Event event ){
	Event tmpEvent;
	if( event.type == SDL_MOUSEBUTTONUP ){
		switch( event.button.button ){
			case SDL_BUTTON_LEFT:
				tmpEvent.setEvent(MOUSE_BUTTON_LEFT);
				break;
			case SDL_BUTTON_RIGHT:
				tmpEvent.setEvent(MOUSE_BUTTON_RIGHT);
				break;
		}
	}
	else if( event.type == SDL_MOUSEMOTION ){
		tmpEvent.setEvent( MOUSE_MOTION );
	}
	return tmpEvent;
}