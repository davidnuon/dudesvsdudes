#include "Subject.h"

bool Subject::compare( Event &a, Event &b){
	return int(a.getType()) > int(b.getType());
}

Subject::Subject(){
	eventHeap = NULL;
	eventHeap = new Heap<Event>(&compare);

	observersVec = NULL;
	observersVec = new std::vector<Observer*>();

}

Subject::~Subject(){
	eventHeap->clear();
	delete eventHeap;

	observersVec->clear();
	delete observersVec;
}

void Subject::push_event(Event event){
	eventHeap->push(event);
}

void Subject::push_event(Events event){
	Event tempEvent(event);
	eventHeap->push( tempEvent );
}

void Subject::addObserver(Observer* observer){
	observersVec->push_back(observer);
}

void Subject::addObserver(Observer* observer, Event event){
	obsMap[event.getType()].push_back(observer);
}
void Subject::addObserver(Observer* observer, Event* event){
	addObserver( observer, *event );
}

void Subject::removeObserver(Observer* observer){
	for(int i = 0; i < observersVec->size(); i++){
		if(observersVec->at(i) == NULL || observersVec->at(i) == observer){
			observersVec->erase(observersVec->begin() + i);
			i--;
		}
	}
}

void Subject::removeObserver(Observer* observer, std::vector<Observer*> &vec){
	for(int i = 0; i < vec.size(); i++){
		if( vec.at(i) == NULL || vec.at(i) == observer){
			vec.erase(vec.begin() + i);
			i--;
		}
	}
}

void Subject::removeObserver(Observer* observer, Event event){
	if( obsMap.find(event.getType()) != obsMap.end() ){
		removeObserver( observer, obsMap[event.getType()] );
	}
}
void Subject::removeObserver(Observer* observer, Event* event){
	removeObserver( observer, *event );
}

void Subject::notifyObservers(){
	while(eventHeap->getSize() > 0){
		
		Event tempEvent = eventHeap->pop(); 
		
		//This loops through observersVec and notifies all of those Observer pointers of all events.
		for(int i = 0; i < observersVec->size(); i++){

			//Checks if the current element is NULL and has been freed or no longer in usable.
			//It then erases it from the list if true.
			if(observersVec->at(i) == NULL){
				observersVec->erase(observersVec->begin() + i);
				i--;
			}
			else
				observersVec->at(i)->notify(tempEvent);
		}

		//This notifies only the observer pointers that said explicitly that they want
		//to be notified of the current event being handled.
		if( obsMap.find(tempEvent.getType()) != obsMap.end() ){

			for( int i = 0; i < obsMap[tempEvent.getType()].size(); i++){
				if( obsMap[tempEvent.getType()].at(i) == NULL ){
					obsMap[tempEvent.getType()].erase(obsMap[tempEvent.getType()].begin() + i);
					i--;
				}
				else
					obsMap[tempEvent.getType()].at(i)->notify(tempEvent);
			}

		}
	}
}

int Subject::getSize(){
	return observersVec->size();
}

int Subject::getEventSize(){
	return eventHeap->getSize();
}