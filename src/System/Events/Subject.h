/**
 *\class Subject
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Used in Event management.
 * Gets events and notifies all listed observers of event as they arrive.
 *
 */
#ifndef SUBJECT_H
#define SUBJECT_H

#include "../LOpenGL.h"
#include <map>
#include "Observer.h"
#include "Event.h"
#include "../Heap.h"

class Subject{
private:
	/// Where the observers are stored.
	std::vector<Observer*> *observersVec;

	/// Stores vectors of Observer pointers that only want to be notified
	/// when a certain event occurs.
	std::map<Events, std::vector<Observer*> > obsMap;

	/// The heap that the events are stored in.
	Heap<Event> *eventHeap;

	static bool compare( Event &a, Event &b);
public:
	Subject();
	virtual ~Subject();
	/**
	 * Adds an observer to this subject and will be notified of all events.
	 *
	 *\param observer = The observer to be added to this Subject object.
	 */
	void addObserver(Observer* observer);

	/**
	 * Adds an observer to this subject.
	 * It will onl be notified of the specified event.
	 * This function can be called for any number of events.
	 *
	 *\param observer = The observer to be added to this Subject object.
	 *\param event = The event that you want to be notified of.
	 */
	void addObserver(Observer* observer, Event event);
	void addObserver(Observer* observer, Event* event);

	/**
	 * Removes an observer from this Subject object.
	 *
	 *\param observer = A pointer the object to be removed.
	 */
	void removeObserver(Observer* observer);
	
	/**
	 * Removes an observer from the specified vector of Observer pointers.
	 *
	 *\param observer = A pointer the object to be removed.
	 *\param vec = The vector to remove observer from.
	 */
	void removeObserver(Observer* observer, std::vector<Observer*> &vec);

	/**
	 * Removes an observer from the specified event.
	 *
	 *\param observer = A pointer the object to be removed.
	 *\param event = The event in which observer should be removed from.
	 */
	void removeObserver(Observer* observer, Event event);
	void removeObserver(Observer* observer, Event* event);

	/**
	 * This function notifies the listed observers.
	 * It will notify all Observer pointers that did not specify an event to log themselves to.
	 * If they did specify an event to log to, then they will only be notified of those events.
	 */
	void notifyObservers();

	/**
	 * Pushes an event to the to this object's event heap.
	 *
	 *\param event = Event to be addd to the heap.
	 */
	void push_event(Event event);
	void push_event(Events event);

	/**
	 *\return The number of events currently stored in the event heap.
	 */
	int getSize();
	int getEventSize();
};

#endif