#include "ScreenPauseEvent.h"

ScreenPauseEvent::ScreenPauseEvent(): Event(SCREEN_PAUSE) {
	active = false;
}

bool ScreenPauseEvent::isActive(){
	return active;
}

void ScreenPauseEvent::setActive( bool active ){
	this->active = active;
}