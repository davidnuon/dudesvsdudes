#include "Clickable.h"
#include "../World.h"

Clickable::Clickable(){
	world = NULL;
	active = true;
}

Clickable::Clickable( World *world ){
	this->world = world;
	//addToLog( world->subject, 		SCREEN_PAUSE );
	//addToLog( world->subject,		 	MOUSE_BUTTON_LEFT );
	active = true;
}

Clickable::~Clickable(){}

void Clickable::setWorld(World *world){
	this->world = world;
	addToLog( world->subject, 		SCREEN_PAUSE );
	addToLog( world->subject, 		SCREEN_UNPAUSE );
	addToLog( world->subject,		MOUSE_BUTTON_LEFT );
}

bool Clickable::checkClick(){
	if( isActive() == false ){
		return false;
	}
	int x = 0, y = 0;
	MouseInput::getMouseState( x, y );
	return x >= clickBox.x && x <= clickBox.x + clickBox.w && y >= clickBox.y && y <= clickBox.y + clickBox.h;
}

void Clickable::setClickBox(int x, int y, int w, int h){
	clickBox.x = x;
	clickBox.y = y;
	clickBox.w = w;
	clickBox.h = h;
}

void Clickable::setClickBox( LFRect box ){
	clickBox = box;
}

LFRect Clickable::getClickBox(){
	return clickBox;
}

void Clickable::notify( Event event ){
	if( event.getType() == MOUSE_BUTTON_LEFT && checkClick() == true ){
		onClick();
	}
	if( event.getType() == SCREEN_PAUSE ){
		setActive(false);
	}
	if( event.getType() == SCREEN_UNPAUSE ){
		setActive(true);
	}
}

bool Clickable::isActive(){
	return active;
}

void Clickable::setActive(bool active){
	this->active = active;
}