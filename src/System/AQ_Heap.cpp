#include "AQ_Heap.h"

bool compare( AQ* &a, AQ* &b) {
	return a->getState().priority < b->getState().priority;
}

AQ_Heap::AQ_Heap(){
	heap = NULL;
	heap = new Heap<AQ*>(&compare);
}

AQ_Heap::~AQ_Heap(){
	//heap->clear();
	while( heap->getSize() > 0 ){
		delete heap->pop();
	}
	delete heap;
}

void AQ_Heap::push(AQ* newAQ){
	heap->push( newAQ );
}

AQ* AQ_Heap::pop(){
	return heap->pop();
}

int AQ_Heap::getSize(){
	return heap->getSize();
}

void AQ_Heap::clear(){
	heap->clear();
}