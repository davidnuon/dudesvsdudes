/**
 *\class Constants
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Holds the global constants in this static class.
 *
 */
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "LFRect.h"
#include "Events/Event.h"
#include <climits>
#include <iostream>
#include <SDL/SDL.h>

class Constants{
private:
	/// The number of frames that have passed.
	static unsigned int frames;
	
	static const int velocity;
	static Uint8* keystate;

	static LFRect panned;
	static LFRect maxPan;
	static void checkMaxPan();
public:
	/**
	 * The width of the screen as a constant.
	 * The screen can be resized, but this default value cannot be changed.
	 */
	static const unsigned int SCREEN_WIDTH;

	/**
	 * The height of the screen as a constant.
	 * The screen can be resized, but this default value cannot be changed.
	 */
	static const unsigned int SCREEN_HEIGHT;

	/**
	 * The bits per pixel of the screen as a constant.
	 */
	static const unsigned int SCREEN_BPP;

	/**
	 * The maximum frames per second.
	 */
	static const unsigned int FRAMES_PER_SECOND;

	static LFRect screenScale;

	/**
	 * Static function that updates the frames.
	 * It resets the frames back to 0 if it would overflow.
	 */
	static void updateFrames();

	/**
	 *\return The number of frames.
	 */
	static unsigned int getFrames();

	static void movePan(int direction);
	static bool isPressed( Event event );
	static bool isPressed( Events event );

	static LFRect getMaxPan();
	static LFRect getPanned();
	
	static void resetPan();
	static void resetMaxPan();
	static void setMaxPan( int x, int y, int w, int h );
	static void setMaxPan( LFRect max );
	static void setPan(int x, int y);

	/**
	 * Player constants
	 */
	static int ENEMY_TEAM;
	static int PLAYER_TEAM;
	static int UNIT_TYPE_BASE;
	static int UNIT_TYPE_GRUNT;
	static int UNIT_TYPE_JOCKEY;
	static int UNIT_TYPE_RUSHER;
	static int UNIT_TYPE_ARCHER;
	static int UNIT_TYPE_TOWER;

};

#endif