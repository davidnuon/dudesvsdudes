/**
 *\class LOpenGL
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * This class includes many of the common header files needed, 
 * including the ones for SDL, OpenGL, DevIL, and GLEW.
 *
 */
#ifndef LOPENGL_H
#define LOPENGL_H


#include "SDL/SDL.h"
#include "GL/glew.h"
#include "IL/il.h"
#include "IL/ilu.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>
#include "LFRect.h"
#include "Constants.h"

#endif