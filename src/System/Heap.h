/**
 *\class Heap
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * A basic data structure, used primarily for storing events and AQ objects.
 * It can be used to store other things as well.
 *
 */
#ifndef HEAP_H
#define HEAP_H

#include <vector>
#include <cmath>
#include <stdio.h>
#include <iostream>
#include <assert.h>

template <class StoreObject>
class Heap{
private:
	/// Holds the objects to be stored in this heap.
	std::vector<StoreObject> vec;

	/**
	 * Function pointer to do comparisons.
	 */
	bool (*compare)(StoreObject&, StoreObject&);

	//Heapify up and down are background operations necessary for the heap data structure.

	/**
	 * Heapify_up function. Used in the background check if everything below it should be there.
	 *
	 *\param index = The index in which to start the check of heapify_up. This is a recurive function.
	 */
	void heapify_up(int index);

	/**
	 * Heapify_down function. Used in the background check if everything above it should be there.
	 *
	 *\param index = The index in which to start the check of heapify_down. This is a recurive function.
	 */
	void heapify_down(int index);


	/**
	 * Default function to set the function pointer to.
	 *
	 *\param a = Object of Generic Data Type for which to check if it should be placed higher in the heap than b.
	 *\param b = Object of Generic Data Type for which to check if it should be placed higher in the heap than a.
	 *\return True if a should be place higher in the heap than b.
	 */
	static bool fillComp(StoreObject &a, StoreObject &b);

	/**
	 * Returns the index of the value that is greater. Used in heapify up and down.
	 *
	 *\param index1 = Index of the value in the heap to be checked against the one in index2.
	 *\param index2 = Index of the value in the heap to be checked against the one in index1.
	 *\return Positive if index1 is greater, 0 if they are the same, and Negative if index2 is greater.
	 */
	int greater( int index1, int index2);

	/**
	 * Swaps objects in the two indicies.
	 *
	 *\param index1 = Index of an object to be swapped.
	 *\param index2 = Index of the other object to be swapped.
	 */
	void swap( int index1, int index2 );

	/**
	 * Checks if the index is within the bounds of the vector.
	 *
	 *\param index = Checks if this index is within the heaps's bounds.
	 *\return True if the index is within the bounds.
	 */
	bool withinBounds(int index);

	/**
	 * Private function that heapifies starting from the given index. There is a public function companion.
 	 * 
 	 *\param index = The index in which to start the heapify.
 	 */
	void heapify( int index );

	/**
	 *\return True if the heap is empty.
	 */
	bool isEmpty();

	/**
	 * This denotes the beginning index of the heap.
	 * It is typically 1, but here it is 0.
	 */
	static const int BEGINNINGINDEX;
public:
	/**
	 * Default heap constructor.
	 * Doesn't need to have a function object immediately. That's right STL, I'm looking at you.
	 */
	Heap();

	/**
	 * Construtcor that allows you to set the function pointer immediately.
	 *\param Funtion pointer to set the object's function pointer to.
	 */
	Heap(bool (*compare)(StoreObject&, StoreObject&));

	/**
	 * Copy constructor.
	 *\param newHeap = Heap to initialize from.
	 */
	Heap( std::vector<StoreObject> newHeap );

	virtual ~Heap();

	/**
	 * Use this to set the function pointer after construction.
	 *
	 *\param Function pointer.
	 */
	void setCompare(bool (*comp)(StoreObject&, StoreObject&));

	/**
	 * Use to add an object to the heap.
	 *
	 *\param newObject = Object to be pushed to the heap.
	 */
	void push(StoreObject newObject);

	/**
	 * Removes the largest value from the heap and returns it.
	 *
	 *\return The object on the top of the heap.
	 */
	StoreObject pop();

	/**
	 * Call this to make a heap out of the vec. Use in case something was changed.
	 */
	void heapify();

	/**
	 * Clears the heap, leaving it empty.
	 */
	virtual void clear();

	/**
	 * Returns the amount of nodes in the heap.
	 *
	 *\return The amount of objects in the heap.
	 */
	int getSize();
};

/*
Look at all them template declarations, and yes, I know it's in the header file.
It's in the header file because it causes a problem with the templates if they are
in a separate source file.
*/

template <class StoreObject>
const int Heap<StoreObject>::BEGINNINGINDEX = 0;

template <class StoreObject>
bool Heap<StoreObject>::fillComp(StoreObject &a, StoreObject &b)	{
	//Standard a is greater than b scenario.
	return a > b;
}

template <class StoreObject>
Heap<StoreObject>::Heap(){
	//Sets the function pointer to the default comparison function.
	compare = &Heap<StoreObject>::fillComp;
}
template <class StoreObject>
Heap<StoreObject>::Heap( std::vector<StoreObject> newHeap ){
	compare = &Heap<StoreObject>::fillComp;

	for(int i = 0; i < newHeap.size(); i++){
		vec.push_back( newHeap[i] );
	}

	heapify();
}

template <class StoreObject>
Heap<StoreObject>::Heap(bool (*comp)(StoreObject&, StoreObject& ) ){
	//Custom function pointer for comparison.
	compare = comp;
}

template <class StoreObject>
Heap<StoreObject>::~Heap(){}

template <class StoreObject>
void Heap<StoreObject>::setCompare(bool (*comp)(StoreObject&, StoreObject&)){
	compare = comp;
}

template <class StoreObject>
void Heap<StoreObject>::push(StoreObject newObject){
	vec.push_back( newObject );
	heapify_up( getSize() - 1);
}

template <class StoreObject>
StoreObject Heap<StoreObject>::pop(){

	assert( getSize() > 0 );

	//Swap the last node with the first node.
	swap( BEGINNINGINDEX, getSize() - 1);

	//Store the new last node, which was originally the first and largest node.
	StoreObject popped = vec[ getSize() - 1 ];

	//Erase the last node from the vector.
	vec.erase( vec.begin() + getSize() - 1 );

	//Heapify down to keep the integrity of the heap. It starts where the highest value should be.
	heapify_down( BEGINNINGINDEX );
	return popped;
}

template <class StoreObject>
void Heap<StoreObject>::clear(){
	//Used to clear the vector. Calls the destructor on elements that aren't pointers.
	//If the elements are pointers, then you are going to have a bad time, and memory leaks everywhere.
	vec.clear();
}

template <class StoreObject>
int Heap<StoreObject>::getSize(){
	return vec.size();
}

template <class StoreObject>
bool Heap<StoreObject>::isEmpty(){
	return vec.size() == 0;
}

template <class StoreObject>
void Heap<StoreObject>::heapify_up( int index ){
	//Determines which node to check against.
	int indexToCheck = (index - 1 ) / 2 ;

	//If true, then heapify up should not continue.
	if( getSize() <= 1 || indexToCheck < 0 || indexToCheck >= getSize() || indexToCheck == index){
		return;
	}

	//Checks to see if it should swap, and if so, it heapifies up.
	if( greater(index, indexToCheck) == index ){
		swap( index, indexToCheck );
		heapify_up( indexToCheck );
	}

}

template <class StoreObject>
void Heap<StoreObject>::heapify_down( int index ){
	//Determines which nodes to check against. These are the child nodes.
	int num1 = (index + 1)*2;
	int num2 = num1 - 1;

	//This is a check in case it is heapifying near the end.
	if(num2 >= getSize())
		return;
	if(num1 >= getSize())
		num1 = num2;

	//Holds the index of the largest of these two.
	int indexToCheck = greater( num1, num2 );

	//Check to see if it should continue heapifying.
	if( getSize() <= 1 || indexToCheck < 0 || indexToCheck >= getSize() || indexToCheck == index){
		return;
	}

	//Checks if the value at index is the largest.
	if( greater( index, indexToCheck ) != index  && indexToCheck >= 0){
		//If not, it swaps the current index with the largest value and then recursively calls heapify_down.
		swap( index, indexToCheck );
		heapify_down( indexToCheck );
	}
}

//use this for easy comparisons between indicies in the heap.
template <class StoreObject>
int Heap<StoreObject>::greater( int index1, int index2){
	if( withinBounds(index1) && withinBounds(index2) && compare(vec[index1], vec[index2]) ){
		return index1;
	}
	return index2;
}

//Use this for easy swapping.
template <class StoreObject>
void Heap<StoreObject>::swap( int index1, int index2 ){
	//std::cout << "Swaping " << index1 << ' ' << index2 << ' ' << getSize() << '\n';
	if(index1 == index2)
		return;
	StoreObject temp = vec[ index1 ];
	vec[ index1 ] = vec[ index2 ];
	vec[ index2 ] = temp;
}

//Use this for easy bounds checking.
template <class StoreObject>
bool Heap<StoreObject>::withinBounds(int index){
	if( index < 0 || index >= getSize() ){
		return false;
	}
	return true;
}

template <class StoreObject>
void Heap<StoreObject>::heapify(){
	heapify( getSize() - 1 );
}

//Heapifies the vector.
template <class StoreObject>
void Heap<StoreObject>::heapify( int index ){
	//Determines which node to check against.
	int indexToCheck = (index - 1 ) / 2 ;

	//If true, then heapify up should not continue.
	if( getSize() <= 1 || indexToCheck < 0 || indexToCheck >= getSize() || indexToCheck == index){
		return;
	}

	//Checks to see if it should swap, and if so, it heapifies up.
	if( greater(index, indexToCheck) == index ){
		swap( index, indexToCheck );
		heapify_down( index );
	}
	heapify( index - 1 );
}

#endif