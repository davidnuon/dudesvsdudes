/**
 *\class World
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * The World object holds much of the game's state.
 *
 */
#ifndef WORLD_H
#define WORLD_H

//Forward declaration to get around circular dependency issue. Trust me, it works.
class Deck;
#include "../GameObjects/ObjectsManager.h"
#include "../Graphics/Deck.h"
#include "../Graphics/Drawing/AQ.h"
#include "../System/Factory/MusicPlayer.h"
#include "../System/Factory/AnimFactory.h"
#include "../System/Factory/ConfigFactory.h"
#include "../Screens/Screen.h"


class World: public Observer{
private:
	/// Container for game objects.
	ObjectsManager objectsManager;

	/// Container for the layers, which store Augmented Quads to be drawn in layers.
	Deck deck;


	void pauseWorld();
	void resumeWorld();

public:
	Screen* screen;
	
	// Object that handles music and sound effects.
	MusicPlayer mp;
	AnimFactory af;
	Subject subject;
	ConfigFactory cf;

	World(Screen* screen);
	virtual ~World();

	virtual void update();
	virtual void render();
	virtual void notify(Event event);
	virtual void handleEvents();
	
	/**
	 * This gets the vector of GameObjects from the objectsManager object.
	 *
	 *\return A vector of game objects.
	 */
	std::vector<GameObjectPtr> getGameObjects();

	/**
	 * Adds a game object to the objects manager.
	 *
	 *\param newObject = GameObject to be added to the objectsManager object.
	 */
	GameObjectPtr addObject(GameObject* newObject);

	/**
	 * Adds a game object to the objects manager.
	 *
	 *\param newObject = GameObject to be added to the objectsManager object.
	 */
	GameObjectPtr addObject(GameObjectPtr newObject);

	/**
	 * Removes a game object from the objects manager.
	 *
	 *\param removed = GameObject to be removed from the objectsManager object.
	 *\return The index of the object removed.
	 */
	int removeObject(GameObject *removed);

	/**
	 * Adds an AQ to the deck, which adds it to the layer corresponding to it's layer attribute 
	 * which resides in the AQState member.
	 *
	 *\param newAQ = A pointer to the AQ object to be pushed to the deck.
	 */
	void addToDeck( AQ* newAQ );
	
};

#endif