/**
 *\class LTime
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Manages time in the program.
 *
 */
#ifndef LTIME_H
#define LTIME_H

#include "LOpenGL.h"
#include <time.h>

class LTime{
private:
	/// Stores the tick at which the time started.
	int startTicks;
public:
	/// Constructor.
	LTime();

	/// Restarts the timer.
	void start();

	/**
	 *\return The ticks since start.
	 */
	int getTicks();

	/// This function calls sleep to keep the frame rate down.
	void delay();
};

#endif