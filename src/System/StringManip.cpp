#include "StringManip.h"

std::stringstream StringManip::ss;

std::vector<std::string> StringManip::split( std::string str, std::string delimiters){
	std::vector<std::string> stringVec;

	std::vector<int> indicies;

	//Findes the indicies where delimiters exist.
	for(int i = 0; i < str.size(); i++){
		if( isDelimiter(str[i], delimiters) ){
			indicies.push_back( i );
		}
	}

	//If there are any indicies where delimiters exist, they are then removed 
	//and the remaining strings in between are placed into the vector and returned.
	if( indicies.size() > 0 ){
		std::string temp = str.substr(0, indicies[0] );
		if( temp.size() != 0 ){
			stringVec.push_back( temp );
		}
		
		for(int i = 0; i < indicies.size()-1; i++){
			temp = str.substr(indicies[i] + 1, indicies[i+1] - indicies[i] - 1 );
			if( temp.size() != 0 ){
				stringVec.push_back( temp );
			}
		}
		
		stringVec.push_back( str.substr(indicies[ indicies.size() - 1 ] + 1 ));
	}

	//If there is nothing in the vector by now, it defaults to placing the
	//original string given into the vector and returning it.
	if( stringVec.size() <= 0 ){
		stringVec.push_back(str);
	}

	return stringVec;
}

bool StringManip::isDelimiter( char c, std::string delimiter ){
	for(int i = 0; i < delimiter.size(); i++){
		if( c == delimiter[i] ){
			return true;
		}
	}
	return false;
}

std::vector<double> StringManip::convertStringVecToDouble( std::vector<std::string> stringVec ){
	std::vector<double> doubleVec;
	for(int i = 0; i < stringVec.size(); i++){
		doubleVec.push_back( atof( stringVec[i].c_str() ) );
	}
	return doubleVec;
}

std::string StringManip::removeWhitespace( std::string str ){
	for(int i = 0; i < str.size(); i++){
		if( str[i] == ' ' || str[i] == '\n' || str[i] == '\r' || str[i] == '\t'){
			str.erase(str.begin()+i);
		}
	}

	return str;
}

std::string StringManip::join(std::vector<std::string> strVec, std::string delim, int begin, int end){
	std::string newString = "";
	if( end == -1 ){
		end = strVec.size();
	}
	if( begin < strVec.size() ){
		newString += strVec[begin];
	}
	for(int i = begin+1; i < end && i < strVec.size(); i++){
		newString += delim + strVec[i];
	}
	return newString;
}

std::string StringManip::convertToInt( std::string str ){
	ss.str( std::string() );
	ss << atoi(str.c_str());
	return ss.str();
}

std::string StringManip::itoa( int num ){
	ss.str( std::string() );
	ss << num;
	return ss.str();
}