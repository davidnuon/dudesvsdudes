#ifndef SQUAD_H
#define SQUAD_H

#include <vector>
#include "../GameObjects/Unit.h"
#include "../GameObjects/Point.h"
#include "../GameObjects/GameObjectPtr.h"
#include "../Components/AI_Components/CostGrid.h"

class Squad {
private:
	//vecotor to hold all units
	std::vector<GameObjectPtr> unitList;
	//may be removed
	bool active;
	//Destination of all units
	Point rallyPoint;
	//Units within this distance of rallyPoint are considered at the target
	int range;
	//if true, will try to get units there quickly
	bool rush;
	//if true, will prefer high health units
	bool defend;
	//if true, will prefer ranged units
	bool ranged;
	//default 1.0
	//priority of 2 means 2 times its fare share of units
	//cannot go bellow 1
	double priority;
	//rate of recent casualties
	//decreases over time
	//increases priority to high casualty squads
	//may be implemented later
	//int casualtyRate;
	CostGrid *path;
	Board *board;

public:
	/**
	 * Constructor
	 */
	Squad(Board *b);

	Squad(const Squad &other);

	Squad& operator = (const Squad &other);

	~Squad();

	/**
	 * Returns the range;
	 * Units within distance range will be considered to be at the rally point.
	 *\return range
	 */
	int getRange() const;

	/**
	 * Sets the range.
	 * Units within distance range will be considered to be at the rally point.
	 *\param r = new range;
	 */
	void setRange(int r);

	/**
	 * If rush is true, will select units based on time to get here.
	 * Units prioritize getting here over living.
	 *\return rush
	 */
	bool getRush() const;

	/**
	 * Sets rush boolean
	 * If rush is true, will select units based on time to get here.
	 * Units prioritize getting here over living.
	 *\param r = new rush value;
	 */
	void setRush(bool r);

	/**
	 * If defend is true, will prefer units with high health.
	 *\return defend
	 */
	bool getDefend() const;

	/**
	 * Sets defend boolean.
	 * If defend is true, will prefer units with high health.
	 *\param d = new defend value;
	 */
	void setDefend(bool d);

	/**
	 * If ranged is true, will prefer ranged units.
	 *\return ranged
	 */
	bool getRanged() const;

	/**
	 * Sets ranged boolean.
	 * If ranged is true, will prefer ranged units.
	 *\param r = new ranged value;
	 */
	void setRanged(bool r);

	/**
	 * A priority of two will try to have twice the amount of units
	 * as a priority of one.
	 *\return priority;
	 */
	double getPriority() const;

	/**
	 * Sets the priority.
	 * A priority of two will try to have twice the amount of units
	 * as a priority of one.
	 *\param p = new priority
	 */
	void setPriority(double p);

	/**
	 * Returns the rallyPoint, which is where all units try to gather.
	 *\return rallyPoint
	 */
	Point getRallyPoint() const;

	/**
	 * Returns a pointer to the navigation grid of this squad. 
	 * All units assigned to this squad use this costGrid
	 *\return path
	 */
	CostGrid* getGrid();

	/**
	 * Add unit to squad
	 *\param unit = Unit to add
	 */
	void addUnit(GameObjectPtr unit);

	/**
	 * Remove unit from squad. Returns true if succesful.
	 *\param unit = Unit to remove
	 *\return True if unit is removed
	 */
	bool removeUnit(GameObjectPtr unit);

	/**
	 * Remove last unit added to squad.
	 *\return Unit removed from squad.
	 */
	GameObjectPtr popUnit();

	/**
	 * Returns unit i from squad.
	 *\param i = index of unit to return
	 *\return pointer to unit
	 */
	GameObjectPtr get(int i);

	/**
	 * Set point for all units in squad to pathfind to
	 *\param p = Point to pathfind to
	 */
	void setRallyPoint(Point p);

	/**
	 * Loops through all units and makes sure they are alive
	 */
	void checkAlive();

	/**
	 * Returns amount of units in squad. If check alive was not called recently,
	 * value returned may not be accurate.
	 *\return Amount of units in squad
	 */
	int getSize() const;

	/**
	 * Inactive squads should not be given units
	 *\return True if active
	 */
	bool isActive() const;

	/**
	 * sets whether this squad is active or inactive
	 */
	void setActive(bool a);

	/**
	 * Used by player class in unit balancing
	 */
	double desiredCount;

};

#endif