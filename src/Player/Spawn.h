#ifndef SPAWN_H
#define SPAWN_H

#include "../GameObjects/Point.h"

struct Spawn {
	int type;
	int count;
	Point loc;
	int spawnTime;
	int repeat;
	int repeatTime;
	int end;
};

#endif