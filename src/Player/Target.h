#ifndef TARGET_H
#define TARGET_H

#include "../GameObjects/Point.h"

struct Target {
	int squad;
	Point loc;
	float priority;
	bool rush;
	bool ranged;
	bool defend;
	int start;
	int end;
};

#endif