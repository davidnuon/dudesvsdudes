#ifndef PLAYERCOMPUTER_H
#define PLAYERCOMPUTER_H

#include "Player.h"
#include "Wave.h"

#include "../System/Events/Observer.h"

class PlayerComputer : public Player {
private:
	/**
	 * Deals with events
	 */
	//void handleEvents();
	
	bool attack;

	Point home, enemyHome;
	int counter;
	int counterEnd;
	Wave wave;

public:
	/**
	 * Create new PlayerComputer
	 *\param world = world to associate with
	 *\param t = team of player
	 */
	PlayerComputer(Board *world, int t);

	/**
	 * Initializes player and sets up stating units.
	 *\return false if unit placement fails
	 */
	bool init();

	/**
	 * Handals all logic for this ai
	 */
	void update();

	/**
	 * Returns the percent of wave progress
	 *\return wave progress in percent
	 */
	GLfloat getWaveProgress();

};

#endif