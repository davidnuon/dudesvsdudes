#ifndef PLAYERHUMAN_H
#define PLAYERHUMAN_H

#include "Player.h"
#include "../System/Factory/Config.h"
#include "../System/Events/Observer.h"

class PlayerHuman : public Player {
private:
	Config* playerConfig;
	int squadIndex[11];

	/**
	 * Deals with events
	 */
	void handleEvents();

	int selectedSquad;
	int action;
	int actionType[3];
	int actionCount[3];
	int currUnitType;
	void playVoice( std::string counter, std::string moveName );
	void bombMoveCharacter();
	void healMoveCharacter();
public:
	/**
	 * Gets squad i. Creats if not made before.
	 * Will assertion fail if is not in [0,10].
	 * Squad 0 is used for temporary unit holding only.
	 *\param i = number of squad
	 *\return squad
	 */
	Squad& getSquad(int i);

	/**
	 * Gets index of squad i.
	 *\param i = number of squad
	 *\return index of i
	 */
	int getSquadIndex(int i);
	
	/**
	 * Create new PlayerHuman
	 *\param world = world to associate with
	 *\param t = team of player
	 */
	PlayerHuman(Board *world, int t);

	/**
	 * Initializes player and sets up stating units.
	 *\return false if unit placement fails
	 */
	bool init();

	/**
	 * Destructor
	 */
	~PlayerHuman();

	/**
	 * Handals all logic for this ai
	 */
	void update();

	/**
	 *\param event = event to handle
	 */
	void notify(Event event);

	/**
	 * Handles events not handled by parent
	 *\param event = event to handle
	 */
	virtual void childHandleEvent(Event event);
	virtual int getUnitType();
	virtual void changeUnitType(int value);
	virtual int getSelectedSquad();
};

#endif