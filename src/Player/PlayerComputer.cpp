#include "PlayerComputer.h"
#include "../GameObjects/Board.h"
#include "../System/Events/MouseInput.h"
#include <iostream>

using std::cout;

PlayerComputer::PlayerComputer(Board *world, int t) :
	Player(world, t){
	counter = 0;
}

bool PlayerComputer::init() {
	attack = false;
	//enable cheating
	setGold(2000000);
	cheat = true;
	home = Point(0,world->getRows()/2);
	enemyHome = Point(world->getColumns()-1, world->getRows()/2);

	//loae wave
	Config *c = world->cf.getConfig("res/Waves/Test/wave0.config");
	wave.load(c);
	counterEnd = c->getInt("Duration");
	//make squads required by wave
	for(int i=0; i<wave.squadCount; i++) {
		makeSquad();
		squads[i+1].setActive(false);
	}
	return true;
}

void PlayerComputer::update() {
	static int delta = 0;
	delta++;
	if((delta % 10) == 0) {		
		changeGold((delta % 1));
	}

	handleEvents();
	int unitCount = getUnitCount();

	//win if game is over
	if(unitCount  == 0 && counter > counterEnd) {
		world->screen->subject.push_event( START_SCREEN_VICTORY );
		return;
	}


	if(counter < counterEnd) {
		// Randomly do things 
		int action = rand() % 20;
		switch(action) {
			case 5:
				makeUnit(1,1 + (rand() % 3),Point(rand()%20,rand()%15));
				break;
			case 10:
				areaAffect(Point(3+(rand() % 2),5),10,Event(WORLD_DAMAGE),200,10000);
				break;
			case 12:
				makeUnit(3,1, Point(rand()%20,rand()%14));
				break;
		}
	}

	//spawn units
	vector<int> cur;
	cur = wave.getSpawn(counter);
	for(int i=0; i<cur.size(); i++) {
		Spawn s = wave.spawns[cur[i]];
		makeUnit(s.type,s.count,s.loc);
	}

	//remove squads
	cur = wave.getTargetEnd(counter);
	for(int i=0; i<cur.size(); i++) {
		squads[wave.targets[cur[i]].squad].setActive(false);
	}

	//creates squads
	cur = wave.getTargetStart(counter);
	for(int i=0; i<cur.size(); i++) {
		//cout << "Making target\n";
		Target tar = wave.targets[cur[i]];
		Squad &squad = squads[tar.squad];
		squad.setActive(true);
		squad.setRallyPoint(tar.loc);
		squad.setRush(tar.rush);
		squad.setDefend(tar.defend);
		squad.setRanged(tar.ranged);
		squad.setPriority(tar.priority);
	}
	counter++;

	balanceSquads();
}


GLfloat PlayerComputer::getWaveProgress() {
	if(counter > counterEnd)
		return 100;
	return counter / (GLfloat) counterEnd * 100;
}