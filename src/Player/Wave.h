#ifndef WAVE_H
#define WAVE_H

#include <vector>
#include <map>
#include <string>

#include "../GameObjects/Point.h"
#include "Spawn.h"
#include "Target.h"
#include "Message.h"
#include "../System/Events/Events.h"
#include "../System/Factory/Config.h"


using std::vector;
using std::map;
using std::string;

class Wave {
public:
	int id;
	int duration;
	int squadCount;

	vector<Spawn> spawns;
	vector<Target> targets;

	map<int,vector<int> > spawn;
	map<int,vector<int> > targetStart;
	map<int,vector<int> > targetEnd;
	map<int,Events> pause;
	map<int,Message> message;

	bool load(Config *c);

	bool getPause(int time);
	Message getMessage(int time);
	vector<int> getSpawn(int time);
	vector<int> getTargetStart(int time);
	vector<int> getTargetEnd(int time);
};

#endif