#ifndef PLAYER_H
#define PLAYER_H

#include <vector>

#include "../GameObjects/Point.h"
#include "../GameObjects/Unit.h"
#include "Squad.h"
#include "../System/Events/Observer.h"
//#include "GameObjectSorter.h"


/**
 * Abstract player class. Subclasses will be human and computer.
 * This class is responsible for all high level logic and control of the game.
 */

class Player : public Observer {
private:
	//variables
	//currency
	int gold;
	//euforia
	int euforia;
	//team
	int team;

	//Corresponds to which character is being played by this player.
	int character;

	//remembers state of balancing
	int balance;
	double shares;
	double unitPerShare;
	int activeSquads;

	int currUnitType;

protected:
	Board *world;
	//list of squads
	std::vector<Squad> squads;

	bool cheat;

	/**
	 * Makes a new squad
	 */
	bool makeSquad();

	void setActive(int i, bool value=true);

	/**
	 * Makes units and places in board. Returns amount that could be 
	 * made and placed.
	 *\param type = type of unit to create
	 *\param count = amount of units to make
	 *\param p = where to place units
	 *\return Amount of units made and placed
	 */
	int makeUnit(int type, int count, Point p);

	/**
	 * Makes tower and places in board. Returns amount that could be 
	 * made and placed.
	 *\param type = type of tower to create
	 *\param p = where to place the tower
	 *\return Amount of towers made and placed
	 */
	int makeTower(int type, Point p);

	/**
	 * Balances units between squads
	 */
	void balanceSquads();

	/**
	 * sends event to game objects in gange
	 * will be moved to private after economy has beed added
	 */
	void areaAffect(Point center, int range, Event effect);
	void areaAffect(Point center, int range, Event effect, float modifier, int time);

public:

	/**
	 * Constructor
	 *\param world = Board that this player is associated with
	 *\param t = Team of this player
	 */
	Player(Board *world, int t);

	void cleanUp();

	virtual bool init()=0;

	virtual void update()=0;

	void superUpdate();

	void handleEvents();

	virtual void childHandleEvent(Event);

	void changeGold(int);
	void setGold(int);
	
	//accessor methods

	int getGold();

	int getEuforia();

	int getTeam();

	int getUnitCount();

	virtual void setCharacter( int num );
	virtual int getCharacter();
};

#endif