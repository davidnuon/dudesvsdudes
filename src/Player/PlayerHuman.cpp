#include "PlayerHuman.h"
#include "../GameObjects/Board.h"
#include "../System/Events/MouseInput.h"
#include "../System/Events/KeyInput.h"
#include <assert.h>
#include <iostream>

PlayerHuman::PlayerHuman(Board *world, int t) : Player(world, t) 
{
	setGold(30000);
	action = 0;
	for(int i=0; i<3; i++) 
	{
		actionType[i] = 1;
		actionCount[i] = 5;
	}
	currUnitType = 1;
	playerConfig = world->cf.getConfig("res/Configs/Player.config");
}

bool PlayerHuman::init() {
	selectedSquad = 1;
	MouseInput::subject.addObserver(this);
	addToLog(KeyInput::subject, KEY_SPACE);
	addToLog(KeyInput::subject, KEY_k);
	addToLog(KeyInput::subject, KEY_p);
	addToLog(KeyInput::subject, KEY_q);
	addToLog(KeyInput::subject, KEY_e);
	addToLog(KeyInput::subject, KEY_r);
	addToLog(KeyInput::subject, KEY_f);
	addToLog(KeyInput::subject, KEY_0);
	addToLog(KeyInput::subject, KEY_1);
	addToLog(KeyInput::subject, KEY_2);
	addToLog(KeyInput::subject, KEY_3);
	addToLog(KeyInput::subject, KEY_4);
	addToLog(KeyInput::subject, KEY_5);
	addToLog(KeyInput::subject, KEY_6);
	addToLog(KeyInput::subject, KEY_7);
	addToLog(KeyInput::subject, KEY_8);
	addToLog(KeyInput::subject, KEY_9);
	for(int i=0; i<11; i++)
		squadIndex[i] = -1;
	squadIndex[0] = 0;
	squads[0].setActive(true);
	//getSquad(1).setRush(true);
	//getSquad(2).setDefend(true);
	//getSquad(3).setRanged(true);
	return makeUnit(1,20,Point(world->getColumns()-1,world->getRows()/2)) > 0;
}

PlayerHuman::~PlayerHuman() {
	MouseInput::subject.removeObserver(this);
	KeyInput::subject.removeObserver(this);
}

Squad& PlayerHuman::getSquad(int i) {
	assert (i>=0 && i<=10);
	if(squadIndex[i] != -1)
		return squads[squadIndex[i]];
	squadIndex[i] = squads.size();
	makeSquad();
	squads[squadIndex[i]].setRallyPoint(Point(30,8));
	return squads[squadIndex[i]];
}

int PlayerHuman::getSquadIndex(int i) {
	assert (i>=0 && i<=10);
	if(squadIndex[i] != -1)
		return squadIndex[i];
	squadIndex[i] = squads.size();
	makeSquad();
	squads[squadIndex[i]].setRallyPoint(Point(30,8));
	return squadIndex[i];
}

void PlayerHuman::update() {
	static int delta = 0;
	delta++;

	if((delta % 20) == 0) {		
		changeGold(3);
	}

	balanceSquads();
}

void PlayerHuman::notify(Event event) {
	eventHeap->push(event);
}

void PlayerHuman::childHandleEvent(Event event) {
	//std::cout << "Event\n";
	static int caffineInvoke = 0;
	int x,y;

	switch(event.getType()){
		case MOUSE_BUTTON_LEFT:
			//flipBlocked(mouse);
			MouseInput::getMouseState(x,y);
			switch(action) {
				case 0:
					//send squad to mouse
					setActive(getSquadIndex(selectedSquad),true);
					getSquad(selectedSquad).setRallyPoint(world->displayToPoint(x,y));
					break;
				case 1:
					//make units
					makeUnit(actionType[1],actionCount[1],world->displayToPoint(x,y));
					break;
				case 2:
					//do magic
					areaAffect(world->displayToPoint(x,y),actionCount[2],Event(SPEED_BUFF),3,500);
					break;
			}
			break;
		case MOUSE_BUTTON_RIGHT:
			//Send all squads to mouse
			MouseInput::getMouseState(x,y);
			for(int i=0; i<squads.size(); i++)
				squads[i].setRallyPoint(world->displayToPoint(x,y));
			break;
		case MOUSE_WHEELUP:
			changeUnitType(1);
			break;
		case MOUSE_WHEELDOWN:
			changeUnitType(-1);
			break;
		case KEY_SPACE:
			MouseInput::getMouseState(x,y);
			if( x <= 1200)
				x = 1201;
			if( x > 1200 && getUnitCount() < 150 ){
				#define SummonOffset  20
				makeUnit(currUnitType,7,world->displayToPoint(x,y));
				makeUnit(currUnitType,2,world->displayToPoint(x,y));
				makeUnit(currUnitType,5,world->displayToPoint(x,y));
				makeUnit(currUnitType,4,world->displayToPoint(x,y));
				world->af.draw( "res/Animations/bubble.anim", 5, x, y);
				world->af.draw( "res/Animations/bubble.anim", 0, x, y + SummonOffset);
				world->af.draw( "res/Animations/bubble.anim", 0, x + SummonOffset, y);
				world->af.draw( "res/Animations/bubble.anim", 0, x, y - SummonOffset);
				world->af.draw( "res/Animations/bubble.anim", 0, x - SummonOffset, y);
			}
			
			break;
		case KEY_q:
			// Bomb costs 400 gold
			if(getGold() - 400 > 0) {
				changeGold(-400);
				MouseInput::getMouseState(x,y);
				if (x > 800) {
					world->af.draw( "res/Animations/bubble.anim", 6, x, y);
					areaAffect(world->displayToPoint(x,y),21,Event(FRIENDLY_SPECIAL));
					bombMoveCharacter();
				} else {
					world->af.draw( "res/Animations/bubble.anim", 2, x, y);
					areaAffect(world->displayToPoint(x,y),5,Event(DAMAGE));
				}
			} else {
				world->af.draw( "res/Animations/bubble.anim", 8, 1024 - 200, 0);
				world->af.draw( "res/Animations/bubble.anim", 9, 1024 - 200, 0);
			}
		
			break;
		case KEY_k:
				//cheating kill of nearby units
			if(getGold() - 300 > 0 && x > 1450 ) {
				changeGold(-300);
				MouseInput::getMouseState(x,y);
				world->af.draw( "res/Animations/bubble.anim", 2, x, y);
				areaAffect(world->displayToPoint(x,y),5,Event(KILL));
			}
		
			break;
		case KEY_e:
			//cheating speedbuff
			if(getGold() - 200 > 0) {
				changeGold(-200);
				MouseInput::getMouseState(x,y);
				areaAffect(world->displayToPoint(x,y),5,Event(FRIENDLY_HEAL),15,1000000);
				areaAffect(world->displayToPoint(x,y),5,Event(FRIENDLY_SPEED_BUFF),5,1000000);
			} else {
				world->af.draw( "res/Animations/bubble.anim", 8, 1024 - 200, 0);
				world->af.draw( "res/Animations/bubble.anim", 9, 1024 - 200, 0);
			}
			healMoveCharacter();
			break;
		case KEY_r:
			#define GoldCost 10000
			#define GoldCostInterest 4000
			if(getGold() - GoldCost + (caffineInvoke)*GoldCostInterest > 0) {
				changeGold(-GoldCost);
				changeGold(-(caffineInvoke)*GoldCostInterest);
				MouseInput::getMouseState(x,y);
				world->af.draw( "res/Animations/specials_abs.anim", getCharacter(), 510, 290);
				for (int i = 0; i < 5; ++i)
				{
					areaAffect(world->displayToPoint(x,y),10 * i,Event(FRIENDLY_SPECIAL));		
				}
				switch( getCharacter() ){
					case 0:
						areaAffect(world->displayToPoint(x,y),100,Event(DAMAGE),115,10000000);
						playVoice( "JackSpecialCount", "JackSpecial" );
						break;
					case 1:
						areaAffect(world->displayToPoint(x,y),100,Event(FRIENDLY_ATTACK_COOLDOWN_BUFF),115,1000000);
						break;
					case 2:
						//areaAffect(world->displayToPoint(x,y),100,Event(),115,1000000);
						areaAffect(world->displayToPoint(x,y),100,Event(FRIENDLY_HEAL),115,1000000);
						areaAffect(world->displayToPoint(x,y),100,Event(FRIENDLY_SPEED_BUFF),53,1000000);
						break;
					case 3:
						areaAffect(world->displayToPoint(x,y),100,Event(FRIENDLY_SPEED_BUFF),115,1000000);
						break;
					case 4:
						areaAffect(world->displayToPoint(x,y),100,Event(FRIENDLY_HEAL),115,1000000);
						break;
					case 5:
						areaAffect(world->displayToPoint(x,y),100,Event(FRIENDLY_DAMAGE_BUFF),115,1000000);						
						break;

				}
			}
			break;
		case KEY_f:
			//increment counter for unit placement
			actionCount[action]++;
			break;
		case KEY_0:
                selectedSquad = 10;
                action = 0;
                break;
        case KEY_1:
                selectedSquad = 1;
                action = 0;
                break;
        case KEY_2:
                selectedSquad = 2;
                action = 0;
                break;
        case KEY_3:
                selectedSquad = 3;
                action = 0;
                break;
        case KEY_4:
                selectedSquad = 4;
                action = 0;
                break;
        case KEY_5:
                selectedSquad = 5;
                action = 0;
                break;
        case KEY_6:
                selectedSquad = 6;
                action = 0;
                break;
        case KEY_7:
                selectedSquad = 7;
                action = 0;
                break;
        case KEY_8:
                selectedSquad = 8;
                action = 0;
                break;
        case KEY_9:
                selectedSquad = 9;
                action = 0;
                break;
	}
}

int PlayerHuman::getUnitType(){
	return currUnitType;
}

void PlayerHuman::changeUnitType( int value ){
	currUnitType += value;
	if( currUnitType > 4 ){
		currUnitType = 1;
	}
	else if( currUnitType < 1 ){
		currUnitType = 4;
	}
}

int PlayerHuman::getSelectedSquad(){
	return selectedSquad;
}

void PlayerHuman::playVoice( std::string counter, std::string moveName ){
	world->mp.playSFX( playerConfig->getString( moveName + StringManip::itoa( rand() % playerConfig->getInt(counter) ) ) );
}

void PlayerHuman::bombMoveCharacter(){
	switch( getCharacter() ){
		case 0:
			playVoice("JackDamageAttackCount", "JackDamageAttack");
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			break;
	}
}

void PlayerHuman::healMoveCharacter(){
	switch( getCharacter() ){
		case 0:
			playVoice("JackHealCount", "JackHeal");
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			break;
	}
}