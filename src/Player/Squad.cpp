#include "Squad.h"
#include "../GameObjects/Unit.h"
#include "../GameObjects/Point.h"
#include "../Components/AI_Components/Dijkstra.h"
#include "../Components/AI_Components/UnitAI.h"
#include <vector>
#include <assert.h>
#include <iostream>

Squad::Squad(Board *b) {
	assert (b != NULL);
	rallyPoint = Point();
	active = false;
	range = 10;
	rush = false;
	defend = false;
	ranged = false;
	priority = 1.0;
	board = b;
	path = new CostGrid();
	*path = b->getCostGrid();
}

Squad::Squad(const Squad &other) {
	rallyPoint = other.rallyPoint;
	active = other.active;
	range = other.range;
	rush = other.rush;
	defend = other.defend;
	ranged = other.ranged;
	priority = other.priority;
	board = other.board;
	path = new CostGrid();
	*path = *other.path;
}

Squad& Squad::operator =(const Squad &other) {
	if(this == &other)
		return *this;
	rallyPoint = other.rallyPoint;
	active = other.active;
	range = other.range;
	rush = other.rush;
	defend = other.defend;
	ranged = other.ranged;
	priority = other.priority;
	board = other.board;
	*path = *other.path;
	return *this;
}

Squad::~Squad() {
	delete path;
}

void Squad::addUnit(GameObjectPtr u) {
	Unit *unit = static_cast<Unit*>(u.getPtr());
	if(unit != NULL) {
		unit->setRallyPoint(rallyPoint,path);
		unitList.push_back(u);
	}
}

bool Squad::removeUnit(GameObjectPtr u) {
	Unit *unit = (Unit*) u.getPtr();
	if(unit == NULL) {
		//delete all NULL pointers
		for(int i=0; i<unitList.size(); i++) {
			if(unitList.at(i).getPtr() == NULL) {
				unitList.at(i) = unitList.back();
				unitList.pop_back();
			}
		}
		return true;
	} else {
		for(int i=0; i<unitList.size(); i++) {
			if(unitList.at(i).getPtr() == unit) {
				unitList.at(i) = unitList.back();
				unitList.pop_back();
				return true;
			}
		}
		return false;
	}
}

GameObjectPtr Squad::popUnit() {
	GameObjectPtr temp = unitList.back();
	unitList.pop_back();
	return temp;
}

GameObjectPtr Squad::get(int i) {
	return unitList.at(i);
}

void Squad::setRallyPoint(Point p) {
	*path = board->getCostGrid();
	dijkstra(*path,p);
	//path->print();
	rallyPoint = p;
	for(int i=0; i<unitList.size(); i++) {
		Unit *unit = static_cast<Unit*>(unitList.at(i).getPtr());
		if(unit != NULL) {
			unit->setRallyPoint(p,path);
			unit->ai->range = range;
		} else {
			unitList.at(i) = unitList.back();
			unitList.pop_back();
		}
	}
}

void Squad::checkAlive() {
	for(int i=0; i<unitList.size(); i++) {
		//std::cout << "checking\n";
		//Unit *unit = static_cast<Unit*>(unitList.at(i).getPtr());
		if(unitList.at(i).getPtr() == NULL) {
			unitList.at(i) = unitList.back();
			unitList.pop_back();
			//std::cout << "removing unit\n";
		}
	}
}

int Squad::getSize() const{
	return unitList.size();
}

bool Squad::isActive() const{
	return active;
}

void Squad::setActive(bool a) {
	active = a;
}

void Squad::setRange(int r) {
	range = r;
	for(int i=0; i<unitList.size(); i++) {
		Unit *unit = static_cast<Unit*>(unitList.at(i).getPtr());
		if(unit != NULL) {
			unit->ai->range = range;
		} else {
			unitList.at(i) = unitList.back();
			unitList.pop_back();
		}
	}
}

void Squad::setRush(bool r) {
	rush = r;
}

void Squad::setDefend(bool d) {
	defend = d;
}

void Squad::setRanged(bool r) {
	ranged = r;
}

void Squad::setPriority(double p) {
	priority = p;
}

int Squad::getRange() const{
	return range;
}

bool Squad::getRush() const {
	return rush;
}

bool Squad::getDefend() const {
	return defend;
}

bool Squad::getRanged() const {
	return ranged;
}

double Squad::getPriority() const {
	return priority;
}

Point Squad::getRallyPoint() const {
	return rallyPoint;
}

CostGrid* Squad::getGrid() {
	return path;
}