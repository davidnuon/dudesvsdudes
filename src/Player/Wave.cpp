#include "Wave.h"
#include <sstream>
#include <string>

using std::stringstream;
using std::string;

bool Wave::load(Config *c) {
	id = c->getInt("ID");
	int spawnCount = c->getInt("Spawns");
	int targetCount = c->getInt("Targets");
	int messages = c->getInt("Messages");
	int pauses = c->getInt("Pauses");
	duration = c->getInt("Duration");
	squadCount = c->getInt("Squads");

	spawns.reserve(spawnCount);
	targets.reserve(targetCount);
	stringstream ss;
	for(int i=0; i<messages; i++) {
		ss.str(string());
		ss << i;
		string j = ss.str();
		Message cur = {
			c->getString("Message" + j + "Text"),
			c->getInt("Message" + j + "X"),
			c->getInt("Message" + j + "Y"),
			c->getInt("Message" + j + "Start"),
			c->getInt("Message" + j + "End")
		};
		message[cur.start] = cur;
	}

	for(int i=0; i<pauses; i++) {
		ss.str(string());
		ss << i;
		string j = ss.str();
		int time = c->getInt("Pause" + j + "Start");
		string end = c->getString("Pause" + j + "End");
		Events trigger = NONE;
		if(end == "Space") {
			trigger = KEY_SPACE;
		} else if(end.size() == 1) {
			char letter = end[0];
			if(letter >= 'A' && letter <= 'Z') {
				trigger = (Events) (letter - 'A' + (int) KEY_a);
			}
		}
		if(trigger != NONE) {
			pause[time] = trigger;
		}
	}

	for(int i=0; i<spawnCount; i++) {
		ss.str(string());
		ss << i;
		string j = ss.str();
		Spawn cur = {
			c->getInt("Spawn" + j + "Type"),
			c->getInt("Spawn" + j + "Count"),
			Point(
			c->getInt("Spawn" + j + "X"),
			c->getInt("Spawn" + j + "Y")),
			c->getInt("Spawn" + j + "Time"),
			c->getInt("Spawn" + j + "Repeat"),
			c->getInt("Spawn" + j + "RepeatTime"),
			c->getInt("Spawn" + j + "End")
		};
		spawns.push_back(cur);

	}
	for(int i=0; i<targetCount; i++) {
		ss.str(string());
		ss << i;
		string j = ss.str();
		Target cur = {
			c->getInt("Target" + j + "Squad"),
			Point(
			c->getInt("Target" + j + "X"),
			c->getInt("Target" + j + "Y")),
			c->getFloat("Target" + j + "Priority"),
			c->getInt("Target" + j + "Rush") != 0,
			c->getInt("Target" + j + "Ranged") != 0,
			c->getInt("Target" + j + "Defend") != 0,
			c->getInt("Target" + j + "Start"),
			c->getInt("Target" + j + "End"),
		};
		targets.push_back(cur);
	}

	for(int i=0; i<spawnCount; i++) {
		int time = spawns[i].spawnTime;
		for(int j=0; j<spawns[i].repeat; j++) {
			if(spawns[i].end > 0 && time > spawns[i].end)
				break;
			spawn[time].push_back(i);
			time += spawns[i].repeatTime;
		}
	}
	for(int i=0; i<targetCount; i++) {
		int time = targets[i].start;
		targetStart[time].push_back(i);
		time = targets[i].end;
		targetEnd[time].push_back(i);
	}
	return true;
}

vector<int> Wave::getSpawn(int time) {
	map<int,vector<int> >::iterator it = spawn.find(time);
	if(it == spawn.end())
		return vector<int>();
	return it->second;
}

vector<int> Wave::getTargetStart(int time) {
	map<int,vector<int> >::iterator it = targetStart.find(time);
	if(it == targetStart.end())
		return vector<int>();
	return it->second;
}

vector<int> Wave::getTargetEnd(int time) {
	map<int,vector<int> >::iterator it = targetEnd.find(time);
	if(it == targetEnd.end())
		return vector<int>();
	return it->second;
}