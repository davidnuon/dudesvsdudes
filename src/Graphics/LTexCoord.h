#ifndef LTEXCOORD_H
#define LTEXCOORD_H

#include "../System/LOpenGL.h"

struct LTexCoord{
	GLfloat x;
	GLfloat y;
};

#endif