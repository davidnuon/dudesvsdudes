#ifndef DECK_H
#define DECK_H

#include "Layer.h"
#include "Drawing/AQ.h"

class Deck{
private:
	/*
	Caution, do not store in a vector. Crazy voodoo happens.
	std::vector<Layer> layers;
	*/

	//This is a pointer array storing the layers.
	Layer *layers;

	//This is the amount of layers.
	int layerAmount;
public:
	//Initialize with the amount of layers desired.
	Deck( int numOfLayers = 5 );

	//Deallocates the layers pointer array.
	virtual ~Deck();

	//Used to render all the layers in order.
	void render();

	//Adds quadToDraw to a layer specified in the AQState within it.
	void addToLayer(AQ* quadToDraw);
};

#endif
