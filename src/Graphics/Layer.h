#ifndef LAYER_H
#define LAYER_H

#include "Drawing/AQ.h"
#include "../System/AQ_Heap.h"
#include "../System/LOpenGL.h"

class Layer{
private:
	//Holds the Augmented Quads in a heap corresponding to their priority values.
	AQ_Heap heap;
public:
	Layer();

	//Adds the AQ to this layer object's heap.
	void add(AQ* newAQ);

	//Removes the AQ's and renders them to the screen.
	void render();
};

#endif