#include "Quad.h"
#include "AQ.h"
#include "../../System/World.h"

Quad::~Quad(){}

void Quad::pushToDeck(AQState newState, World *world){
	AQ* newAQ = new AQ(newState, this);
	world->addToDeck( newAQ );
}

void Quad::pushToDeck(int index, World *world){
	if( index < 0 || index >= states.size() ){
		index = 0;
	}
	pushToDeck( getState( index ), world);
}

void Quad::pushToDeck(GLfloat x, GLfloat y, GLuint index, World *world){
	AQState newState;
	newState.x = x;	newState.y = y;	newState.index = index;
	pushToDeck( newState, world );
}

void Quad::applyState( AQState newState ){
	states.push_back( newState );
}

AQState Quad::getState(int index){
	if( index < 0 || index >= states.size() ){
		AQState safetyState;
		return safetyState;
	}
	return states[ index ];
}

bool Quad::updateState( int index, AQState newState ){
	if( index < 0 || index >= states.size() ){
		return false;
	}

	states[ index ] = newState;

	return true;
}