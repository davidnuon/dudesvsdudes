#include "LTexture.h"
#include "../../System/World.h"

LTexture::LTexture(){
	texID     = 0;
	texWidth  = 0;
	texHeight = 0;
	imgWidth  = 0;
	imgHeight = 0;
	pixels    = NULL;
}

LTexture::~LTexture(){
	freeTexture();
}

void LTexture::freeTexture(){
	if(texID != 0){
		glDeleteTextures(1, &texID);
		texID = 0;
	}
	if(pixels != NULL){
		delete [] pixels;
		pixels = NULL;
	}
	texWidth  = 0;
	texHeight = 0;
	imgWidth  = 0;
	imgHeight = 0;
}

GLuint LTexture::poweroftwo(GLuint num){
	if(num != 0){
		num--;
		num |= (num >> 1);
		num |= (num >> 2);
		num |= (num >> 4);
		num |= (num >> 8);
		num |= (num >> 16);
		num++;
	}
	return num;
}

bool LTexture::loadTexture(std::string filename, GLubyte r, GLubyte g, GLubyte b, GLubyte a){

	freeTexture();

	if( !loadTextureFromFile(filename) ){
		printf("Error loading texture from file: %s\n", filename.c_str());
		return false;
	}

	GLuint size = texWidth * texHeight;
	for(int i = 0; i < size; i++){
		GLubyte *color = (GLubyte*)&pixels[i];

		if(color[0] == r && color[1] == g && color[2] == b && (color[3] == a || a == 0)){
			color[0] = 255;
			color[1] = 255;
			color[2] = 255;
			color[3] = 0;
		}
	}

	return loadTextureFromPixels();
}

bool LTexture::loadTextureFromFile(std::string filename){
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);

	ILboolean success = ilLoadImage(filename.c_str());
	if(success){
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		if(success){

			imgWidth  = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
			imgHeight = (GLuint)ilGetInteger(IL_IMAGE_HEIGHT);
			
			texWidth  = poweroftwo(imgWidth);
			texHeight = poweroftwo(imgHeight);

			if(imgWidth != texWidth || imgHeight != texHeight){
				iluImageParameter(ILU_PLACEMENT, ILU_UPPER_LEFT);
				iluEnlargeCanvas( (int)texWidth, (int)texHeight, 1.f );
			}

			GLuint size = texWidth * texHeight;
			pixels      = new GLuint[ size ];
			memcpy(pixels, ilGetData(), size * 4);

		}
		ilBindImage(0);
		ilDeleteImage(imgID);
	}
	else{
		printf("Couldn't even load the file: %s\n", filename.c_str());
	}

	return success;
}

bool LTexture::loadTextureFromPixels(){
	if(texID == 0 && pixels != NULL){
		
		glGenTextures(1, &texID);
		glBindTexture(GL_TEXTURE_2D, texID);

		glTexImage2D(GL_TEXTURE_2D, 0.f, GL_RGBA, texWidth, texHeight, 0.f, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glBindTexture(GL_TEXTURE_2D, 0);

		GLenum error = glGetError();
		if(error != GL_NO_ERROR){
			printf("Error loading texture from pixels: %s\n", gluErrorString(error));
			return false;
		}
	}
	else{
		if(texID != 0)
			printf("Texture already loaded.\n");
		if(pixels == NULL)
			printf("No pixel data.\n");
		printf("Error loading texture from pixels.\n");
		return false;
	}
	return true;
}

void LTexture::render(GLfloat x, GLfloat y, GLfloat scalarX, GLfloat scalarY, GLfloat degrees){
	if( texID != 0 ){
	
		glTranslatef( (x + getImgWidth())/2.f, (y + getImgHeight())/2.f, 0.f);

		glRotatef(degrees, 0.f, 0.f, 1.f);

		glScalef( scalarX, scalarY, 1.f);

		render();

		glScalef(1.f/scalarX, 1.f/scalarY, 1.f);

		glRotatef(-degrees, 0.f, 0.f, 1.f);

		glTranslatef( (x + getImgWidth())/-2.f, (y + getImgHeight())/-2.f, 0.f);
	}
}

void LTexture::render(AQState state){
	render(state.x, state.y, state.scaleX, state.scaleY, state.degrees);
}

void LTexture::render(){

	glBindTexture(GL_TEXTURE_2D, texID);

	GLfloat tLeft      = 0.f;
	GLfloat tRight     = (GLfloat)imgWidth / (GLfloat)texWidth;
	GLfloat tTop       = 0.f;
	GLfloat tBottom    = (GLfloat)imgHeight / (GLfloat)texHeight;
	
	GLfloat quadWidth  = imgWidth;
	GLfloat quadHeight = imgHeight;

	glBegin(GL_QUADS);
		glTexCoord2f(tLeft, 	tTop); 		glVertex2f(quadWidth / -2.f, 	quadHeight / -2.f);
		glTexCoord2f(tRight, 	tTop); 		glVertex2f(quadWidth /  2.f,		quadHeight / -2.f);
		glTexCoord2f(tRight, 	tBottom); 	glVertex2f(quadWidth /  2.f,		quadHeight /  2.f);
		glTexCoord2f(tLeft, 	tBottom); 	glVertex2f(quadWidth / -2.f, 	quadHeight /  2.f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint LTexture::getTexID(){
	return texID;
}

GLuint LTexture::getTexWidth(){
	return texWidth;
}

GLuint LTexture::getTexHeight(){
	return texHeight;
}

GLuint LTexture::getImgWidth(){
	return imgWidth;
}

GLuint LTexture::getImgHeight(){
	return imgHeight;
}