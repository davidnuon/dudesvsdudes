#include "AQ.h"
#include "Quad.h"

AQ::AQ(){
	quad = NULL;
}

AQ::AQ(AQState newState, Quad *newQuad){
	state = newState;
	quad = newQuad;
}

AQ::~AQ(){}

void AQ::applyState(AQState newState){
	state = newState;
}

void AQ::setTexture(Quad *newQuad){
	quad = newQuad;
}

AQState AQ::getState(){
	return state;
}

void AQ::render(){
	quad->render( state );
}