#ifndef LTEXTURE_H
#define LTEXTURE_H

//Forward declaration to get out of circular dependecy problems.
class World;
#include "../../System/LOpenGL.h"
#include "AQState.h"
#include "AQ.h"
#include "Quad.h"

class LTexture: public Quad{
private:
	//The attributes of the texture.
	GLuint texID;
	GLuint texWidth;
	GLuint texHeight;
	GLuint imgWidth;
	GLuint imgHeight;

	//To quickly find the next highest power of two.
	GLuint poweroftwo(GLuint num);

	//Stores the pixel data of this texture.
	GLuint *pixels;

	//Use for dallocation deallocation.
	void freeTexture();

	//Used for loading a texture from an image file.
	bool loadTextureFromFile(std::string filename);
	bool loadTextureFromPixels();

public:
	LTexture();
	virtual ~LTexture();

	//Call this to load a texture from an image file. If you want a colorkey, you must set it at this time.
	//Note: the path to the image file is relative to the executable, not this file.
	bool loadTexture(std::string filename, GLubyte r = 0, GLubyte g = 0, GLubyte b = 0, GLubyte a = 0);
	
	//Typical render function
	void render(GLfloat x, GLfloat y, GLfloat scalarX = 1.f, GLfloat scalarY = 1.f, GLfloat degrees = 0);
	
	//Holds the actual rendering to be called upon by other functions.
	void render();

	//Rendering with AQState object.
	void render(AQState state);

	//Used to get attributes of the texture.
	GLuint getTexID();
	GLuint getTexWidth();
	GLuint getTexHeight();
	GLuint getImgWidth();
	GLuint getImgHeight();
};

#endif