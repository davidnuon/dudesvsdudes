#ifndef BASIC_QUAD_H
#define BASIC_QUAD_H

#include "../../System/LOpenGL.h"
#include "Quad.h"
#include "AQState.h"

class Basic_Quad: public Quad{
public:
	virtual ~Basic_Quad();
	void render(AQState state);
};

#endif