#include "SpriteSheet.h"

SpriteSheet::SpriteSheet(){
	vbo = 0;
	ibo = NULL;
}

SpriteSheet::~SpriteSheet(){
	freeBuffers();
	clearClips();
}

bool SpriteSheet::init(){
	return true;	
}

void SpriteSheet::freeBuffers(){
	if( vbo != 0 ){
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}
	if(ibo != NULL){
		glDeleteBuffers( clips.size(), ibo );
		delete [] ibo;
		ibo = NULL;
	}
}

void SpriteSheet::clearClips(){
	clips.clear();
}

int SpriteSheet::getSize(){
	return clips.size();	
}

LFRect SpriteSheet::getClip(int index){
	//Makes sure that you are getting all positive values, incase the sprite was flipped.
	if( index >= getSize() || index < 0){
		LFRect temp = {0.f, 0.f, 0.f, 0.f};
		return temp;
	}
	LFRect returnClip = { abs(clips[ index ].x), abs(clips[ index ].y), abs(clips[ index ].w), abs(clips[ index ].h) };
	return returnClip;
}

std::vector<LFRect> SpriteSheet::getClips(){
	return clips;
}

void SpriteSheet::addClip(GLfloat x, GLfloat y, GLfloat w, GLfloat h, bool flipped){
	LFRect temp = {x, y, w, h};
	addClip(temp, flipped);
}

void SpriteSheet::addClip(LFRect clip, bool flipped){
	if(flipped){
		clip.x = clip.x + clip.w;
		clip.w *= -1;
	}
	clips.push_back(clip);
}

void SpriteSheet::render(int index){
	if( getTexID() != 0 && vbo != 0 && ibo != NULL){

		glBindTexture(GL_TEXTURE_2D, getTexID());

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glTexCoordPointer(2, GL_FLOAT, sizeof(LVertexData), (GLvoid*)offsetof(LVertexData, coord));
		glVertexPointer(2, GL_FLOAT, sizeof(LVertexData), (GLvoid*)offsetof(LVertexData, position));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo[ index ]);
		glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, NULL);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void SpriteSheet::render(AQState state){
	LFRect stretch = {state.scaleX, state.scaleY, 0.f, 0.f};
	if( state.scale != 1.f ){
		stretch.x = state.scale;
		stretch.y = state.scale;
	}
	render(state.x, state.y, state.index, !state.drawFromCenter, state.drawAbsolute, &stretch, state.degrees, state.color.r, state.color.g, state.color.b, state.color.a);
}

bool SpriteSheet::generateBuffers(){
	if( getTexID() != 0 && vbo == 0 && ibo == NULL ){

		freeBuffers();

		glGenBuffers(1, &vbo);
		ibo = new GLuint[ 4 * clips.size() ];

		glGenBuffers(4 * clips.size(), ibo);
		LVertexData vData[ 4 * clips.size() ];

		GLuint indicies[4];

		for(int i = 0; i < clips.size(); i++){

			//Sets the indices.
			for(int j = 0; j < 4; j++){
				indicies[j] = i * 4 + j;
			}

			GLfloat tLeft 	= clips[i].x 				/ getTexWidth();
			GLfloat tRight 	= (clips[i].x + clips[i].w) / getTexWidth();
			GLfloat tTop 	= clips[i].y 				/ getTexHeight();
			GLfloat tBottom = (clips[i].y + clips[i].h) / getTexHeight();

			vData[ indicies[0] ].coord.x = tLeft;
			vData[ indicies[0] ].coord.y = tTop;

			vData[ indicies[0] ].position.x = abs(clips[i].w) / -2.f;
			vData[ indicies[0] ].position.y = abs(clips[i].h) / -2.f;

			vData[ indicies[1] ].coord.x = tRight;
			vData[ indicies[1] ].coord.y = tTop;

			vData[ indicies[1] ].position.x = abs(clips[i].w) /  2.f;
			vData[ indicies[1] ].position.y = abs(clips[i].h) / -2.f;

			vData[ indicies[2] ].coord.x = tRight;
			vData[ indicies[2] ].coord.y = tBottom;

			vData[ indicies[2] ].position.x = abs(clips[i].w) /  2.f;
			vData[ indicies[2] ].position.y = abs(clips[i].h) /  2.f;

			vData[ indicies[3] ].coord.x = tLeft;
			vData[ indicies[3] ].coord.y = tBottom;

			vData[ indicies[3] ].position.x = abs(clips[i].w) / -2.f;
			vData[ indicies[3] ].position.y = abs(clips[i].h) /  2.f;

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo[i]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint), indicies, GL_STATIC_DRAW);

		}

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(LVertexData) * clips.size(), vData, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	}
	else{
		printf("Error generating buffers: \n");
		if( getTexID() == 0 ){
			printf("\tTexture not loaded.\n");
		}
		if( vbo != 0 )
			printf("\tVertex Buffer already generated.\n");
		if( ibo != NULL )
			printf("\tIndex Buffer already generated.\n");
		return false;
	}
	return true;
}

void SpriteSheet::render(GLfloat x, GLfloat y, int index, bool beginAtCenter, bool drawAbsolute, LFRect* stretch, GLfloat degrees, GLubyte r, GLubyte g, GLubyte b, GLubyte a){
	if( x + getClip(index).w < 0 || x > Constants::SCREEN_WIDTH - Constants::getPanned().x || y + getClip(index).w < 0 || y > Constants::SCREEN_HEIGHT - Constants::getPanned().y) 
		return;

	if(drawAbsolute)
		glTranslatef( -Constants::getPanned().x, -Constants::getPanned().y, 0.f );

	glTranslatef( x, y, 0.f );
	
	if(r != 255 || g != 255 || b != 255 || a != 255)
		glColor4ub(r, g, b, a);

	if(stretch != NULL)
		glScalef(stretch->x, stretch->y, 0.f);
	
	if(beginAtCenter)
		glTranslatef( (getClip(index).w)/2.f, (getClip(index).h)/2.f, 0.f );

	if(degrees != 0.f)
		glRotatef(degrees, 0.f, 0.f, 1.f);

	render(index);

	if(degrees != 0.f)
		glRotatef(-degrees, 0.f, 0.f, 1.f);

	if(beginAtCenter)
		glTranslatef( (getClip(index).w)/-2.f, (getClip(index).h)/-2.f, 0.f );

	if(stretch != NULL)
		glScalef(1.f/stretch->x, 1.f/stretch->y, 1.f);

	if(r != 255 || g != 255 || b != 255 || a != 255)
		glColor4f(1.f, 1.f, 1.f, 1.f);
	
	glTranslatef( -x, -y, 0.f );

	if(drawAbsolute)
		glTranslatef( Constants::getPanned().x, Constants::getPanned().y, 0.f );
}

void SpriteSheet::render(int index, int x, int y){
	glTranslatef( x, y, 0.f );

	render( index );

	glTranslatef( -x, -y, 0.f );
}

bool SpriteSheet::open( std::string path ){
	std::ifstream file;

	if( loadFile( file, path ) == false ){
		return false;
	}

	loadClipData( file );

	file.close();

	generateBuffers();

	return true;

}

bool SpriteSheet::loadFile( std::ifstream &file, std::string path ){

	file.open( path.c_str() );

	std::string spritePath = "";

	if( file.is_open() && file.eof() == false ){
		
		while( getline(file, spritePath) ){
			//Special command.
			if( spritePath.find("//") != -1 ){
				spritePath = spritePath.substr(0, spritePath.find("//"));
			}
			if( spritePath != "" ){
				break;
			}
		}

		//This section is for retrieving any information for a color key, then loading the texture.
		if( file.eof() == false ){
			std::string rgbaMapValue = "";
			getline(file, rgbaMapValue);

			std::vector<double> colorKeyVec = StringManip::convertStringVecToDouble( StringManip::split( rgbaMapValue, "," ) );
			
			//Convert the doubles to GLubytes and store them in the array to be used when loading texture.
			GLubyte arr[4];

			for(int i = 0; i < 4; i++){
				//This if else is if the text file didn't specify all of the rgba values. 
				//It'll only account for the ones that they have specified.
				if( i < colorKeyVec.size() )
					arr[i] = GLubyte(colorKeyVec[i]);
				else
					arr[i] = 0;
			}


			//Loads the texture with the specified rgba values.
			if( loadTexture( spritePath, arr[0], arr[1], arr[2], arr[3] ) == false ){
				printf("Sprite sheet failed to open %s\n", spritePath.c_str());
				return false;
			}
	
		}
		else{
			if( loadTexture( spritePath ) == false ){
				printf("Sprite sheet failed to open %s\n", spritePath.c_str());
				return false;
			}
		}
	}
	else{
		printf("Sprite sheet failed to open %s\n", path.c_str());
		return false;
	}

	return true;

}

void SpriteSheet::loadClipData(std::ifstream &file){
	
	std::string line = "";

	while( file.eof() == false ){
		getline(file, line);

		//Special command.
		if( line.find("//") != -1 ){
			line = line.substr(0, line.find("//"));
		}

		if( line.find("end") != -1 ){
			break;
		}

		if( line == "" ){
			continue;
		}

		std::vector<double> clipData = StringManip::convertStringVecToDouble( StringManip::split( line, "," ) );

		if( clipData.size() < 4 ){
			break;
		}

		//This is to check if the sprite should be flipped.
		bool flipped = false;
		if( clipData.size() >= 5 ){
			flipped = clipData[4];
		}

		LFRect dataToPush = { clipData[0], clipData[1], clipData[2], clipData[3] };

		addClip( dataToPush, flipped );

	}
}