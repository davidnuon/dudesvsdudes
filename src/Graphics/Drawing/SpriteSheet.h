#ifndef SPRITESHEET_H
#define SPRITESHEET_H

#include <fstream>
#include <sstream>

#include "LTexture.h"
#include "../../System/LOpenGL.h"
#include "AQState.h"

#include "../LVertexData.h"
#include "../LTexCoord.h"
#include "../../System/StringManip.h"


class SpriteSheet: public LTexture{
private:
	//Vertex Buffer Object
	GLuint vbo;

	//Index Buffer Object
	GLuint *ibo;

	//Use for freeing the buffers from memory.
	void freeBuffers();

	bool loadFile( std::ifstream &file, std::string path );
	void loadClipData(std::ifstream &file);

protected:
	//Store the sprite clips.
	std::vector<LFRect> clips;
	
public:
	SpriteSheet();
	virtual ~SpriteSheet();

	//Use when initializing abstractions of spritesheets.
	virtual bool init();

	//Use this after you add all the clips otherwise this won't 
	bool generateBuffers();

	//Use this to add clips.
	void addClip(LFRect clip, bool flipped = false);
	void addClip(GLfloat x, GLfloat y, GLfloat w, GLfloat h, bool flipped = false);

	//Use this to get rid of all clips.
	void clearClips();

	//Use this to render.
	virtual void render(int index);
	virtual void render(AQState state);
	virtual void render(GLfloat x, GLfloat y, int index, bool beginAtCenter = true, bool drawAbsolute = false, LFRect *stretch = NULL, GLfloat degrees = 0.f, GLubyte r = 255, GLubyte g = 255, GLubyte b = 255, GLubyte a = 255);

	//Returns a specific clip.
	LFRect getClip(int index);

	//Returns the clips vector.
	std::vector<LFRect> getClips();

	//Returns the amount of clips in the spritesheet.
	int getSize();

	//Use this as a basic rendering function when you don't want to deal with AQStates.
	void render(int index, int x, int y);

	bool open( std::string path );
};

#endif