#ifndef AQ_H
#define AQ_H

//Forward declaration to get around circular dependency issue. Leave it or suffer the consequences.
class Quad;
#include "../../System/LOpenGL.h"
#include "AQState.h"

class AQ{
private:
	//Holds a texture for rendering.
	Quad *quad;

	//Holds a state for the augmented quad, which will be called upon at render time.
	AQState state;
public:
	AQ(AQState newState, Quad *newQuad);
	AQ();
	virtual ~AQ();

	//Use this to apply a state or texture to this Augmented Quad.
	void applyState(AQState newState);
	void setTexture(Quad *newQuad);

	//This is for rendering. Use this when a layers object is rendering this Augmented Quad.
	void render();

	//Returns the associated AQState.
	AQState getState();
};

#endif