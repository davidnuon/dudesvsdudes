#include "TextSheet.h"

TextSheet::~TextSheet(){}

bool TextSheet::init(std::string path){
	delta.clear();

	if( open( path ) == false ){
		printf("Open failed in init.\n");
		return false;
	}

	return true;
}

bool TextSheet::init(){
	return init("res/Fonts/fontGarmond.load");
}

void TextSheet::render(std::string text){
	render(0.f, 0.f, text);
}

void TextSheet::render(GLfloat x, GLfloat y, std::string text, GLfloat scale){

	GLfloat xTraversal = 0.f;
	GLfloat yTraversal = 0.f;

	glTranslatef(x, y, 0.f);

	glScalef( scale, scale, 1.f );

	for(int i = 0; i < text.size(); i++){

		GLuint index = calculateIndex(text[i]);

		SpriteSheet::render( index );
		glTranslatef( getClip(index).w , 0.f, 0.f);
		xTraversal += getClip(index).w;
	}

	glTranslatef( -xTraversal, -yTraversal, 0.f);

	glScalef( 1.f/scale, 1.f/scale, 1.f );

	glTranslatef( -x, -y, 0.f );
}

LFRect TextSheet::getTextDimensions(std::string text, GLfloat scale){

	LFRect dimensions = {0.f, 0.f, 0.f, 0.f};

	for(int i = 0; i < text.size(); i++){
		
		GLuint index = calculateIndex(text[i]);
		if( text[i] == ' ' ){
			//text[i] = 63 + 65;
			//text[i] = 'm';
			index = 63;
		}
		dimensions.w += getClip(index).w * scale;
	}

	dimensions.h = getClip(0).h * scale;

	return dimensions;
}

//Yes, you can only display upper case letters and the space character.
//I will get .ttf's working shortly.
GLuint TextSheet::calculateIndex(char c){
	return c - 65;
}

void TextSheet::render(AQState state){

	AQState tempState = state;
	tempState += delta;
	tempState.x = 0;
	tempState.y = 0;

	GLfloat xTraversal = 0.f;
	GLfloat yTraversal = 0.f;

	glTranslatef(state.x, state.y, 0.f);

	glScalef( state.scale, state.scale, 1.f );

	for(int i = 0; i < state.text.size(); i++){
		tempState.index = calculateIndex(state.text[i]);

		SpriteSheet::render( tempState );
		
		if( tempState.index + 65 == ' ' ){
			//tempState.index = 'm' - 65;
			tempState.index = 63;
		}
		glTranslatef( getClip(tempState.index).w , 0.f, 0.f);
		xTraversal += getClip(tempState.index).w;
	}

	glTranslatef( -xTraversal, -yTraversal, 0.f);

	glScalef( 1.f/state.scale, 1.f/state.scale, 1.f );

	glTranslatef( -state.x, -state.y, 0.f );
}

void TextSheet::draw( World* world, std::string text ){
	AQState newState;
	newState = state + delta;
	newState.text = text;

	pushToDeck( newState, world );
}

void TextSheet::draw( World *world, AQState state ){
	pushToDeck( state, world );
}

void TextSheet::draw( World *world ){
	pushToDeck( state, world );
}

void TextSheet::setState( AQState newState ){
	this->state = newState;
}

void TextSheet::addDelta( AQState newState ){
	state = state + newState;
}