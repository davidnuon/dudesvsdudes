#include "AnimationIter.h"

AnimationIter::AnimationIter(){
	location = 0;
	size = 0;
	paused = false;
	loop = false;
	startLoop = 0;
	finishLoop = -1;
}

bool AnimationIter::init( unsigned int size, bool loop, int startLoop, int finishLoop ){
	setSize( size );
	setLoop( loop, startLoop, finishLoop );
	return true;
}

void AnimationIter::start(){
	paused = false;
}

void AnimationIter::stop(){
	paused = true;
}

void AnimationIter::reset(){
	location = 0;
}

void AnimationIter::setLoop(bool loop, int startLoop, int finishLoop){
	this->loop       = loop;
	this->startLoop  = startLoop;
	this->finishLoop = finishLoop;
	if( finishLoop = -1 ){
		finishLoop = getSize();
	}
}

bool AnimationIter::update(){
	if( isPaused() == false ){

		//Check if incrementing would result in out of bounds.
		if( location + 1 >= getSize() || location + 1 >= finishLoop ){
			//If it is and it is a looping animation, it will reset back to beginning.
			if( loop == true ){
				reset();
			}
			return true;
		}
		else{
			//If incrementing will not put it out of bounds, it will increment after returning.
			location++;
		}
	}
	//If paused.
	return false;
}

void AnimationIter::setSize( unsigned int size ){
	this->size = size;
}

unsigned int AnimationIter::getSize(){
	return size;
}

bool AnimationIter::isLoop(){
	return loop;
}

bool AnimationIter::isPaused(){
	return paused;
}

unsigned int AnimationIter::getLoc(){
	return location;
}