#include "Animated.h"

bool Animated::commandsSet = false;
std::vector< std::string > Animated::commands;

Animated::Animated(){
	loaded = false;
	currAnim = 0;

	if( commandsSet == false ){
		setCommands();
	}
}

Animated::~Animated(){}

//This function needs to load the animation entirely;
//This can be easily broken. Safety measures not taken.
bool Animated::loadAnimation( std::string path ){
	
	if( animations.empty() == false ){
		animations.clear();
		std::vector<Animation> (animations).swap(animations);
	}

	std::string pathTOLoader = "";

	std::ifstream file( path.c_str() );
	if( file.is_open() == false ){
		return false;
	}
	while( getline( file, pathTOLoader ) ){

		//Special command.
		if( pathTOLoader.find("//") != -1 ){
			pathTOLoader = pathTOLoader.substr(0, pathTOLoader.find("//"));
		}		
		if( pathTOLoader != "" ){
			break;
		}
	}

	//Load SpriteSheet.
	loaded = open( pathTOLoader );
	if( loaded == false ){
		file.close();
		return false;
	}
	currPath = path;

	std::vector<AQState> anim;

    while( file.eof() == false ){
        anim.clear();
        std::string temp = "";
        getline( file, temp );

        //Special command.
		if( temp.find("//") != -1 ){
			temp = temp.substr(0, temp.find("//"));
		}

        //Check to see if loop should terminate.
        if( temp.find("end") != -1 ){
        	break;
        }
        if( temp == "" ){
            continue;
        }
        
        
        std::vector<int> loopVars;
        
        //bool loop
        loopVars.push_back(0);
        
        //startLoop
        loopVars.push_back(0);
        
        //finishLoop
        loopVars.push_back(-1);

        std::vector<std::string> loopValues = StringManip::split(temp, ",");
        for(int i = 0; i < loopValues.size() && i < loopVars.size(); i++){
            loopValues[i] = StringManip::removeWhitespace(loopValues[i]);
            loopVars[i] = atoi( loopValues[i].c_str() );
        }

		//For looping and adding delta AQStates.
		GLuint delta     = 0;
		
		//Flag to check if it is a delta or static_delta.
		bool staticDelta = false;
		
		//Checks if it needs to draw or not.
		bool storing     = false;

        //The held state AQState.
        AQState heldState;

        //Holds the origin number of deltas in indicies[0], and from there,
        //it holds the index ranges.
        std::vector<int> indicies;

        while( getline( file, temp ) ){
            //Check if it should break from loop.

            //Special command.
            if( temp.find("//") != -1 ){
                temp = temp.substr(0, temp.find("//"));
            }               

            if( temp.find("end") != -1 ){
                break;
            }
            if( temp == "" ){
                continue;
            }

            //Finds commands and handles them.
            if( commandFound(temp) == true ){
                std::vector<std::string> com = StringManip::split( temp, "=," );
                com[0] = StringManip::removeWhitespace(com[0]);
                
                if( anim.size() > 0 ){
                    heldState = anim[ anim.size()-1 ];
                }

                if( com.size() > 1 ){
                    delta = atoi( com[1].c_str() );
                    if( delta < 0 ){
                        delta = 0;
                    }
                }

                //Sets up index ranges to make looping through animations easier.
                indicies.clear();
                if( com.size() >= 5 && com[2].compare("$range") == 0 ){
                    indicies.push_back( atoi( com[1].c_str() ) );

                    int start = atoi( com[3].c_str() );
                    int stop = atoi( com[4].c_str() );

                    int step = 1;
                    if( com.size() >= 6 ){
                        step = atoi( com[5].c_str() );
                    }
                    int repeater = 1;
                    if( com.size() >= 7 ){
                        repeater = atoi( com[6].c_str() );
                    }

                    for(int i = start; abs(i-start) <= abs(stop-start); i+=step){
                        for(int j = 0; j < repeater; j++){
                        	indicies.push_back( i );
                        }
                    }
                }

                //Special command "$repeat" that allows you to repeat the last AQState n times.
                //It also allows for index ranges.
                if( com[0].compare("$repeat") == 0 ){
                    while( delta > 0 ){
                        if( indicies.size() > 1 ){
                            heldState.index = indicies[ (indicies[0] - delta) % (indicies.size() - 1) + 1 ];
                        }
                        anim.push_back( heldState );
                        --delta;
                    }
                }

                //Command "$delta" allows you to update the most recent AQState with the delta state.
                //This is what you want for piping.
                if( com[0].compare("$delta") == 0 ){
                    staticDelta = false;
                }

                //Command "$static_delta" is basically repeat, but first adding a delta to 
                //the state to be repeated.
                if( com[0].compare("$static_delta") == 0 ){
                    staticDelta = true;
                }

                //Command "$store" saves the following state in heldState, but doesn't add it.
                //Use for piping.
                if( com[0].compare("$store") == 0 ){
                    storing = true;
                }       

            }
            else{
                if( delta > 0 ){
                    //Gets the delta state to be added later on.
                    AQState deltaState = getDeltaState(temp);

                    //Loops delta times, adding a new AQState each time.
                    while( delta > 0 ){
                        //Adds the delta state to the held state.
                        AQState newState = heldState + deltaState;

                        //If there is an index range, this changes the index
                        //based on the index range.
                        if( indicies.size() > 1 ){
                            newState.index = indicies[ (indicies[0] - delta) % (indicies.size() - 1) + 1 ];
                        }

                        //Addes the new state to the vector holding AQStates.
                        anim.push_back( newState );

                        //If it is not a static delta, it updates the held state here.
                        if( staticDelta == false ){
                            heldState = anim[ anim.size()-1 ];
                        }
                        --delta;
                    }
                    staticDelta = false;
                }       
                else{
                    //Pushes that new AQState into the local temporary vector storing AQStates.
                    if(storing == false){
                        anim.push_back( defaultLoad(temp) );
                    }
                    else{
                        heldState = defaultLoad(temp);
                        storing = false;
                    }
                }
            }

        }

        //Creates a new Animation object and initializes it with the vector of AQStates from parsing.
        Animation animation( anim, loopVars[0], loopVars[1], loopVars[2] );

        //Adds that animation to the list of animations.
        addAnimation( animation );
    }

    file.close();

    return loaded;
}

AQState Animated::setState( std::vector<std::string> state, AQState newState ){
	std::map<std::string, int*> 	stateMapI;
	std::map<std::string, GLfloat*> stateMapF;
	std::map<std::string, GLubyte*> stateMapU;
	std::map<std::string, bool*>	stateMapB;
	

	stateMapI["w"]              = (int*)&newState.w;
	stateMapI["h"]              = (int*)&newState.h;
	stateMapI["index"]          = (int*)&newState.index;
	stateMapI["layer"]          = (int*)&newState.layer;
	stateMapI["priority"]       = (int*)&newState.priority;

	stateMapB["drawFromCenter"] = (bool*)&newState.drawFromCenter;
	stateMapB["drawAbsolute"]   = (bool*)&newState.drawAbsolute;
	
	stateMapF["x"]              = (GLfloat*)&newState.x;
	stateMapF["y"]              = (GLfloat*)&newState.y;
	stateMapF["degrees"]        = (GLfloat*)&newState.degrees;
	stateMapF["scaleX"]         = (GLfloat*)&newState.scaleX;
	stateMapF["scaleY"]         = (GLfloat*)&newState.scaleY;
	stateMapF["scale"]          = (GLfloat*)&newState.scale;
	
	stateMapU["r"]              = &newState.color.r;
	stateMapU["g"]              = &newState.color.g;
	stateMapU["b"]              = &newState.color.b;
	stateMapU["a"]              = &newState.color.a;

	for(int i = 0; i < state.size(); i++){
		std::vector<std::string> vec = StringManip::split( state[i], "=" );
		if( vec.size() == 2 ){
			vec[0] = StringManip::removeWhitespace(vec[0]);
			if( stateMapI.find( vec[0] ) != stateMapI.end() ){
				setValueInt( stateMapI[ vec[0] ], vec[1] );
			}
			else if( stateMapF.find( vec[0] ) != stateMapF.end() ){
				setValueDouble( stateMapF[ vec[0] ], vec[1] );
			}
			else if( stateMapU.find( vec[0] ) != stateMapU.end() ){
				setColor( *stateMapU[ vec[0] ], vec[1] );
			}
			else if(stateMapB.find( vec[0] ) != stateMapB.end()){
				setValueBool( stateMapB[ vec[0] ], vec[1] );
			}
		}
	}

	return newState;
}

void Animated::setColor(GLubyte &a, std::string value){
	if( value.size() > 0 ){
		a = (GLubyte)atoi( value.c_str() );
	}
}

void Animated::setValueInt( int *a, std::string stringVal ){
	if( stringVal.size() > 0 ){
		*a = atoi( stringVal.c_str() );
	}
}

void Animated::setValueDouble(GLfloat *a, std::string stringVal){
	if( stringVal.size() > 0 ){
		*a = atof( stringVal.c_str() );
	}
}

void Animated::setValueBool( bool* a, std::string val){
	*a = (bool)atoi( val.c_str() );
}

bool Animated::isLoaded(){
	return loaded;
}

bool Animated::switchAnim( int index ){
	//Safety check.
	if( index < 0 || index >= getSize() ){
		printf("Warning: index out of range.\n\tbool Animated::switchAnim( int index );\n\tIndex is: %d\nErrorFrom: %s\n", index, currPath.c_str());
		//Returns false if didn't successfully switch.
		return false;
	}

	//Reset current animation before switching to the new one.
	getAnim( currAnim )->reset();
	//Switch to new Animation.
	currAnim = index;
	return true;
}

void Animated::animate(World *world){
	pushToDeck( getAnim( currAnim )->next(), world );
}

bool Animated::update(){
	return getAnim( currAnim )->update();
}

void Animated::update( int index ){
	if( index >= 0 && index < getSize() ){
		getAnim( index )->update();
	}
	else{
		printf("Warning: index out of range.\n\void Animated::update( int index );\n\tIndex is: %d\nError is from %s\n", index, currPath.c_str());
	}
}

void Animated::draw(World *world){
	pushToDeck( getAnim( currAnim )->getCurr(), world );
}

void Animated::draw( World *world, int index ){
	if( index >= 0 && index < getSize() ){
		pushToDeck( getAnim( index )->getCurr(), world );
	}
}

void Animated::draw( World *world, GLfloat x, GLfloat y, bool drawFromCenter ){
		AQState state = getAnim( currAnim )->getCurr();
		
		state.x += x;
		state.y += y;

		if( drawFromCenter == true ){
			LFRect dim = getDimensionsScaled( currAnim );
			state.x += dim.w/2;
			state.y += dim.h/2;
		}

		pushToDeck( state, world );
}

void Animated::draw( World *world, int index, GLfloat x, GLfloat y, bool drawFromCenter ){
	if( index >= 0 && index < getSize() ){
		AQState state = getAnim( index )->getCurr();
		state.x += x;
		state.y += y;

		pushToDeck( state, world );
	}
	else{
		printf("Error, index out of bounds on draw function from %s.\n", currPath.c_str());
	}
}

void Animated::draw( World *world, AnimIter *iter, GLfloat x, GLfloat y, bool drawFromCenter ){
	unsigned int index = iter->getLoc();
	if( index >= 0 && index < getSize() ){
		AQState state = getAnim( index )->getIndex( iter->getCurrAnim()->getLoc() );
		state.x += x;
		state.y += y;

		pushToDeck( state, world );
	}
}

void Animated::draw( World *world, AnimIter *iter, AQState delta, GLfloat x, GLfloat y){
	unsigned int index = iter->getLoc();
	if( index >= 0 && index < getSize() ){
		AQState state = getAnim( index )->getIndex( iter->getCurrAnim()->getLoc() );
		state.x += x;
		state.y += y;

		pushToDeck( state + delta, world );
	}
}

std::vector< Animation > Animated::getAnims(){
	return animations;
}

Animation* Animated::getAnim( int index ){
	//Safety check.
	if( index < 0 || index >= getSize() ){
		printf("Warning: index out of range.\n\tAnimation Animated::getAnim( int index );\n\tIndex is: %d\n", index);
		if( getSize() > 0 ){
			return &animations[ 0 ];
		}
		return new Animation();
	}
	return &animations[ index ];
}

void Animated::addAnimation( Animation anim ){
	//Safety check.
	if( anim.getSize() > 0 ){
		animations.push_back( anim );
	}
}

int Animated::getSize(){
	return animations.size();
}

void Animated::pause( int index ){
	getAnim( index )->stop();
}

void Animated::pause(){
	getAnim( currAnim )->stop();
}

void Animated::start( int index ){
	getAnim( index )->start();
}

void Animated::start(){
	getAnim( currAnim )->start();
}

void Animated::reset( int index ){
	getAnim( index )->reset();
}

void Animated::reset(){
	getAnim( currAnim )->reset();
}

GLuint Animated::getCurrAnim(){
	return currAnim;
}

LFRect Animated::getDimensions( int index ){
	if( index > getSize() ){
		LFRect temp = {0,0,0,0};
		return temp;
	}
	return getClip( getAnim(index)->getSpriteIndex() );
}

LFRect Animated::getDimensionsScaled( int index ){
	if( index > getSize() ){
		LFRect temp = {0,0,0,0};
		return temp;
	}
	AQState state = getAnim( index )->getCurr();

	LFRect dim = getClip( getAnim(index)->getSpriteIndex() );
	dim.w *= state.scaleX * state.scale;
	dim.h *= state.scaleY * state.scale;
	return dim;
}

void Animated::setCommands(){
	commands.push_back("delta");
	commands.push_back("static_delta");
	commands.push_back("repeat");
	commands.push_back("store");
}

bool Animated::commandFound(std::string line){
	for(int i = 0; i < commands.size(); i++){
		if( line.find( '$' + commands[i] ) != -1 ){
			return true;
		}
	}
	return false;
}

AQState Animated::defaultLoad( std::string temp ){
	//Splits string into vector of strings.
	std::vector<std::string> strState = StringManip::split(temp, ",");

	AQState newState;

	//Places the values into an AQState.
	return setState( strState, newState );
}

std::string Animated::getValue(std::string str){
	std::vector<std::string> vec = StringManip::split(str, "=");
	return vec[ vec.size()-1 ];
}

AQState Animated::getDeltaState(std::string temp){
	AQState newState;
	newState.clear();
	return setState( StringManip::split(temp, ","), newState );
}

LFRect Animated::getPos( int index, int animationIndex ){
	if( index <= 0 || index >= getSize() ){
		index = currAnim;
	}
	AQState state = animations[ index ].getIndex( animationIndex );
	LFRect pos = animations[ index ].getBox( animationIndex );
	
	if( pos.w <= 0 ){
		pos.w = getClip( animations[ index ].getSpriteIndex( animationIndex ) ).w;
	}
	if( pos.h <= 0 ){
		pos.h = getClip( animations[ index ].getSpriteIndex( animationIndex ) ).h;
	}
	int mx = pos.w / 2.f;
	int my = pos.h / 2.f;

	pos.w *= state.scaleX * state.scale;
	pos.h *= state.scaleY * state.scale;

	pos.x -= pos.w / 2.f - mx;
	pos.y -= pos.h / 2.f - my;

	return pos;
}

std::string Animated::getPath(){
	return currPath;
}