#ifndef CUTSCENE_H
#define CUTSCENE_H

#include "Animated.h"
#include "AnimIter.h"
#include "TimeRange.h"

class Cutscene{
protected:
	TimeRange timeRange;
	GLuint beginTime;
	std::vector< std::pair< TimeRange, AnimIter > > anims;
	
	World *world;
	
	bool started;
	bool finished;

	bool loadCutscene( std::string path );
	void initVars();

public:
	Cutscene(World *world);
	virtual ~Cutscene();
	bool init( std::string path );
	bool update();
	void render();

	void start();
	void stop();

	bool isStarted();
	bool isFinished();
	void setWorld( World *world );
};

#endif