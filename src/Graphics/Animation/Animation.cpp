#include "Animation.h"

Animation::Animation( bool loop, int startLoop, int finishLoop ){
	this->loop = loop;
	location = 0;
	setLoopBounds(startLoop, finishLoop);
	paused = false;
}

Animation::Animation( std::vector<AQState> newAnim, bool loop, int startLoop, int finishLoop ){
	setAnimation( newAnim );
	this->loop = loop;
	location = 0;
	setLoopBounds(startLoop, finishLoop);
	paused = false;
}
	
void Animation::start(){
	paused = false;
}

void Animation::stop(){
	paused = true;
}

void Animation::reset(){
	location = startLoop;
}

void Animation::setLoop(bool loop){
	this->loop = loop;
}

void Animation::setLoopBounds(int startLoop, int finishLoop){
	if( startLoop < 0 || startLoop >= getSize() ){
		startLoop = 0;
	}
	if( finishLoop < 0 || finishLoop < startLoop || finishLoop > getSize()){
		finishLoop = getSize();
	}
	this->startLoop  = startLoop;
	this->finishLoop = finishLoop;
}

AQState Animation::next(){
	if( paused == false ){

		//Check if incrementing would result in out of bounds.
		if( location + 1 >= getSize() || location + 1 >= finishLoop ){
			//If it is and it is a looping animation, it will reset back to beginning.
			if( loop == true ){
				reset();
			}
			return getIndex( location );
		}
		else{
			//If incrementing will not put it out of bounds, it will increment after returning.
			return getIndex( location++ );
		}
	}
	//If paused.
	return getIndex( location );
}

bool Animation::update(){
	if( isPaused() == false ){

		//Check if incrementing would result in out of bounds.
		if( location + 1 >= getSize() || location + 1 >= finishLoop ){
			//If it is and it is a looping animation, it will reset back to beginning.
			if( loop == true ){
				reset();
			}
			return true;
		}
		else{
			//If incrementing will not put it out of bounds, it will increment after returning.
			location++;
		}
	}
	//If paused.
	return false;
}

AQState Animation::getCurr(){
	return getIndex( location );
}

AQState Animation::getIndex( int index ){
	if( index <= 0 || index >= anim.size() ){
		//Warning message, do not remove.
		//printf("Warning: index out of range.\n\tAQState Animation::getIndex( int index );\n\tIndex is: %d", index);

		//Returns a new AQState to prevent a segfault.
		index = location;
	}
	return anim[ index ];
}

int Animation::getSize(){
	return anim.size();
}

bool Animation::isLoop(){
	return loop;
}

std::vector<AQState> Animation::getAnim(){
	return anim;
}

void Animation::setAnimation( Animation newAnim ){
	setAnimation( newAnim.getAnim() );
}

void Animation::setAnimation( std::vector<AQState> newAnim ){
	anim = newAnim;
}

void Animation::add( AQState newState ){
	anim.push_back( newState );
}

unsigned int Animation::getSpriteIndex( int index ){
	if( index <= 0 || index >= getSize() ){
		index = location;
	}
	return getIndex( index ).index;
}

bool Animation::isPaused(){
	return paused;
}

LFRect Animation::getBox( int index ){
	if( index <= 0 || index >= getSize() ){
		index = location;
	}
	AQState cs = getIndex( index );
	LFRect box = {cs.x, cs.y, cs.w, cs.h};
	return box;
}