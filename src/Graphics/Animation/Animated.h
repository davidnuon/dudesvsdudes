#ifndef ANIMATED_H
#define ANIMATED_H

#include "../Drawing/SpriteSheet.h"
#include "Animation.h"
#include "AnimIter.h"
#include "../../System/StringManip.h"

#include <map>
#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <fstream>
#include <cmath>

class Animated: public SpriteSheet{
private:
	
	/// Holds the position of the current animation in the vector of Animations.
	GLuint currAnim;
	std::string currPath;
	/**
	Stores whether or not this object is loaded. Starts as false.
	To load, call loadAnimation( std::string path );
	*/
	bool loaded;
	static bool commandsSet;
	static std::vector< std::string > commands;
	static void setCommands();
	bool commandFound(std::string line);

	/// These three functions are background functions.
	AQState setState( std::vector<std::string> state, AQState newState );
	AQState getDeltaState(std::string temp);
	void setValueInt( int *a, std::string stringVal );
	void setValueDouble(GLfloat *a, std::string stringVal);
	void setColor(GLubyte &a, std::string value);
	void setValueBool( bool *, std::string );
	std::string getValue(std::string str);
	AQState defaultLoad( std::string temp );

protected:
	/// This is where the animations are stored.
	std::vector< Animation > animations;

public:
	Animated();
	virtual ~Animated();

	/// Use this for loading. Always call this or segfaults will occur if anything is done with it.
	bool loadAnimation( std::string path );

	/**
	Use to check if it has loaded,
	though you can check with the bool returned from the loadAnimation(std::string path) function.
	*/
	bool isLoaded();
	
	/// Use to switch between animations.
	bool switchAnim( int index );

	/**
	Use to iterate through as you render to the screen.
	Not recommended because if the screen needs to stop updates, this will still be updating,
	since its updating happens during rendering.
	*/
	void animate(World *world);

	/**
	Updates the animation.
	Prefered over animate(World *world) function.
	*/
	bool update();

	void update( int index );

	/**
	Call this after update to draw the properly.
	This function is meant as a pair with the update() function.
	*/
	void draw( World *world );
	void draw( World *world, int index );
	void draw( World *world, int index, GLfloat x, GLfloat y, bool drawFromCenter = false );
	void draw( World *world, GLfloat x, GLfloat y, bool drawFromCenter = false );

	void draw( World *world, AnimIter *iter, GLfloat x = 0.f, GLfloat y = 0.f, bool drawFromCenter = false );
	void draw( World *world, AnimIter *iter, AQState delta, GLfloat x = 0.f, GLfloat y = 0.f);


	/// Use this to add an animation properly to this object's vector of Animations.
	void addAnimation( Animation anim );

	/**
	 *\return The vector of animations that this object is storing.
	 */
	std::vector< Animation > getAnims();

	/**
	 *\param index = The index of vector of animations corresponding to the animation to retrieve.
	 *\return A pointer to the specified animation object.
	 */
	Animation* getAnim( int index );

	/**
	 * Pauses an animation.
	 *\param index = The index of the animation to be paused.
	 */
	void pause( int index );
	void pause();

	/**
	 * Unpauses an animation.
	 *\param index = The index of the animation to be unpaused.
	 */
	void start( int index );
	void start();

	/**
	 * Resets an animation back to its initial frame.
	 *\param index = The index of the animation to be reset.
	 */
	void reset( int index );
	void reset();

	/**
	 *\return An integer corresponding to the current index of the animation.
	 */
	GLuint getCurrAnim();

	/**
	 *\return The amount of animations stored in this object.
	 */
	int getSize();

	LFRect getDimensions( int index = 0 );
	LFRect getDimensionsScaled( int index = 0 );

	LFRect getPos( int index = -1, int animationIndex = -1 );
	std::string getPath();

};

#endif