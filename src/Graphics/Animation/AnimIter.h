#ifndef ANIMITER_H
#define ANIMITER_H

#include "AnimationIter.h"
#include <vector>
#include "../Drawing/AQState.h"

class Animated;

class AnimIter{
private:
	unsigned int currAnim;
	Animated* anim;
	std::vector<AnimationIter> animationIters;
	LFRect offset;
	bool drawFromCenter;
	AQState delta;
	bool loaded;
public:
	AnimIter();
	bool init( Animated* animated );

	/**
	 * Pauses an animation.
	 *\param index = The index of the animation to be paused.
	 */
	void pause( int index );
	void pause();

	/**
	 * Unpauses an animation.
	 *\param index = The index of the animation to be unpaused.
	 */
	void start( int index );
	void start();

	/**
	 * Resets an animation back to its initial frame.
	 *\param index = The index of the animation to be reset.
	 */
	void reset( int index );
	void reset();

	bool switchAnim( unsigned int index );

	/**
	 * \return Returns true when the animation has finished its loop.
	 */
	bool update();
	bool update( unsigned int index );

	bool setAnimationIters();

	void setLoc( unsigned int index );
	
	void setOffset( LFRect box );
	void setOffset( GLfloat x = 0.f, GLfloat y = 0.f, GLfloat w = 0.f, GLfloat h = 0.f );
	LFRect getOffset();

	bool isLoaded();

	void setDrawFromCenter( bool drawFromCenter );
	bool getDrawFromCenter();

	unsigned int getLoc();

	AnimationIter* getAnim( int index );
	AnimationIter* getCurrAnim();

	unsigned int getSize();

	void draw( World *world, GLfloat x, GLfloat y );
	void draw( World *world );
	LFRect getPos( int index = -1 );

	void addDelta( AQState delta );
};

#endif