#ifndef ANIMATIONITER_H
#define ANIMATIONITER_H

class World;

class AnimationIter{
private:
	unsigned int location;
	bool paused;
	unsigned int size;
	bool loop;
	int startLoop;
	int finishLoop;
public:
	AnimationIter();
	bool init( unsigned int size, bool loop = false, int startLoop = 0, int finishLoop = -1 );
	/**
	You can call reset to reset the animation back to it's beginning.
	*/
	void reset();

	/**
	Use state to unpause the animation.
	*/
	void start();

	/**
	Stop pauses the animation.
	*/
	void stop();

	/**
	 * \return Returns true when the animation has finished its loop.
	 */
	bool update();

	void setSize( unsigned int size );
	unsigned int getSize();

	void setLoop( bool looping, int startLoop = 0, int finishLoop = -1 );
	bool isLoop();

	bool isPaused();

	unsigned int getLoc();
};

#endif