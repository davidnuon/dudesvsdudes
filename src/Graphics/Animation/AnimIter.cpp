#include "Animated.h"
#include "AnimIter.h"

AnimIter::AnimIter(){
	currAnim = 0;
	anim = NULL;
	delta.drawFromCenter = false;
	loaded = false;
	setOffset();
}

bool AnimIter::init( Animated* animated ){
	
	if( animated->isLoaded() == false || animated->getSize() <= 0 ){
		printf("Trying to load AnimIter from non-loaded Animated object.\n");
		return false;
	}

	delta.clear();
	
	anim = animated;
	if( setAnimationIters() == false ){
		return false;
	}

	loaded = true;
	return true;
}

bool AnimIter::setAnimationIters(){
	animationIters.clear();
	for( int i = 0; i < anim->getSize(); i++ ){
		AnimationIter iter;
		Animation* temp = anim->getAnim( i );
		if( iter.init( temp->getSize(), temp->isLoop() ) == false ){
			return false;
		}
		animationIters.push_back( iter );
	}

	return true;
}

void AnimIter::setLoc(unsigned int index){
	if(index < anim->getSize()){
		currAnim = index;
	}
}

void AnimIter::setOffset( LFRect box ){
	offset = box;
}

void AnimIter::setOffset( GLfloat x, GLfloat y, GLfloat w, GLfloat h ){
	offset.x = x;
	offset.y = y;
	offset.w = w;
	offset.h = h;
}

LFRect AnimIter::getOffset(){
	return offset;
}

bool AnimIter::isLoaded(){
	return loaded;
}

void AnimIter::setDrawFromCenter( bool drawFromCenter ){
	delta.drawFromCenter = drawFromCenter;
}

bool AnimIter::getDrawFromCenter(){
	return delta.drawFromCenter;
}

unsigned int AnimIter::getLoc(){
	return currAnim;
}

bool AnimIter::switchAnim( unsigned int index ){
	//Safety check.
	if( index >= getSize() ){
		printf("Warning: index out of range.\n\tbool AnimIter::switchAnim( int index );\n\tIndex is: %d\nError From %s\n", index, anim->getPath().c_str());
		//Returns false if didn't successfully switch.
		return false;
	}

	//Reset current animation before switching to the new one.
	getAnim( currAnim )->reset();
	//Switch to new Animation.
	currAnim = index;
	return true;
}

AnimationIter* AnimIter::getAnim( int index ){
	if( index >= 0 && index < getSize() )
		return &animationIters[ index ];
	return NULL;
}

AnimationIter* AnimIter::getCurrAnim(){
	return &animationIters[ currAnim ];
}

void AnimIter::pause( int index ){
	getAnim( index )->stop();
}

void AnimIter::pause(){
	getAnim( currAnim )->stop();
}

void AnimIter::start( int index ){
	getAnim( index )->start();
}

void AnimIter::start(){
	getAnim( currAnim )->start();
}

void AnimIter::reset( int index ){
	getAnim( index )->reset();
}

void AnimIter::reset(){
	getAnim( currAnim )->reset();
}

bool AnimIter::update(){
	return animationIters[ currAnim ].update();
}

bool AnimIter::update( unsigned int index ){
	if( index < anim->getSize() ){
		return animationIters[ index ].update();
	}
	return true;
}

unsigned int AnimIter::getSize(){
	return animationIters.size();
}

void AnimIter::draw( World *world, GLfloat x, GLfloat y ){
	if( anim == NULL ){
		printf("AnimIter trying to draw to NULL Animated object.\nError from %s\n", anim->getPath().c_str());
		printf("\tInstead, nothing will be drawn.\nError from %s\n", anim->getPath().c_str());
		return;
	}
	anim->draw( world, this, delta, x, y );
}

void AnimIter::draw( World *world ){
	draw( world, offset.x, offset.y );
}

LFRect AnimIter::getPos( int index ){
	if( anim == NULL ){
		LFRect temp;
		return temp;
	}
	if( index <= 0 || index >= getSize() ){
		index = currAnim;
	}
	return anim->getPos( index, animationIters[ index ].getLoc() );
}

void AnimIter::addDelta( AQState delta ){
	this->delta = delta;
}