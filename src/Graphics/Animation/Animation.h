/**
\class Animation

\author Shane Satterfield

\date 2013/03/02

\brief

This class stores the data for animations. It's wrapper is the Animated class.

*/
#ifndef ANIMATION_H
#define ANIMATION_H

#include "../Drawing/AQState.h"

class Animation{
private:
	/// True if the animation is currently paused. Initialized as false.
	bool paused;

	/// Corresponds to the current location in the animation.
	int location;

	/// Holds whether or not the animation should loops.
	bool loop;
	int startLoop;
	int finishLoop;

	/// The AQStates corresponding to the animation are stored in this vector.
	std::vector<AQState> anim;
public:
	/**
	Default constructor.

	\param loop = Sets the loop variable value.
	*/
	Animation( bool loop = false, int startLoop = 0, int finishLoop = -1 );

	/**
	Optional Deep Copy Construtcor.

	\param newAnim = Copies the content of this AQState vector to this animation.
	\param loop = Sets the loop variable value.
	*/
	Animation( std::vector<AQState> newAnim, bool loop = false, int startLoop = 0, int finishLoop = -1 );

	/**
	You can call reset to reset the animation back to it's beginning.
	*/
	void reset();

	/**
	Use state to unpause the animation.
	*/
	void start();

	/**
	Stop pauses the animation.
	*/
	void stop();

	/**
	Sets loop.

	\param loop = The value to set the loop variable to.
	*/
	void setLoop(bool loop);

	void setLoopBounds(int startLoop, int finishLoop);
	/**
	Call this to return the current AQState and move to iterated through the animation.
	If it's a looping animation, this will loop, if not it will halt propperly.

	This iterates through the animation.

	\return The current AQState and moves to the next AQState.
	*/
	AQState next();

	/**
	Call this to update the current animation to it's correct next step.
	Typically you would follow this function call with a call to getCurr()
	*/
	bool update();

	/**
	Returns the AQState at the current location.
	Typicall is called after update() is called.

	\return The current AQState.
	*/
	AQState getCurr();

	/**
	Returns the AQState from the given index;

	\param index = The index of the AQState to be returned.

	\return The AQState at the index corresponding to the parameter.
	*/
	AQState getIndex( int index );

	/**
	\return The amount of AQStates in the animation object.
	*/
	int getSize();

	/**
	\return True if this animation is set to loop.
	*/
	bool isLoop();

	/**
	\return A C++ vector of AQStates that this animation object holds.
	*/
	std::vector<AQState> getAnim();

	/**
	Sets the animation.

	\param newAnim = Deep copies the animation object into this one.
	*/
	void setAnimation( Animation newAnim );

	/**
	Sets the animation.

	\param newAnim = Sets the animation through a vector of AQStates.
	*/
	void setAnimation( std::vector<AQState> newAnim );

	/**
	Add a new AQState to the vector of AQStates.

	\param newState = The AQState to be added to the AQState vector.
	*/
	void add( AQState newState );

	unsigned int getSpriteIndex( int index = -1 );

	bool isPaused();

	LFRect getBox( int index = -1 );
};

#endif