#include "Deck.h"

Deck::Deck( int numOfLayers ){
	/*
	Voodoo magic.
	layers.resize( numOfLayers );
	*/
	layerAmount = numOfLayers;
	layers = new Layer[ numOfLayers ];
}

Deck::~Deck(){
	delete [] layers;
}

void Deck::render(){
	for(int i = 0; i < layerAmount; i++){
		layers[i].render();
	}
}

void Deck::addToLayer(AQ* quadToDraw){
	//This is a check to make sure that the layer variable is within the right bounds.
	if( quadToDraw == NULL ){
		printf("Deck Error: Attemping to add NULL AQ from Deck to Layer.\n");
	}
	else if( quadToDraw->getState().layer >= 0 && quadToDraw->getState().layer < layerAmount ){
		layers[ quadToDraw->getState().layer ].add( quadToDraw );
	}
}