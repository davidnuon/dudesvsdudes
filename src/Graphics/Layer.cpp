#include "Layer.h"

Layer::Layer(){
	heap.clear();
}

void Layer::add(AQ* newAQ){
	heap.push( newAQ );
}

void Layer::render(){
	while( heap.getSize() > 0 ){
		AQ* temp = heap.pop();
		temp->render();
		delete temp;
	}
}