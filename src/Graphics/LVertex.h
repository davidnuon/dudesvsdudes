#ifndef LVERTEX_H
#define LVERTEX_H

#include "../System/LOpenGL.h"

struct LVertex{
	GLfloat x;
	GLfloat y;
};

#endif