#ifndef TESTPHYSICS_H
#define TESTPHYSICS_H

#include "Physics.h"

class TestPhysics: public Physics{
	void update(GameObject *self, World *world);
};

#endif