#include "UnitPhysics.h"
#include "../../GameObjects/GameObject.h"
#include "../../GameObjects/Unit.h"
#include "../../System/LOpenGL.h"
#include <cmath>


UnitPhysics::UnitPhysics() {
	deltaX = 1.5;
	deltaY = 1;
	targetX = 600;
	targetY = 400;
}

GLfloat UnitPhysics::distance(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2) {
	return abs(x2-x1) + abs(y2-y1);
}

GLfloat UnitPhysics::trueDistance(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2) {
	return sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
}

void UnitPhysics::moveTo(GLfloat x, GLfloat y, Unit *self) {
	targetX = x;
	targetY = y;
	GLfloat dist = trueDistance(x,y,self->x,self->y);
	deltaX = (targetX - self->x)/dist * self->speed;
	deltaY = (targetY - self->y)/dist * self->speed;
	self->atTarget = false;
}

void UnitPhysics::teleportTo(GLfloat x, GLfloat y, Unit *self) {
	deltaX = 0;
	deltaY = 0;
	self->x = x;
	self->y = y;
	self->atNextLocation();
}

void UnitPhysics::update(GameObject *self, World *world) {
	Unit * selfCast = dynamic_cast<Unit*> (self);
	if(selfCast == NULL) return;

	if(selfCast->atTarget == true) return;

	if(trueDistance(selfCast->x,selfCast->y,targetX,targetY) <= selfCast->speed * selfCast->speedBuff) {
		selfCast->x = targetX;
		selfCast->y = targetY;
		selfCast->atNextLocation();
	} else {
		selfCast->x += deltaX * selfCast->speedBuff;
		selfCast->y += deltaY * selfCast->speedBuff;
	}
}