/**
 *\class Physics
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * This is the physics component.
 * Use it for collision detectiona and other physics related things.
 *
 */
#ifndef PHYSICS_H
#define PHYSICS_H

#include "../../GameObjects/GameObject.h"
#include "../../System/World.h"

/**
 * Physics component used mainly for checking collisions.
 */
class Physics{
public:

	/**
	 * Update function.
	 *\param self = GameObject of Self.
	 *\param world = Pointer to World object to get information from.
	 */
	virtual void update(GameObject *self, World *world) = 0;
};

#endif