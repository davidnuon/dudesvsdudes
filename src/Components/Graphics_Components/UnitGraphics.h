#ifndef UNITGRAPHICS_H
#define UNITGRAPHICS_H

#include "Graphics.h"
#include "../../GameObjects/UnitState.h"
#include "../../Graphics/Animation/AnimIter.h"
#include "../../System/LOpenGL.h"
class UnitGraphics : public Graphics{
private:
	AnimIter sprite;
	UnitState direction;
	UnitState state;
	GLfloat x,y;

public:
	/**
	 * Constructor. Expects an already set up AnimIter.
	 *\param animation = animation of this unit
	 */
	UnitGraphics(AnimIter animation);

	/**
	 * Destructor, clears up sprite
	 */
	~UnitGraphics();

	bool init();

	/**
	 * Sets the state of the animation. 
	 *\param s = new animation state
	 */
	void setState(UnitState s);

	/**
	 *\return the current state of the animation
	 */
	UnitState getState();

	/**
	 * sets position
	 */
	void setPosition(GLfloat x, GLfloat y);

	/**
	 * move iterator to next frame
	 */
	void update();

	/**
	 * renders the current frame
	 *\param world = world to render to
	 */
	void render(World *world);
};

#endif