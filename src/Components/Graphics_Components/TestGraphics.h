#ifndef TESTGRAPHICS_H
#define TESTGRAPHICS_H

#include "Graphics.h"
#include "../../Graphics/Animation/Animated.h"

class TestGraphics: public Graphics{
private:
	Animated *anim;
public:
	virtual ~TestGraphics();
	bool init();
	void update();
	void render(World *world);
};

#endif