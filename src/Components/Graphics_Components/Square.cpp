#include "Square.h"
#include "../../System/World.h"
#include <iostream>

bool Square::init(){

	state.x       =  10.f;	state.y 		=  10.f;
	state.w       =  25.f;	state.h 		=  25.f;

	state.color.r =   255;	state.color.g 	=   0;
	state.color.b =   0;	state.color.a 	=   63;

	state.text = "Square";

	state.layer = 1;
	state.priority = 0;
	return true;
}

void Square::setState(AQState a) {
	state = a;
}

AQState Square::getState() {
	return state;
}
void Square::update(){

};

void Square::render(World *world){
	//std::cout << "render\n";
	quad.pushToDeck( state, world );
	quad.render(state);
}