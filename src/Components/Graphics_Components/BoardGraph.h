/**
 *\class BoardGraph
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * This is a graphics component for test animations.
 *
 */
#ifndef BOARDGRAPH_H
#define BOARDGRAPH_H

#include "../../Graphics/Drawing/Basic_Quad.h"
#include "../../System/Color.h"
#include "Graphics.h"
#include "GraphState.h"
#include "../../GameObjects/Point.h"
#include "../../Graphics/Drawing/SpriteSheet.h"

class BoardGraph: public Graphics{
private:
	Basic_Quad bq;
	GraphState gState;
	int states;
public:
	BoardGraph();
	//bool init();
	//bool init(GraphState);
	//void update();
	//void render( World *world );

	
	/**
	 * Initializes the graphics component.
	 *
	 *\return False if error initializing.
	 */
	bool init();

	/**
	 * Initializes the graphics component.
	 *
	 *\return False if error initializing.
	 */
	bool init(GraphState);

	/**
	 * Function for updating.
	 */
	void update();

	/**
	 * Function for rendering to the screen.
	 *
	 *\return world = Pointer to the world object that this graphics component will draw to.
	 */
	void render(World *world);

	void setColor(Color c, Point p);

	void pointToDisplay(Point p, GLfloat &x, GLfloat &y);
	Point displayToPoint(GLfloat x, GLfloat y);
};

#endif