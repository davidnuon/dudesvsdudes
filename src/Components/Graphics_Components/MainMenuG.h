/**
 *\class MainMenuG
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * This is the Grahpics component corresponding to the Main Menu.
 *
 */
#ifndef MAINMENUG_H
#define MAINMENUG_H

#include "Graphics.h"
#include "../../Graphics/Animation/Animated.h"
#include "../../Graphics/Drawing/Basic_Quad.h"

class MainMenuG: public Graphics{
private:
	/**
	 *The animation object corresponding to the logo.
	 */
	Animated *logo;

	/**
	 *The Basic_Quad object corresponding to the colored background.
	 */
	Basic_Quad *bq;
public:
	/**
	 *Deconstructor.
	 */
	virtual ~MainMenuG();

	/**
	 *Initializes the graphics component.
	 *
	 *\return False if error initializing.
	 */
	bool init();

	/**
	 *Function for updating.
	 */
	void update();

	/**
	 *Function for rendering to the screen.
	 *
	 *\return world = Pointer to the world object that this graphics component will draw to.
	 */
	void render(World *world);
};

#endif