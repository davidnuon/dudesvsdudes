#include "BoardGraph.h"
#include "../../System/LOpenGL.h"
#include "../../System/Color.h"
#include <iostream>



BoardGraph::BoardGraph() {
	states = 0;
}

bool BoardGraph::init(){
	states = 2;
	AQState state;
	state.layer = 1;
	state.x = 20.f;	state.y = 200.f;	
	state.w = 50.f; state.h = 50.f;
	
	bq.applyState( state );

	state.x = 200.f;	 state.y = 20.f;	
	state.w = 100.f; 	 state.h = 100.f;
	
	bq.applyState( state );


	return true;
}


bool BoardGraph::init(GraphState gState){
	this->gState = gState;
	if(gState.horzCount < 1 || gState.vertCount < 1) return false;

	states = 1 + gState.horzCount * gState.vertCount;
	AQState state;
	state.x = gState.x; 	state.y = gState.y;
	state.w = gState.width; state.h = gState.height;
	state.color = gState.line;

	bq.applyState( state );

	state.color = gState.background;
	GLfloat x, y;
	GLfloat deltaX, deltaY;
	x = gState.x + gState.thickness;
	y = gState.y + gState.thickness;
	deltaX = (gState.width - (gState.thickness * (gState.horzCount + 1)))/
				(gState.horzCount);
	deltaY = (gState.height - (gState.thickness * (gState.vertCount + 1)))/
				(gState.vertCount);
	state.w = deltaX;
	state.h = deltaY;
	deltaX += gState.thickness;
	deltaY += gState.thickness;
	state.y = y;
	state.x = x;


	for(int i=0; i<gState.horzCount; i++, state.x+= deltaX) {
		for(int j=0; j<gState.vertCount; j++, state.y+= deltaY) {
			bq.applyState( state );
		}
		state.y = y;
	}
	return true;
}

void BoardGraph::update(){

}

void BoardGraph::render( World *world ){
	for(int i=1; i<states; i++)
		bq.pushToDeck( i, world );
}

void BoardGraph::setColor(Color c, Point p) {
	if(p.x < 0 || p.y < 0 || p.x >= gState.horzCount || p.y >= gState.vertCount)
		return;
	int index = p.y + p.x * gState.vertCount + 1;
	AQState state = bq.getState(index);
	state.color = c;
	bq.updateState(index, state);
}

void BoardGraph::pointToDisplay(Point p, GLfloat &x, GLfloat &y) {
	if(p.x < 0 || p.y < 0 ||
		p.x >= gState.horzCount || p.y >= gState.vertCount) return;

	GLfloat deltaX, deltaY;
	deltaX = (gState.width - (gState.thickness * (gState.horzCount + 1)))/
				(gState.horzCount);
	deltaY = (gState.height - (gState.thickness * (gState.vertCount + 1)))/
				(gState.vertCount);
	x = gState.x + gState.thickness + p.x * (deltaX+gState.thickness) + deltaX/2;
	y = gState.y + gState.thickness + p.y * (deltaY+gState.thickness) + deltaY/2;
}

Point BoardGraph::displayToPoint(GLfloat x, GLfloat y) {
	Point p(-1,-1);
	//if(x < gState.x || y < gState.y) return p;
	//if(x > gState.x + gState.width || y > gState.y + gState.height) return p;

	GLfloat deltaX, deltaY;
	deltaX = (gState.width)/
				(gState.horzCount);
	deltaY = (gState.height)/
				(gState.vertCount);

	x -= gState.x;
	y -= gState.y;
	p.x = (int) (x/deltaX);
	p.y = (int) (y/deltaY);
	if(p.x < 0)
		p.x = 0;
	else if(p.x >= gState.horzCount )
		p.x = gState.horzCount - 1;
	if(p.y < 0)
		p.y = 0;
	else if(p.y >= gState.vertCount )
		p.y = gState.vertCount - 1;
	
	//std::cout << "x: " << p.x << " y: " << p.y << "\n";
	return p;
}