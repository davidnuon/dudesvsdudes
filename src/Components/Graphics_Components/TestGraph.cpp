#include "TestGraph.h"

TestGraph::~TestGraph(){
	delete anim;
	delete anim2;
	delete explosions;
}

bool TestGraph::init(){
	AQState state;
	if( spsheet.open("res/Animations/testLoader.load") == false ){
		return false;
	}

	state.x = 200.f;	state.y = 220.f;	
	state.w = 200.f; 	state.h = 100.f;
	state.index = 0;
	
	spsheet.applyState( state );

	state.x = 10.f;	 	state.y = 280.f;	
	state.w = 200.f; 	state.h = 100.f;
	state.index = 1;
	
	spsheet.applyState( state );

	state.x = 200.f;	state.y = 250.f;	
	state.w = 200.f; 	state.h = 100.f;
	state.index = 2;
	
	spsheet.applyState( state );

	state.x = 20.f;	 	state.y = 300.f;	
	state.w = 200.f; 	state.h = 100.f;
	state.index = 3;
	
	spsheet.applyState( state );

	anim = new Animated;
	if( anim->loadAnimation("res/Animations/testAnimation.anim") == false ){
		return false;
	}

	anim2 = new Animated;
	if( anim2->loadAnimation("res/Animations/explosions.anim") == false ){
		return false;
	}

	explosions = new Animated;
	if( explosions->loadAnimation("res/Animations/explosions.anim") == false ){
		return false;
	}

	return true;
}

void TestGraph::update(){
	anim->update();
	anim2->update();
	explosions->update();
	std::cout << explosions->getDimensionsScaled().w << std::endl;
}

void TestGraph::render( World *world ){
	spsheet.pushToDeck(0, world);
	spsheet.pushToDeck(1, world);
	spsheet.pushToDeck(2, world);
	spsheet.pushToDeck(3, world);
	//anim->animate(world);
	anim->draw( world );

	anim2->draw( world, 350, -40 );
	explosions->draw(world);
}