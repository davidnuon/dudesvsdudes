/**
 *\class Square
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * This is a Graphics Component.
 * It is used to render a square.
 *
 */
#ifndef SQUARE_H
#define SQUARE_H

class World;
#include "Graphics.h"
#include "../../Graphics/Drawing/Basic_Quad.h"
#include "../../Graphics/Drawing/AQState.h"
#include "../../System/LOpenGL.h"

class Square: public Graphics{
private:
	/**
	 * Holds the information of this graphics component.
	 */
	AQState state;

	/**
	 * Basic_Quad of corresponding to this object.
	 */
	Basic_Quad quad;
public:
	/**
	 * Initializes the graphics component.
	 *
	 *\return False if error initializing.
	 */
	bool init();

	/**
	 * Function for updating.
	 */
	void update();
	
	void setState(AQState);
	AQState getState();

	/**
	 * Function for rendering to the screen.
	 *
	 *\return world = Pointer to the world object that this graphics component will draw to.
	 */
	void render(World *world);

};

#endif