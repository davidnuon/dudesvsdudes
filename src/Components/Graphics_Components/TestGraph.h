/**
 *\class TestGraph
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * This is a graphics component for test animations.
 *
 */
#ifndef TESTGRAPH_H
#define TESTGRAPH_H

#include "../../Graphics/Drawing/Basic_Quad.h"
#include "Graphics.h"
#include "../../Graphics/Drawing/SpriteSheet.h"
#include "../../Graphics/Animation/Animated.h"

class TestGraph: public Graphics{
private:
	SpriteSheet spsheet;
	Animated *anim;

	Animated *anim2;
	/**
	 * Animation object for the explosion animation.
	 */
	Animated *explosions;
public:
	virtual ~TestGraph();

	/**
	 * Initializes the graphics component.
	 *
	 *\return False if error initializing.
	 */
	bool init();

	/**
	 * Function for updating.
	 */
	void update();

	/**
	 * Function for rendering to the screen.
	 *
	 *\return world = Pointer to the world object that this graphics component will draw to.
	 */
	void render(World *world);
};

#endif