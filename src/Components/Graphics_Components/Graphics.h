/**
 *\class Graphics
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * This is the abstract Graphics component in the game.
 *
 */
#ifndef GRAPHICS_H
#define GRAPHICS_H

class World;

/// Abstract Component Class
class Graphics{
public:
	virtual ~Graphics();
	/**
	 *Abstract function for initialization.
	 */
	virtual bool init()               = 0;

	/**
	 *Abstract update function.
	 */
	virtual void update()             = 0;

	/**
	 *Abstract function for rendering to a world object.
	 *
	 *\param world = Pointer to the world object.
	 */
	virtual void render(World *world) = 0;
};

#endif