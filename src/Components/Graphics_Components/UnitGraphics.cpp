#include "UnitGraphics.h"

UnitGraphics::UnitGraphics(AnimIter animation) {
	sprite = animation;
	sprite.switchAnim(11);
	direction = N;
	state = STANDING;
	sprite.pause();
}

UnitGraphics::~UnitGraphics() {
}

bool UnitGraphics::init() {
	sprite.pause();
	return true;
}
void UnitGraphics::setState(UnitState s) {
	state = s;
	if(s >= N && s <= NW)
		direction = s;
	switch(state){
		case STANDING:
			sprite.pause();
			break;
		case N:
			sprite.start();
			sprite.switchAnim(9);
			break;
		case E:
			sprite.start();
			sprite.switchAnim(11);
			break;
		case S:
			sprite.start();
			sprite.switchAnim(8);
			break;
		case W:
			sprite.start();
			sprite.switchAnim(10);
			break;
		default:
			//sprite.start();
			break;
	}
	//todo: select frame and range of frames

}

UnitState UnitGraphics::getState() {
	return state;
}

void UnitGraphics::setPosition(GLfloat x, GLfloat y) {
	this->x = x;
	this->y = y;
}

void UnitGraphics::update() {
	sprite.update();
}

void UnitGraphics::render(World *world) {
	sprite.draw(world, x, y);
}