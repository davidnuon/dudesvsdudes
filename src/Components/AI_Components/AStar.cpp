#include "AStar.h"
#include "Node.h"
#include "NodeHeap.h"
#include "../../GameObjects/Point.h"
#include <cmath>
#include <iostream>

AStar::AStar() {
	pathFound = false;
	path = NULL;
	pathIndex = 0;
	pathLength = 0;
	nodeArray = NULL;
}

AStar::AStar(CostGrid grid, int w, int h) {
	this->grid = grid;
	width = w;
	height = h;
	pathFound = false;
	path = NULL;
	pathIndex = 0;
	pathLength = 0;
}

void AStar::setCostGrid(CostGrid grid) {
	this->grid = grid;
	width = grid.getWidth();
	height = grid.getHeight();
}



Point AStar::getEnd() {
	return end;
}

void AStar::setStart(Point s) {
	start = s;
}

void AStar::setEnd(Point e) {
	end = e;
}

bool AStar::findPath(Point start, Point end){
	this->start = start;
	this->end = end;

	return findPath();
}

bool AStar::hasNext() {
	return pathIndex < pathLength;
}

Point AStar::next() {
	if(hasNext()) return path[pathIndex++];
	std::cout << "Does not have next\n";
	return Point(-1,-1);
}

Point AStar::safeNext() {
	if(hasNext()) return path[pathIndex];
	std::cout << "Does not have next\n";
	return Point(-1,-1);
}

void AStar::debugPath() {
	std::cout << "Path length: " << pathLength << "\n";
	for(int i=0; i<pathLength; i++)
		std::cout << "\t" << path[i].x << ' ' << path[i].y << "\n";
}

bool AStar::findPath() {
	//variables
	Node *cur, *endNode;

	pathFound = false;

	//housekeeping
	pathIndex = 0;
	pathLength = 0;
	if(path != NULL) {
		delete[] path;
		path = NULL;
	}

	//test for obvious casses
	//std::cout << start.x << " " << start.y << "\n";
	//std::cout << end.x << " " << end.y << "\n";
	//make sure start and end are valid points
	if(start.x < 0 || start.x >= width || end.x < 0 || end.x >= width) return false;
	if(start.y < 0 || start.y >= height || end.y < 0 || end.y >= height) return false;
	if(isBlocked(start)) return false;
	if(isBlocked(end)) return false;

	//see if the start and end are the same
	if(start == end) {
		pathFound = true;
		pathIndex = 1;
		pathLength = 1;
		path = new Point[1];
		path[0] = end;
		return true;
	}

	//generate array of nodes
	nodeArray = new Node**[width];
	for(int i=0; i<width; i++) {
		nodeArray[i] = new Node*[height];
		for(int j=0; j<height; j++) {
			nodeArray[i][j] = new Node(i,j);
		}
	}

	//find end node
	endNode = nodeArray[end.x][end.y];

	//add start to open list
	cur = nodeArray[start.x][start.y];
	cur->whichList = Node::onOpen;
	//cur->parent = cur;
	openList.push(cur);

	//crazy stuff begins here
	while(!openList.isEmpty()) {
		cur = openList.pop();
		cur->whichList = Node::onClosed;

		//check if cur is end point
		if(cur->center == end) {
			pathFound = true;
			//create array to hold points in path
			pathLength = cur->pathLength;
			path = new Point[pathLength];
			//pathLength = cur->pathLength+1;
			//fill path with points
			for(int i = pathLength-1; i>=0; i--) {
				path[i] = cur->center;
				cur = cur->parent;
			}
			//std::cout << "Path length: " << pathLength << "\n";
			//debugPath();
			//clean up
			cleanUp();

			//report path found
			return true;
		}

		/* Visit surrounding nodes. If they are not on the open list, add them. 
		 * If they are on open list, update if new cost is less. If on closed list, ignore.
		 */
		Node *nextNode;
		int x,y;
		x = cur->center.x;
		y = cur->center.y;
		if(x-1 >=0) {
			nextNode = nodeArray[x-1][y];
			addToOpen(nextNode, cur, STRAIGHT);
		}
		if(x+1 < width) {
			nextNode = nodeArray[x+1][y];
			addToOpen(nextNode, cur, STRAIGHT);
		}
		if(y-1 >= 0) {
			nextNode = nodeArray[x][y-1];
			addToOpen(nextNode, cur, STRAIGHT);
		}
		if(y+1 < height) {
			nextNode = nodeArray[x][y+1];
			addToOpen(nextNode, cur, STRAIGHT);
		}
		if(x-1 >=0 && y-1 >= 0 && !(isBlocked(Point(x-1,y)) || isBlocked(Point(x,y-1)))) {
			nextNode = nodeArray[x-1][y-1];
			addToOpen(nextNode, cur, DIAGONAL);
		}
		if(x-1 >=0 && y+1 < height && !(isBlocked(Point(x-1,y)) || isBlocked(Point(x,y+1)))) {
			nextNode = nodeArray[x-1][y+1];
			addToOpen(nextNode, cur, DIAGONAL);
		}
		if(x+1 < width && y-1 >= 0 && !(isBlocked(Point(x+1,y)) || isBlocked(Point(x,y-1)))) {
			nextNode = nodeArray[x+1][y-1];
			addToOpen(nextNode, cur, DIAGONAL);
		}
		if(x+1 < width && y+1 < height && !(isBlocked(Point(x+1,y)) || isBlocked(Point(x,y+1)))) {
			nextNode = nodeArray[x+1][y+1];
			addToOpen(nextNode, cur, DIAGONAL);
		}

	}

	//no path was found
	cleanUp();
	return false;
}

int AStar::getDistance(Point a, Point b) {
	int x,y;
	x = a.x-b.x;
	y = a.y-b.y;
	return (int) sqrt(x*x + y*y);
}

void AStar::addToOpen(Node *newNode, Node *parent, int dirCost) {
	if(newNode->whichList == Node::onClosed) return;
	if(isBlocked(newNode->center)) return;
	int gCost = dirCost * grid.get(newNode->center) + parent->gCost;
	int hCost = getDistance(newNode->center, end);
	int cost = gCost + hCost;
	if(newNode->whichList == Node::onNone) {
		newNode->gCost = gCost;
		newNode->hCost = hCost;
		newNode->cost = cost;
		newNode->parent = parent;
		newNode->pathLength = parent->pathLength + 1;
		newNode->whichList = Node::onOpen;
		openList.push(newNode);
	} else if(cost < newNode->cost) {
		newNode->gCost = gCost;
		newNode->hCost = hCost;
		newNode->cost = cost;
		newNode->parent = parent;
		newNode->pathLength = parent->pathLength + 1;
		newNode->whichList = Node::onOpen;
		openList.update(newNode);
	}
}

bool AStar::isBlocked(Point p) {
	return grid.get(p) == BLOCKED;
}

void AStar::cleanUp() {
	for(int i=0; i< width; i++) {
		for(int j=0; j < height; j++) {
			delete nodeArray[i][j];
		}
		delete[] nodeArray[i];
	}
	delete[] nodeArray;
	nodeArray = NULL;
	openList.erase();
}

