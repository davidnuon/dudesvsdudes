#ifndef PATHFINDERPOINTER_H
#define PATHFINDERPOINTER_H

#include "Pathfinder.h"
#include "../../GameObjects/GameObjectPtr.h"

class PathfinderPointer {
public:
	GameObjectPtr ptr;
	int priority;

	PathfinderPointer(GameObjectPtr p, int prio) {
		ptr = p;
		priority = prio;
	}
};

#endif