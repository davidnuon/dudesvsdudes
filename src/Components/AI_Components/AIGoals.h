#ifndef AIGOALS_H
#define AIGOALS_H

enum AIGoals {
	GOTO_RALLY_POINT,
	ATTACK,
	DEFEND_SELF
};

#endif