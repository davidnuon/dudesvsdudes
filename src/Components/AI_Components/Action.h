#ifndef ACTION_H
#define ACTION_H

#include "../../GameObjects/UnitState.h"

class Action {
public:
	UnitState type;
	int duration;
	int timeLeft;

	Action();

	Action(UnitState type, int duration);

	bool update();

	bool done();

};

#endif