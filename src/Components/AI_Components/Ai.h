/**
 *\class Ai
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * This is the abstract component for Artificial Intelligence in the game.
 *
 */
#ifndef AI_H
#define AI_H

#include "../../GameObjects/GameObject.h"
#include "../../System/World.h"

class Ai{
public:
	/**
	 * Abstract function for AI.
	 *
	 *\param self = Pointer to GameObject that has this AI.
	 *\param world = Pointer to a World object.
	 */
	virtual void update(GameObjectPtr self, World *world) = 0;
};

#endif