#ifndef TARGETFINDER_H
#define TARGETFINDER_H

//#include "../../GameObjects/Board.h"
//#include "../../GameObjects/Unit.h"
#include "../../GameObjects/Point.h"
#include "../../GameObjects/GameObjectPtr.h"
#include <list>

class Unit;
class Board;

class TargetFinder {
public:
	TargetFinder();

	/**
	 * Constructor
	 *\param b = Board to associate with
	 */
	TargetFinder(Board *b);

	/**
	 * Destructor, clears up pointers
	 */
	~TargetFinder();

	/**
	 * Set location to search from
	 * \param p = location to search from
	 */
	void setCenter(Point p);

	 /**
	  * Set range for search
	  *\param r = radius of new range
	  */
	void setRange(int r);

	/**
	 * Set team to search for
	 *\param t = Team to search for
	 */
	void setTeam(int t);

	/**
	 * searches a set amount of spaces until it finds a suitable unit
	 * returns null if nothing found
	 *\return Unit to attack
	 */
	GameObjectPtr find();

	/**
	 * searches adjacent squares for unint
	 *\return Unit to attack
	 */
	GameObjectPtr findNextTo(Point p);

	void print();
private:
	int x,y,range, team;
	std::list<Point> pointList;
	bool **visited;
	Point center;
	Board *board;
	static int checks;
};

#endif