#ifndef NODEHEAP_H
#define NODEHEAP_H

#include "Node.h"
#include "NodePointer.h"
#include "../../System/Heap.h"


class NodeHeap {
private:
	//Used for comparisons during sorting. Use when intializing with make_heap();
	bool comparison(Node *a, Node *b);

	//Heap where nodes are stored
	Heap<NodePointer> *heap;

	//Clears the heap, removing all nodes.
	void clear();
	
public:
	//Initialize heap
	NodeHeap();

	//Destroy heap
	~NodeHeap();

	//push node onto heap
	void push(Node*);

	//pop node with lowest cost
	Node* pop();

	//Gets the size of the heap.
	int getSize();

	//erase data in heap
	void erase();

	//Updates heap based on object that has changed
	void update(Node *n);

	//Return true if empty
	bool isEmpty();
};

#endif