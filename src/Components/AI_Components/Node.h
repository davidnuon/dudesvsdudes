#ifndef NODE_H
#define NODE_H

#include "../../GameObjects/Point.h"
#include <climits>

class Node {
public:
	int cost, gCost, hCost;
	Node* parent;
	Point center;
	int pathLength;

	enum onWhichList {onNone, onOpen, onClosed} whichList;

	Node();
	Node(int x, int y);
	Node(Point center);
	Node(Point center, int cost);
	~Node();

	static const int BLOCKED = INT_MAX;
};

#endif