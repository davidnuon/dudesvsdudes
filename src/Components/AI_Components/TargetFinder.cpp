#include "TargetFinder.h"
#include "../../GameObjects/Board.h"
#include "../../GameObjects/Unit.h"
#include "../../GameObjects/GameObject.h"
#include <cmath>
#include <vector>
#include <iostream>

int TargetFinder::checks = 3;

TargetFinder::TargetFinder() {
	board = NULL;
	x = 0;
	y = 0;
	range = 1;
	team = 0;
	center = Point(-1,-1);
	visited = NULL;
}

TargetFinder::TargetFinder(Board *b) {
	board = b;
	x = 0;
	y = 0;
	range = 1;
	team = 0;
	center = Point(-1,-1);
	/*
	visited = new bool*[board->getColumns()];
	for(int i=0; i< board->getColumns(); i++){
		visited[i] = new bool[board->getRows()];
		for(int j=0; j<board->getRows(); j++) {
			visited[i][j] = false;
		}
	}*/
}

TargetFinder::~TargetFinder() {
	/*
	if(visited != NULL) {
		for(int i=0; i<board->getRows(); i++)
			delete[] visited[i];
		delete[] visited;
	}*/
}

void TargetFinder::setCenter(Point p) {
	center = p;
	if(pointList.empty() && center != Point(-1,-1))
		pointList.push_back(center);
}

void TargetFinder::setRange(int r) {
	range = r;
}

void TargetFinder::setTeam(int t) {
	team = t;
}


GameObjectPtr TargetFinder::find() {
	//if(center == Point())
		//std::cout << "Center: (" << center.x << ',' << center.y << ")\n";
	for(int i=0; i<checks; i++) {
		Point curPoint = Point(center.x + x, center.y + y);
		GameObjectPtr curPtr = board->getGameObject(curPoint);
		Unit *cur = dynamic_cast<Unit*>(curPtr.getPtr());
		if(cur != NULL) {
			if(cur->getTeam() != team)
				return curPtr;
		}
		x++;
		if(x+abs(y)> range) {
			y++;
			if(y>range) {
				y = -range;
				x = 0;
			} else {
				x = abs(y)-range;
			}
		}
	}
	return GameObjectPtr();
}
/*
GameObjectPtr TargetFinder::find() {
	if(pointList.empty()) {
		//reset visited array
		for(int i=0; i<board->getColumns(); i++)
			for(int j=0; j<board->getRows(); j++)
				visited[i][j] = false;
		if(center != Point(-1,-1))
			pointList.push_back(center);
	} else {
		for(int i=0; i<checks && !pointList.empty(); i++) {
			Point curPoint = pointList.front();
			pointList.pop_front();
			GameObjectPtr dude = board->getGameObject(curPoint);
			Unit* dudePtr = dynamic_cast<Unit*>(dude.getPtr());
			if(dudePtr != NULL) {
				if(dudePtr->getTeam() == team)
					return dude;
			}
			//add surrounding nodes to list
			int x = curPoint.x;
			int y = curPoint.y;
			if(x-1 >=0 && !visited[x-1][y]) {
				pointList.push_back(Point(x-1,y));
				visited[x-1][y] = true;
			}
			if(y-1 >=0 && !visited[x][y-1]) {
				pointList.push_back(Point(x,y-1));
				visited[x][y-1] = true;
			}
			if(x+1 < board->getColumns() && !visited[x+1][y]) {
				pointList.push_back(Point(x+1,y));
				visited[x+1][y] = true;
			}
			if(y+1 < board->getRows() && !visited[x][y+1]) {
				pointList.push_back(Point(x,y+1));
				visited[x][y+1] = true;
			}
		}
	}
	return GameObjectPtr();
}*/

GameObjectPtr TargetFinder::findNextTo(Point p) {
	for(int i=-1; i<=1; i++)
		for(int j=-1; j<=1; j++){
			Point curPoint = Point(i+p.x,j+p.y);
			GameObjectPtr curPtr = board->getGameObject(curPoint);
			Unit *cur = dynamic_cast<Unit*>(curPtr.getPtr());
			if(cur != NULL) {
				if(cur->getTeam() != team)
					return curPtr;
			}
		}
	return GameObjectPtr();
}

void TargetFinder::print() {
	std::cout << "Center: (" << center.x << ',' << center.y << ")\n";
	std::cout << "Range: " << range << "\n";
}