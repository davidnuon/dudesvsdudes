#ifndef COSTGRID_H
#define COSTGRID_H

#include "../../GameObjects/Point.h"
#include <climits>
/**
 * The CostGrid class holds an array of movement costs for pathfinding.
 * The grids can be added together. They can also be a subset of the larger grid.
 */
class CostGrid {
public:
	/**
	 * Constructor, initializes everything to NULL
	 */
	CostGrid();

	/**
	 * Constructor, initializes grid to size and gives 
	 * every spot a value of 1
	 *\param width = width of grid
	 *\param height = height of grid
	 */
	CostGrid(int width, int height);

	/**
	 * Constructor, initializes grid to size with offset
	 * and gives every spot a value of 1
	 *\param width = width of grid
	 *\param height = height of grid
	 *\param offset = offset point
	 */
	CostGrid(int width, int height, Point offset);

	/**
	 * Copy constructor
	 */
	CostGrid(const CostGrid &other);

	/**
	 * Destructor, takes care of arrays
	 */
	~CostGrid();

	/**
	 * Copy and puplicate arrays
	 */
	CostGrid& operator=(const CostGrid &other);

	/**
	 * Adds other to this grid and returns the result
	 * If other is outside of this grid, nothing happens
	 *\param other = Grid to add 
	 *\return sum of grids
	 */
	CostGrid add(const CostGrid &other);

	/**
	 * Addds other to this grid, changing this grid
	 * If other is outside of this grid, nothing happens
	 *\param other = Grid to add
	 */
	CostGrid& addTo(const CostGrid &other);

	/**
	 * Set element
	 *\param p = point to set
	 *\param cost = value to set to
	 */
	void set(Point p, int cost);

	/**
	 * Set element relative to offset
	 \param p = point to set
	 *\param cost = value to set to
	 */
	void setRelative(Point p, int cost);

	/**
	 * Get element
	 *\param p = point to get
	 *\return value at p
	 */
	int get(Point p);

	/**
	 * Get element relative to offset
	 *\param p = point to get
	 *\return value at p
	 */
	int getRelative(Point p);

	/**
	 * Returns width of this grid
	 *\return width of this grid
	 */
	int getWidth();

	/**
	 * Returns height of this grid
	 *\return height of this grid
	 */
	int getHeight();

	/**
	 * Returns offset of this grid
	 *\return offset of this grid
	 */
	Point getOffset();

	/**
	 * prints grid to terminal for debuging
	 */
	void print();

	static const int NORMAL_COST = 1;
	static const int WALL = INT_MAX;

private:
	int width, height;
	Point offset;
	int **grid;

};

#endif