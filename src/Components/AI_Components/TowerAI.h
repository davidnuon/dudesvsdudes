#ifndef TOWERAI_H
#define TOWERAI_H

#include "UnitAI.h"

class TowerAI : public UnitAI {
public:
	TowerAI();
	virtual bool init(Board *b,int team, Unit *self);
	virtual void update();


};

#endif