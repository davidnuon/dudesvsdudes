#ifndef ASTAR_H
#define ASTAR_H

#include "Pathfinder.h"
#include "../../GameObjects/Point.h"
#include "Node.h"
#include "NodeHeap.h"
#include "CostGrid.h"
#include <climits>

class UnitAI;


class AStar : public Pathfinder {
public:
	//Default constructor
	AStar();

	//Constructor with tile cost array
	AStar(CostGrid grid, int w, int h);

	//Finds a path 
	bool findPath();

	//Finds a path
	bool findPath(Point start, Point end);

	//Return next point in path
	Point next();

	//Return next point without changing index
	Point safeNext();

	//Return true if there are still points in path
	bool hasNext();

	//Set new grid
	virtual void setCostGrid(CostGrid grid);

	//returns tile array,
	CostGrid getGrid();

	void setStart(Point s);

	void setEnd(Point e);
	
	//returns starting point
	Point getStart();

	//Return ending point
	Point getEnd();

	void debugPath();

private:
	//Array of walls and tile costs
	CostGrid grid;

	//remembers if a path has been found yet
	bool pathFound;

	//List of nodes that make up the path
	Point* path;

	//Index of current node in the path
	int pathIndex;

	//Length of path
	int pathLength;

	//Dimensions of grid
	int width, height;

	//Start and end points
	Point start, end;

	//Funtions
	//returns distance between two points
	int getDistance(Point a, Point b);

	//helper for finding path
	void addToOpen(Node *newNode, Node *parent, int dirCost);

	//return true if point is blocked
	bool isBlocked(Point);

	//clean up after looking for path
	void cleanUp();

	//costs of each leg
	static const int STRAIGHT = 10;
	static const int DIAGONAL = 14;

	static const int BLOCKED = INT_MAX;

	//grid of nodes
	Node ***nodeArray;

	//Heap of nodes
	NodeHeap openList;

	friend class UnitAI;
};

#endif