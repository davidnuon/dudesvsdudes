#include "Node.h"
#include "cstddef"
#include "../../GameObjects/Point.h"

Node::Node() {
	cost = 0;
	gCost = 0;
	hCost = 0;
	center = Point(0,0);
	parent = NULL;
	whichList = onNone;
	pathLength = 0;
}

Node::Node(int x, int y) {
	cost = 0;
	gCost = 0;
	hCost = 0;
	center = Point(x,y);
	parent = NULL;
	whichList = onNone;
	pathLength = 0;
}

Node::Node(Point center) {
	cost = 0;
	gCost = 0;
	hCost = 0;
	this->center = center;
	parent = NULL;
	whichList = onNone;
	pathLength = 0;
}

Node::Node(Point center, int cost) {
	this->cost = cost;
	gCost = 0;
	hCost = 0;
	this ->center = center;
	parent = NULL;
	whichList = onNone;
	pathLength = 0;
}

Node::~Node() {
	
}