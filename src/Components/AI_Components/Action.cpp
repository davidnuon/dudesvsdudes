#include "Action.h"
#include <iostream>

Action::Action() {
	type = STANDING;
	duration = 0;
	timeLeft = 0;
}

Action::Action(UnitState type, int duration) {
	this->type = type;
	this->duration = duration;
	timeLeft = duration;
}

bool Action::update() {
	//std::cout << timeLeft << '\n';
	if(timeLeft <= 0) return false;
	timeLeft--;
	return true;
}

bool Action::done() {
	return timeLeft == 0;
}