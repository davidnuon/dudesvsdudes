#ifndef TESTAI_H
#define TESTAI_H

#include "Ai.h"

class TestAi: public Ai{
	void update(GameObject *self, World *world);
};

#endif