#include "Dijkstra.h"
#include "CostGrid.h"
#include "../../System/Heap.h"

#include <vector>
#include <iostream>

using std::vector;

const int STRAIGHT = 10;
const int DIAGONAL = 14;

bool compare(Node &a, Node &b) {
	return a.cost < b.cost;
}

void dijkstra(CostGrid &grid, Point start) {
	Heap<Node> nodeHeap(compare);
	int width = grid.getWidth();
	int height = grid.getHeight();
	bool ** visited = new bool*[width];
	for(int i=0; i< width; i++){
		visited[i] = new bool[height];
		for(int j=0; j<height; j++) {
			visited[i][j] = false;
		}
	}

	start.x -= grid.getOffset().x;
	start.y -= grid.getOffset().y;

	nodeHeap.push(Node(start,grid.get(start)));

	while(nodeHeap.getSize() > 0) {
		Node cur = nodeHeap.pop();

		Node nextNode;
		int x,y;
		x = cur.center.x;
		y = cur.center.y;

		//if node has already been visited, ignore
		if(visited[x][y] == true)
			continue;
		//set to visited
		visited[x][y] = true;
		grid.set(Point(x,y),cur.cost);

		Point nextPoint;

		nextPoint = Point(x-1,y);
		if(nextPoint.x >=0 && visited[nextPoint.x][nextPoint.y] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + STRAIGHT * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x+1,y);
		if(x+1 < width && visited[nextPoint.x][nextPoint.y] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + STRAIGHT * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x,y-1);
		if(y-1 >= 0 && visited[nextPoint.x][nextPoint.y] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + STRAIGHT * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x,y+1);
		if(y+1 < height && visited[nextPoint.x][nextPoint.y] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + STRAIGHT * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x-1,y-1);
		if(x-1 >=0 && y-1 >= 0 && !(grid.get(Point(x-1,y)) == CostGrid::WALL || 
			 grid.get(Point(x,y-1)) == CostGrid::WALL)
			 && visited[nextPoint.x][nextPoint.y] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + DIAGONAL * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x-1,y+1);
		if(x-1 >=0 && y+1 < height && !(grid.get(Point(x-1,y)) == CostGrid::WALL || 
			 grid.get(Point(x,y+1)) == CostGrid::WALL)
			 && visited[nextPoint.x][nextPoint.y] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + DIAGONAL * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x+1,y-1);
		if(x+1 < width && y-1 >= 0 && !(grid.get(Point(x+1,y)) == CostGrid::WALL || 
			 grid.get(Point(x,y-1)) == CostGrid::WALL)
			 && visited[nextPoint.x][nextPoint.y] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + DIAGONAL * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x+1,y+1);
		if(x+1 < width && y+1 < height && !(grid.get(Point(x+1,y)) == CostGrid::WALL || 
			 grid.get(Point(x,y+1)) == CostGrid::WALL)
			 && visited[nextPoint.x][nextPoint.y] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + DIAGONAL * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}

	}


}

vector<Point> dijkstraFill(CostGrid grid, Point start, int count) {
	Heap<Node> nodeHeap(compare);
	vector<Point> list;
	int width = grid.getWidth();
	int height = grid.getHeight();
	start.x -= grid.getOffset().x;
	start.y -= grid.getOffset().y;

	if(start.x < 0 || start.x >= width || start.y < 0 || start.y >= height)
		return list;

	bool ** visited = new bool*[width];
	for(int i=0; i< width; i++){
		visited[i] = new bool[height];
		for(int j=0; j<height; j++) {
			visited[i][j] = false;
		}
	}


	nodeHeap.push(Node(start,grid.get(start)));

	while(nodeHeap.getSize() > 0 && count > 0) {
		Node cur = nodeHeap.pop();

		Node nextNode;
		int x,y;
		x = cur.center.x;
		y = cur.center.y;

		//if node has already been visited, ignore
		if(visited[x][y] == true)
			continue;
		//set to visited
		visited[x][y] = true;
		grid.set(Point(x,y),cur.cost);

		//add to list
		list.push_back(cur.center);
		//std::cout << cur.center.toString() << "\n";
		count--;

		Point nextPoint;
		nextPoint = Point(x-1,y);
		if(x-1 >=0 && visited[x-1][y] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			
			nextNode = Node(nextPoint, cur.cost + STRAIGHT * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x+1,y);

		if(x+1 < width && visited[x+1][y] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + STRAIGHT * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x,y-1);
		if(y-1 >= 0 && visited[x][y-1] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + STRAIGHT * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x,y+1);
		if(y+1 < height && visited[x][y+1] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + STRAIGHT * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x-1,y-1);
		if(x-1 >=0 && y-1 >= 0 && !(grid.get(Point(x-1,y)) == CostGrid::WALL || 
			 grid.get(Point(x,y-1)) == CostGrid::WALL)
			 && visited[x-1][y-1] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + DIAGONAL * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x-1,y+1);
		if(x-1 >=0 && y+1 < height && !(grid.get(Point(x-1,y)) == CostGrid::WALL || 
			 grid.get(Point(x,y+1)) == CostGrid::WALL)
			 && visited[x-1][y+1] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + DIAGONAL * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x+1,y-1);
		if(x+1 < width && y-1 >= 0 && !(grid.get(Point(x+1,y)) == CostGrid::WALL || 
			 grid.get(Point(x,y-1)) == CostGrid::WALL)
			 && visited[x+1][y-1] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + DIAGONAL * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}
		nextPoint = Point(x+1,y+1);
		if(x+1 < width && y+1 < height && !(grid.get(Point(x+1,y)) == CostGrid::WALL || 
			 grid.get(Point(x,y+1)) == CostGrid::WALL)
			 && visited[x+1][y+1] == false 
			&& grid.get(nextPoint) != CostGrid::WALL) {
			nextNode = Node(nextPoint, cur.cost + DIAGONAL * grid.get(nextPoint));
			nodeHeap.push(nextNode);
		}

	}
	return list;
}

Point nextPoint(Board *board, CostGrid &grid, Point start, Point reject) {
	Point next = start;
	int x = start.x;
	int y = start.y;
	int min = grid.get(start);
	int cur;
	Point curPoint;
	curPoint = Point(x +1, y );
	cur = grid.get(curPoint);
	if(cur < min && cur != -1 && board->getGameObject(curPoint).getPtr() == NULL
		&& curPoint != reject) {
		min = cur;
		next = curPoint;
	}
	curPoint = Point(x +1, y +1);
	cur = grid.get(curPoint);
	if(cur < min && cur != -1 && board->getGameObject(curPoint).getPtr() == NULL
		&& curPoint != reject) {
		min = cur;
		next = curPoint;
	}
	curPoint = Point(x +1, y -1);
	cur = grid.get(curPoint);
	if(cur < min && cur != -1 && board->getGameObject(curPoint).getPtr() == NULL
		&& curPoint != reject) {
		min = cur;
		next = curPoint;
	}
	curPoint = Point(x , y +1);
	cur = grid.get(curPoint);
	if(cur < min && cur != -1 && board->getGameObject(curPoint).getPtr() == NULL
		&& curPoint != reject) {
		min = cur;
		next = curPoint;
	}
	curPoint = Point(x , y -1);
	cur = grid.get(curPoint);
	if(cur < min && cur != -1 && board->getGameObject(curPoint).getPtr() == NULL
		&& curPoint != reject) {
		min = cur;
		next = curPoint;
	}
	curPoint = Point(x -1, y );
	cur = grid.get(curPoint);
	if(cur < min && cur != -1 && board->getGameObject(curPoint).getPtr() == NULL
		&& curPoint != reject) {
		min = cur;
		next = curPoint;
	}
	curPoint = Point(x -1, y +1);
	cur = grid.get(curPoint);
	if(cur < min && cur != -1 && board->getGameObject(curPoint).getPtr() == NULL
		&& curPoint != reject) {
		min = cur;
		next = curPoint;
	}
	curPoint = Point(x -1, y -1);
	cur = grid.get(curPoint);
	if(cur < min && cur != -1 && board->getGameObject(curPoint).getPtr() == NULL
		&& curPoint != reject) {
		min = cur;
		next = curPoint;
	}
	//if(next == start)
	//	std::cout << "staying put\n";
	//std::cout << next.toString() << '\n';

	return next;
}

