#include "TowerAI.h"
#include <iostream>
using std::cout;


TowerAI::TowerAI() : UnitAI() {

}

bool TowerAI::init(Board *b,int team, Unit *self) {
	//bool success =  UnitAI::init(b,team,self);
	this->self = self;
	finder = TargetFinder(b);
	finder.setRange(self->attackRange);
	finder.setCenter(self->location);
	finder.setTeam(team);
	primary = Action();
	return true;
}

void TowerAI::update() {
	if(primary.update()) return;
	if(self->attackRange==1) return;
	//cout << "updating\n";
	finder.setRange(self->attackRange);
	finder.setCenter(self->location);
	finder.setTeam(self->team);
	Unit* targetCast = dynamic_cast<Unit*> (target.getPtr());
	if(targetCast != NULL) {
		//cout << "attacking\n";
		if(self->location.distance(targetCast->location) <= self->attackRange) {
			if(!attack(targetCast)) {
				//cout << "attack faild\n";
				GameObjectPtr temp = finder.find();
				if(temp.getPtr() != NULL)
					target = temp;
			}
		} else {
			finder.setCenter(self->location);
			target = finder.find();
		}
	} else {
		finder.setCenter(self->location);
		target = finder.find();
		//cout << "finding\n";
		//finder.print();
	}
}