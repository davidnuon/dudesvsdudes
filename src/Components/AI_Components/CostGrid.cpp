#include "CostGrid.h"
#include "../../GameObjects/Point.h"
#include <cstdlib>
#include <algorithm>
#include <iostream>

CostGrid::CostGrid() {
	width = 0;
	height = 0;
	offset = Point(0,0);
	grid = NULL;
}

CostGrid::CostGrid(int width, int height) {
	this->width = width;
	this->height = height;
	offset = Point(0,0);
	grid = new int*[width];
	for(int i=0; i<width; i++) {
		grid[i] = new int[height];
		for(int j=0; j<height; j++) {
			grid[i][j] = 1;
		}
	}
}

CostGrid::CostGrid(int width, int height, Point offset) {
	this->width = width;
	this->height = height;
	this->offset = offset;
	grid = new int*[width];
	for(int i=0; i<width; i++) {
		grid[i] = new int[height];
		for(int j=0; j<height; j++) {
			grid[i][j] = 1;
		}
	}
}

CostGrid::CostGrid(const CostGrid &other) {
	width = other.width;
	height = other.height;
	offset = other.offset;
	if(other.grid == NULL) {
		grid = NULL;
	} else {
		grid = new int*[width];
		for(int i=0; i<width; i++) {
			grid[i] = new int[height];
			for(int j=0; j<height; j++) {
				grid[i][j] = other.grid[i][j];
			}
		}
	}
}

CostGrid::~CostGrid() {
	if(grid != NULL) {
		for(int i=0; i<width; i++) {
			delete[] grid[i];
		}
		delete[] grid;
	}
}

CostGrid& CostGrid::operator=(const CostGrid &other) {
	if(this == &other)
		return *this;
	width = other.width;
	height = other.height;
	offset = other.offset;
	//delete array
	if(grid != NULL) {
		for(int i=0; i<width; i++) {
			delete[] grid[i];
		}
		delete[] grid;
		grid = NULL;
	}
	if(other.grid != NULL) {
		//create new array and fill
		grid = new int*[width];
		for(int i=0; i<width; i++) {
			grid[i] = new int[height];
			for(int j=0; j<height; j++) {
				grid[i][j] = other.grid[i][j];
			}
		}
	}
	return *this;
}

CostGrid CostGrid::add(const CostGrid &other) {
	CostGrid temp = *this;
	int startx = std::max(offset.x, other.offset.x);
	int endx = std::min(offset.x + width, other.offset.y + other.width);
	int starty = std::max(offset.y, other.offset.y);
	int endy = std::min(offset.y = height, other.offset.y + other.height);

	for(int i=startx; i<endx; i++) {
		for(int j=starty; j<endy; j++) {
			temp.grid[i-offset.x][j-offset.y] = other.grid[i-other.offset.x][j-other.offset.y];
		}
	}

	return temp;
}

CostGrid& CostGrid::addTo(const CostGrid &other) {
	int startx = std::max(offset.x, other.offset.x);
	int endx = std::min(offset.x + width, other.offset.y + other.width);
	int starty = std::max(offset.y, other.offset.y);
	int endy = std::min(offset.y = height, other.offset.y + other.height);

	for(int i=startx; i<endx; i++) {
		for(int j=starty; j<endy; j++) {
			grid[i-offset.x][j-offset.y] = other.grid[i-other.offset.x][j-other.offset.y];
		}
	}

	return *this;
}

void CostGrid::set(Point p, int cost) {
	if(p.x < 0 || p.x >= width || p.y < 0 || p.y >= height)
		return;
	grid[p.x][p.y] = cost;
}

void CostGrid::setRelative(Point p, int cost) {
	if(p.x < offset.x || p.x >= width + offset.y || 
		p.y < offset.y || p.y >= height + offset.y)
		return;
	grid[p.x - offset.x][p.y - offset.y] = cost;
}

int CostGrid::get(Point p) {
	if(p.x < 0 || p.x >= width || p.y < 0 || p.y >= height)
		return -1;
	return grid[p.x][p.y];
}

int CostGrid::getRelative(Point p) {
	if(p.x < offset.x || p.x >= width + offset.x || 
		p.y < offset.y || p.y >= height + offset.y)
		return -1;
	return grid[p.x - offset.x][p.y - offset.y];
}

int CostGrid::getWidth() {
	return width;
}

int CostGrid::getHeight() {
	return height;
}

Point CostGrid::getOffset() {
	return offset;
}

void CostGrid::print() {
	for(int i=0; i<height; i++) {
		for(int j=0; j<width; j++) {
			std::cout << grid[j][i] << ' ';
		}
		std::cout << "\n";
	}
	std::cout << "\n";
}