#ifndef GAMEOBJECTFACTORY_H
#define GAMEOBJECTFACTORY_H

#include "GameObject.h"
#include "Unit.h"
#include "GameObjectPtr.h"
#include "Point.h"
//#include "Board.h"
#include "../System/World.h"
#include "../Graphics/Animation/Animated.h"
#include "../System/Events/Event.h"
//#include "../Components/AI_Components/UnitAI.h"
#include <string>
#include <vector>
using std::string;
using std::vector;

class Board;
class UnitAI;

class GameObjectFactory {
private:
	Board *world;
	Config *unitConfig;
	int unitCount;
	vector<Animated*> animations;
	vector<string> names;
	vector<string> baseNames;
	vector<string> unitNames;
	vector<string> towerNames;

	GameObjectPtr make(int type, int team, Point p, UnitAI *ai);
public:

	/**
	 * loads animation files
	 *\return true if successfull
	 */
	bool init(Board *world);

	~GameObjectFactory();

	/**
	 * Creates a new unit
	 *\param type = type of unit type to make
	 *\param team = team of unit
	 *\param p = where to place unit
	 *\param world = world to place unit in
	 *\return unit created
	 */
	GameObjectPtr makeUnit(int type, int team, Point p);

	void makeProjectile(Point target, Event event, GLfloat x, GLfloat y, GLfloat speed=10);

	int getCost(int type);
	
	/**
	 * TODO
	 * Creates a new tower
	 *\param type = type of tower type to make
	 *\param team = team of tower
	 *\param p = where to place tower
	 *\param world = world to place tower in
	 *\return tower created
	 */
	GameObjectPtr makeTower(int type, int team, Point p);

	GameObjectPtr makeBase(int team, Point p);

	vector<string> getUnitNames();

	vector<string> getTowerNames();

};

#endif