#include "GameObject.h"
#include "../System/World.h"

GameObject::GameObject(){
	alive = true;
	team = 0;
	world = NULL;
}

//declare destructor. silly c++
GameObject::~GameObject() {}

void GameObject::kill() {
	alive = false;
	world->removeObject(this);
}

void GameObject::setWorld( World *world ){
	this->world = world;
}

bool GameObject::init(World *world, int team) {
	this->world = world;
	this->team = team;
	return true;
}