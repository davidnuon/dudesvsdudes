#include "Tile.h"
#include "../System/LOpenGL.h"
#include "../System/World.h"

Tile::Tile(Graphics *graph){
	graphics = graph;
	graph->init();
}

Tile::~Tile(){
	delete graphics;
}

bool Tile::init(){
	return true;
}

void Tile::update(World *world){
	graphics->update();
}

void Tile::render(World *world){
	graphics->render(world);
}