#ifndef OPTIONS_H
#define OPTIONS_H

#include "../Graphics/Animation/Animated.h"
#include "../Graphics/Animation/AnimIter.h"
#include "GameObject.h"
#include "MenuButtons.h"
#include <stdio.h>

class Options: public GameObject{
private:
	std::vector<AnimIter> iters;
	MenuButtons *buttons[4];
	Events toSend[4];
public:
	Options();
	virtual ~Options();
	virtual bool init( World *world );
	void update();
	void render();
};

#endif