#ifndef POINT_H
#define POINT_H

#include <string>

class Point {
public:
	int x, y;

	/**
	 * Default constructor
	 * Intializes Point to (-1,-1).
	 */
	Point();

	/**
	 * Constructor
	 *\param x = x corrdinate of Point
	 *\param y = y corrdinate of Point
	 */
	Point(int x, int y);

	/**
	 * Equality tester
	 *\param other = Point to compare to
	 *\return True if both points are the same
	 */
	bool operator == (const Point &other) const;
	bool operator != (const Point &other) const;
	
	/**
	 * Computes manhatan distance between two points.
	 * Manhatan distance is x distance + y distance.
	 *\param other = Point to measure distance to
	 *\return Manhatan distance to other
	 */
	int distance(Point other);

	/**
	 * Computes true distance between two points.
	 *\param other = Point to measure distance to
	 *\return Distance between two points
	 */
	int trueDistance(Point other);

	/**
	 * Returns true it points are next to eachother or the same.
	 *\return True if next to other
	 */
	bool nextTo(Point other);

	/**
	 * Convert to string for debugging
	 *\return String version of point
	 */
	std::string toString();
};

#endif