/**
 *\class Tile
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * This is a tile that is subclassed from the GameObject class.
 *
 */
#ifndef TILE_H
#define TILE_H

#include "../Components/Graphics_Components/Graphics.h"
#include "GameObject.h"

class Tile: public GameObject{
private:
	Graphics *graphics;
public:
	/**
	 * Constructor.
	 *\param graph = Pointer to a graphics component which will be used to graphically represent this GameObject.
	 */
	Tile(Graphics *graph);
		
	/**
	 * Deconstructor.
	 */
	virtual ~Tile();
	
	bool init();
	/**
	 * Update function.
	 * \param world = Pointer to a World Object.
	 */
	void update(World *world);

	/**
	 * Render function.
	 * \param world = Pointer to a World Object.
	 */
	void render(World *world);
};

#endif