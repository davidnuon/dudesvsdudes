#include "GameObjectFactory.h"
#include "../Graphics/Animation/AnimIter.h"
#include "Projectile.h"
#include "../Components/AI_Components/UnitAI.h"
#include "../Components/AI_Components/TowerAI.h"

#include <assert.h>
#include <string>
#include <sstream>
#include <iostream>

bool GameObjectFactory::init(Board *b) {
	world = b;
	unitConfig = b->cf.getConfig("res/Configs/Unit.config");
	if(unitConfig == NULL)
		return false;

	unitCount = unitConfig->getInt("UnitCount");
	if(unitCount < 1) 
		return false;

	animations.resize(unitCount);
	names.resize(unitCount);

	for(int i=0; i<unitCount; i++) {
		std::stringstream ss("");
		ss << i;
		string name = unitConfig->getString("Unit" + ss.str());
		names[i] = name;
		animations[i] = new Animated();
		animations[i]->loadAnimation(unitConfig->getString(name + "Animation"));
		string type = unitConfig->getString(name + "Type");
		if(type == "Base") {
			baseNames.push_back(name);
		} else if(type == "Unit") {
			unitNames.push_back(name);
		} else if(type == "Tower") {
			towerNames.push_back(name);
		}
	}
	
	return true;
}

GameObjectFactory::~GameObjectFactory() {
	for(int i=0; i<unitCount; i++) {
		delete animations[i];
	}
}

GameObjectPtr GameObjectFactory::make(int type, int team, Point p, UnitAI *ai) {
	GameObjectPtr guy;
	string typeString = names[type];
	AnimIter iter;
	iter.init(animations[type]);
	AQState newState;
	newState.clear();

	switch(team) {
		case 1:
			newState.color.r = 255;
			newState.color.g = 20;
			newState.color.b = 20;
			break;
		case 2:
			newState.color.r = 20;
			newState.color.g = 20;
			newState.color.b = 255;
		break;
	}
	iter.addDelta(newState);

	UnitGraphics *gr = new UnitGraphics(iter);

	Unit *tempUnit = new Unit(team, type, ai, gr);
	guy = GameObjectPtr(tempUnit);

	tempUnit->health 		= unitConfig->getInt(typeString + "Health");
	tempUnit->maxHealth 	= unitConfig->getInt(typeString + "Health");
	tempUnit->attackRange	= unitConfig->getInt(typeString + "AttackRange");
	tempUnit->attackDamage	= unitConfig->getInt(typeString + "AttackDamage");
	tempUnit->attackCooldown= unitConfig->getInt(typeString + "AttackCooldown");
	tempUnit->speed			= unitConfig->getInt(typeString + "Speed");
	tempUnit->stationary	= unitConfig->getInt(typeString + "Stationary") != 0;
	bool success = tempUnit->init(world, p, guy);


	assert (success);
	world->addObject(guy);

	return guy;

}

GameObjectPtr GameObjectFactory::makeUnit(int type, int team, Point p) {
	GameObjectPtr guy;

	//Check inputs for validity
	if(p == Point())
		return guy;
	if(type >= unitCount || type < 0)
		return guy;

	//check if p is open
	if(world->getGameObject(p).getPtr() != NULL)
		return guy;


	string typeString = names[type];

	//make sure that type is unit
	if(unitConfig->getString(typeString + "Type") != "Unit")
		return guy;

	UnitAI *ai = new UnitAI();

	return make(type,team,p,ai);
}

int GameObjectFactory::getCost(int type) {
	if(type <0 || type >= unitCount)
		return -1;
	string typeName = names[type];
	return unitConfig->getInt(typeName + "Cost");
}

void GameObjectFactory::makeProjectile(Point target, Event event, GLfloat x, GLfloat y, GLfloat speed){
	AnimIter iter;
	iter.init(animations[4]);
	UnitGraphics *gr = new UnitGraphics(iter);
	gr->init();


	Projectile *p = new Projectile(world, target, event, x, y, speed, gr);
	world->addObject(GameObjectPtr(p));
}

GameObjectPtr GameObjectFactory::makeBase(int team, Point p) {
	GameObjectPtr guy;

	//Check inputs for validity
	if(p == Point())
		return guy;

	//check if p is open
	if(world->getGameObject(p).getPtr() != NULL)
		return guy;


	string typeString = names[0];
	//make sure that type is unit
	if(unitConfig->getString(typeString + "Type") != "Base")
		return guy;
	TowerAI *ai = new TowerAI();

	return make(0,team,p,ai);
}

GameObjectPtr GameObjectFactory::makeTower(int type, int team, Point p) {
	GameObjectPtr guy;

	//Check inputs for validity
	if(p == Point())
		return guy;

	if(type >= unitCount || type < 0)
		return guy;
	//check if p is open
	if(world->getGameObject(p).getPtr() != NULL)
		return guy;


	string typeString = names[type];
	//make sure that type is unit
	if(unitConfig->getString(typeString + "Type") != "Tower")
		return guy;
	TowerAI *ai = new TowerAI();

	return make(type,team,p,ai);
}