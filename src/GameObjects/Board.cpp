#include "Board.h"
#include "GameObject.h"
#include "Unit.h"
#include "Point.h"
#include "../Components/Graphics_Components/BoardGraph.h"
#include "../Components/Graphics_Components/GraphState.h"
#include "../System/LOpenGL.h"
#include "../System/World.h"
#include "../System/Events/KeyInput.h"
#include "../System/Events/Event.h"
#include "../System/Color.h"
#include "../System/Factory/ConfigFactory.h"
#include "../System/Factory/Config.h"
#include "../System/Constants.h"
#include <iostream>

Color Board::COLOR_OPEN		 = Color(  0,0  ,  0,0);
Color Board::COLOR_BLOCKED	 = Color(  0,  0,255,255);
Color Board::COLOR_LINE		 = Color(  0,127, 50,255);

Board::Board(Screen *screen) :World(screen) {
	objectGrid = NULL;
	rows = 0;
	columns = 0;
	//dude;
}

bool Board::init() {
	rows = 16;
	columns = 32;
	GraphState gState;
	gState.x 			= 50;
	gState.y 			= 50;
	gState.width 		= 900;
	gState.height 		= 500;
	gState.horzCount	= columns;
	gState.vertCount	= rows;
	gState.thickness	= 2;
	gState.background 	= COLOR_OPEN;
	gState.line			= COLOR_LINE;
	if( tg.init(gState) == false ) {
		return false;
	}


	costGrid = CostGrid(columns, rows);

	objectGrid = new GameObjectPtr*[columns];
	for(int i=0; i<columns; i++){
		objectGrid[i] = new GameObjectPtr[rows];
	}

	if(UnitFactory.init(this) == false)
		return false;
	return true;
}

bool Board::init(std::string configPath) {
	Config *boardConfig = cf.getConfig(configPath);
	if(boardConfig == NULL)
		return false;
	columns = boardConfig->getInt("COLUMNS");
	rows = boardConfig->getInt("ROWS");

	GraphState gState;
	gState.x 			= boardConfig->getInt("XOFFSET");
	gState.y 			= boardConfig->getInt("YOFFSET");
	gState.width 		= boardConfig->getInt("WIDTH");
	gState.height 		= boardConfig->getInt("HEIGHT");
	gState.horzCount	= columns;
	gState.vertCount	= rows;
	gState.thickness	= 2;
	gState.background 	= COLOR_OPEN;
	gState.line			= COLOR_LINE;
	if( tg.init(gState) == false ) {
		return false;
	}

	int panwidth = gState.width - Constants::SCREEN_WIDTH+50;
	if(panwidth < 0)
		panwidth = 0;
	int panheight = gState.height - Constants::SCREEN_HEIGHT+50;
	if(panheight < 0)
		panheight = 0;
	Constants::setMaxPan(0,0,panwidth,panheight);
	//Constants::setMaxPan(0,0,10000,10000);
	costGrid = CostGrid(columns,rows);


	objectGrid = new GameObjectPtr*[columns];
	for(int i=0; i<columns; i++){
		objectGrid[i] = new GameObjectPtr[rows];
	}

	if(UnitFactory.init(this) == false)
		return false;

	//set up background
	background = af.getIter(boardConfig->getString("BACKGROUND"));
	background.setDrawFromCenter(false);
	background.setOffset(gState.x,gState.y,gState.width,gState.height);

	return true;
}

bool Board::putInBoard(Point p, GameObjectPtr g) {
	//exit if point is not in grid
	if(p.x < 0 || p.y < 0 ||
		p.x >= columns || p.y >= rows) return false;

	//exit if GameObject does not exits
	if(g.getPtr() == NULL) return false;

	//exit if already occupied
	if(objectGrid[p.x][p.y].getPtr() != NULL) return false;

	//ocupy space
	objectGrid[p.x][p.y] = g;
	return true;
}

void Board::removeFromBoard(Point p) {
	//exit if point is not in grid
	if(p.x < 0 || p.y < 0 ||
		p.x >= columns || p.y >= rows) return;

	//clear space
	objectGrid[p.x][p.y].reset();
}

void Board::removeFromBoard(Point p, GameObjectPtr g) {
	//exit if point is not in grid
	if(p.x < 0 || p.y < 0 ||
		p.x >= columns || p.y >= rows) return;

	//clear space
	if(objectGrid[p.x][p.y].getPtr() == g.getPtr())
		objectGrid[p.x][p.y].reset();
}

GameObjectPtr Board::getGameObject(Point p) {
	//exit if point is not in grid
	if(p.x < 0 || p.y < 0 ||
		p.x >= columns || p.y >= rows) return GameObjectPtr();

	return objectGrid[p.x][p.y];
}

void Board::passEvent(Event e, Point p) {
	Observer *dude = dynamic_cast<Observer*>(getGameObject(p).getPtr());
	if(dude != NULL)
		dude->notify(e);
}
 
//Set pathfinding
void Board::setWall(Point p) {
	if(p.x < 0 || p.y < 0 ||
		p.x >= columns || p.y >= rows) return;

	//set blocked
	costGrid.set(p,WALL);
}

void Board::setOpen(Point p) {
	if(p.x < 0 || p.y < 0 ||
		p.x >= columns || p.y >= rows) return;

	//set blocked
	costGrid.set(p,NORMAL_COST);
}

void Board::setCost(Point p, int cost) {
	if(p.x < 0 || p.y < 0 ||
		p.x >= columns || p.y >= rows) return;

	//set blocked
	costGrid.set(p,cost);
}

int Board::getCost(Point p) {	
	if(p.x < 0 || p.y < 0 ||
		p.x >= columns || p.y >= rows) return -1;

	return costGrid.get(p);
}

bool Board::isOpen(Point p) {
	//exit if point is not in grid
	if(p.x < 0 || p.y < 0 ||
		p.x >= columns || p.y >= rows) return false;

	return costGrid.get(p) != WALL;
}

CostGrid Board::getCostGrid() {
	return costGrid;
}

int Board::getRows() {
	return rows;
}

int Board::getColumns() {
	return columns;
}

void Board::flipBlocked(Point p) {
	//exit if point is not in grid
	if(p.x < 0 || p.y < 0 ||
		p.x >= columns || p.y >= rows) return;

	if(costGrid.get(p) == WALL) {
		costGrid.set(p,NORMAL_COST);
		tg.setColor(COLOR_OPEN, p);
	} else {
		costGrid.set(p,WALL);
		tg.setColor(COLOR_BLOCKED, p);
	}
}

//render helpers
void Board::pointToDisplay(Point p, GLfloat &x, GLfloat &y) {
	tg.pointToDisplay(p,x,y);
}

Point Board::displayToPoint(GLfloat x, GLfloat y) {
	return tg.displayToPoint(x,y);
}

void Board::update() {
	int mouseX,mouseY;
	KeyInput::getMouseState(mouseX, mouseY);
	mouse = displayToPoint(mouseX, mouseY);
	handleEvents();
	for(int i=0; i<10 && (!pathHeap.isEmpty()); i++) {
		GameObjectPtr dude = pathHeap.pop();
		Unit* dudeCast = static_cast<Unit*>(dude.getPtr());
		if(dudeCast != NULL) {
			Pathfinder *p = dudeCast->getPathfinder();
			p->setCostGrid(costGrid);
			p->findPath();
		}
	}
	World::update();
}

void Board::render() {
	tg.render(this);
	background.update();
	World::render();
	//background.draw(this);
}

void Board::handleEvents() {
	//std::cout << "Handling event\n";
	while( eventHeap->getSize() > 0 ){
		//std::cout << "Handling event\n";
		Event event = pop_event();
		switch(event.getType()){
			case MOUSE_BUTTON_LEFT:
			//std::cout << "Left click\n";
			//flipBlocked(mouse);
			break;
		case MOUSE_BUTTON_RIGHT:
			break;
		}
	}
}

void Board::pushPathfinder(PathfinderPointer p) {
	pathHeap.push(p);
}