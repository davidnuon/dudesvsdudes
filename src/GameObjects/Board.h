#ifndef BOARD_H
#define BOARD_H

#include "GameObject.h"
#include "Unit.h"
#include "GameObjectPtr.h"
#include "../Components/Graphics_Components/BoardGraph.h"
#include "../Components/AI_Components/PathfinderHeap.h"
#include "Point.h"
#include "../System/LOpenGL.h"
#include "../System/World.h"
#include "../System/Events/Observer.h"
#include "../System/Color.h"
#include "../Components/AI_Components/AStar.h"
#include "../Player/Squad.h"
#include "../Components/AI_Components/CostGrid.h"
#include "GameObjectFactory.h"

#include <climits>
#include <string>

class Board : public World {
private:
	//array to hold gameobjects
	GameObjectPtr 	**objectGrid;
	//hold terain cost for pathfinding
	CostGrid		costGrid;
	//heap for rationing pathfinding
	PathfinderHeap 	pathHeap;

	BoardGraph	tg;
	int rows, columns;
	Point mouse;
	static Color COLOR_OPEN;
	static Color COLOR_BLOCKED;
	static Color COLOR_LINE;

	AnimIter background;

	void handleEvents();

public:

	GameObjectFactory UnitFactory;

	bool init();
	bool init(std::string path);

	Board(Screen *screen);

	//insert GameObject in grid
	bool putInBoard(Point, GameObjectPtr);

	//Clear selected point
	void removeFromBoard(Point);
	void removeFromBoard(Point, GameObjectPtr);

	//get GameObject at point
	GameObjectPtr getGameObject(Point);

	//sends event to game object at point
	void passEvent(Event e, Point p);

	//Pathfinding
	//Set blocked
	void setWall(Point);

	//Set clear
	void setOpen(Point);

	//Set navigation cost
	void setCost(Point, int cost);

	//Get navigation cost
	int getCost(Point);

	//Return true if no wall
	bool isOpen(Point);

	void flipBlocked(Point);

	//Return cost grid
	CostGrid getCostGrid();

	int getRows();

	int getColumns();

	void pushPathfinder(PathfinderPointer);


	//Render helpers
	//Translate grid point to screen quordinates
	void pointToDisplay(Point p, GLfloat &x, GLfloat &y);

	//Translate screen to point. (-1,-1) means out of range
	Point displayToPoint(GLfloat x, GLfloat y);

	//inherited methods
	void virtual update();
	void virtual render();

	friend class AStar;

	static const int NORMAL_COST = 1;
	static const int WALL = INT_MAX;

};

#endif