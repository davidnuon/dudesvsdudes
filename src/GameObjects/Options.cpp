#include "Options.h"

Options::Options(){
	for( int i = 0; i < sizeof(buttons)/sizeof(buttons[0]); i++ ){
		buttons[i] = NULL;
	}
	toSend[0] = START_SCREEN_CHARSEL;
	toSend[1] = START_SCREEN_STORY;
	toSend[2] = START_SCREEN_BESTIARY;
	toSend[3] = START_SCREEN_HELP;
}

Options::~Options(){
	for( int i = 0; i < sizeof(buttons)/sizeof(buttons[0]); i++ ){
		if( buttons[i] != NULL ){
			delete buttons[i];
		}
	}
}

bool Options::init( World *world ){
	setWorld( world );

	AQState delta;
	delta.clear();
	//delta.color.a = -128;

	for( int i = 0; i < sizeof(buttons)/sizeof(buttons[0]); i++ ){
		buttons[i] = new MenuButtons;
		buttons[i]->setWorld( world );
	}

	for( int i = 0; i < sizeof(buttons)/sizeof(buttons[0]); i++ ){
		iters.push_back( world->af.getIter("res/Animations/menu_text.anim") );
		iters[i].addDelta( delta );
		iters[i].switchAnim( i );
	}

	for( int i = 0; i < sizeof(buttons)/sizeof(buttons[0]); i++ ){
		if( buttons[i] == NULL ){
			buttons[i] = new MenuButtons;
		}
		buttons[i]->setClickBox( iters[i].getPos(i) );
	}

	return true;
}

void Options::update(){
	for(int i = 0; i < iters.size(); i++){
		//buttons[i]->setClickBox( iters[i].getPos(i) );
		if( buttons[i]->isClicked() == true ){
			world->screen->notify( toSend[i] );
			buttons[i]->reset();
		}
		iters[i].update();
	}
}

void Options::render(){
	for(int i = 0; i < iters.size(); i++){
		iters[i].draw( world );
	}
}