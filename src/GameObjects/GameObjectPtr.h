#ifndef GAMEOBJECTPOINTER_H
#define GAMEOBJECTPOINTER_H

#include "GameObject.h"

class GameObjectPtr {
private:
	GameObject	*ptr;
	int			*count;

	/**
	 * Constructor to initialize chain.
	 * Only ment to be called by ObjectManager
	 * Only call once per GameObject.
	 */
	GameObjectPtr(GameObject *object);

public:
	//constructors
	/**
	 * Default constructor. Initializes pointers to NULL.
	 */
	GameObjectPtr();

	/**
	 * Copy constructor. Copies pointers and increases count.
	 */
	GameObjectPtr(const GameObjectPtr &copy);

	/**
	 * Destructor. Destroys pointers if count == 0.
	 * Decrements count.
	 */
	~GameObjectPtr();

	/**
	 * Copy assignment. Decrements old count, copies values, then increments count.
	 * Will destroy pointers if old count == 0.
	 */
	GameObjectPtr& operator = (const GameObjectPtr &other);

	/**
	 * This method should be called every game loop.
	 * Get a pointer to a game object. Checks if GameObject is still alive.
	 * If it is not, this pointer gets reset and counter is decremented.
	 * Delets pointers if count == 0.
	 *\return Pointer to GameObject.
	 */
	GameObject* getPtr();

	/**
	 * Sets this pointer to NULL, deleting if needed.
	 */
	void reset();

	/**
	 * There are no checks for NULL pointers!
	 *\return GameObject pointed to by ptr
	 */
	GameObject& operator -> ();

	/**
	 * There are no checks for NULL pointers!
	 *\return GameObject pointed to by ptr
	 */
	GameObject& operator * ();

	friend class ObjectsManager;
	friend class GameObjectFactory;
};

#endif