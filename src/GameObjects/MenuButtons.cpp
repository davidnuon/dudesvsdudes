#include "MenuButtons.h"

MenuButtons::MenuButtons(){
	clicked = false;
}

void MenuButtons::onClick(){
	/*
	world->mp.playSFX("res/Music/Shotgun_Blast-Jim_Rogers.wav");
	LFRect clicky = getClickBox();
	GLfloat x = clicky.x + clicky.w/2.f;
	GLfloat y = clicky.y + clicky.h/2.f;
	world->af.draw("res/Animations/explosions.anim", 0, x, y, true);
	
	Event tempEvent(TRANSITION_OUT);
	world->screen->notify( tempEvent );
	*/
	clicked = true;
}

void MenuButtons::onHover(){
	
}

bool MenuButtons::isClicked(){
	return clicked;
}

void MenuButtons::reset(){
	clicked = false;
}