/**
 *\class Unit
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Base class for Units. It is abstracted from the GameObjects class.
 *
 */
#ifndef UNIT_H
#define UNIT_H

#include "GameObject.h"
#include "Point.h"
//#include "Board.h"
#include "../System/LOpenGL.h"
#include "../System/Events/Observer.h"
#include "../Components/Graphics_Components/UnitGraphics.h"
#include "../System/LOpenGL.h"
//#include "../Components/AI_Components/UnitAI.h"
#include "../Components/AI_Components/Pathfinder.h"
#include "../Components/Graphics_Components/Graphics.h"
//#include "../Components/Physics_Components/UnitPhysics.h"
#include "../Components/AI_Components/TargetFinder.h"

class Board;
class UnitPhysics;
class UnitAI;


//Basic Unit class
class Unit : public GameObject, public Observer{
protected:
	UnitAI *ai;
	UnitGraphics *graphics;
	UnitPhysics *physics;

	GameObjectPtr me;

	//Stats
	int health;
	int maxHealth;
	int attackRange;
	int attackDamage;
	int attackCooldown;
	int attackCooldownTimer;
	GLfloat attackRangeBuff;
	int attackRangeBuffTimer;
	GLfloat attackDamageBuff;
	int attackDamageBuffTimer;
	GLfloat attackCooldownBuff;
	int attackCooldownBuffTimer;
	GLfloat speedBuff;
	int speedBuffTimer;

	//int team;
	int type;

	//Physics variables
	GLfloat x,y;
	GLfloat speed;
	bool stationary;

	//Pathfinding variables
	Board *board;
	Point lastLocation;
	Point location;
	Point nextLocation;
	bool atTarget;

	void atNextLocation();
	void clearOldLocation();

	bool attack(Point p);

public:
	//Constructors
	/**
	 * Manual Constructor. Does not work
	 *\param aiComponent = The Ai component.
	 *\param graphicsComponent = The Graphics component.
	 *\param physicsComponent = The Physics component.
	 */
	Unit(UnitAI *aiComponent, UnitGraphics *graphicsComponent, UnitPhysics *physicsComponent);
	
	/**
	 * Default constructor.
	 *\team Team of this unit.
	 *\param type = type of this unit.
	 *\param graphicsComponent = initialized sprite
	 */
	Unit(int team, int type, UnitAI *aiComponent, UnitGraphics *graphicsComponent);

	virtual ~Unit();

	/**
	 * Finishes constructing unit.
	 *\param b = The Board this unit is being inserted to.
	 *\param loc = The location this unit will occupy.
	 *\ptr Pointer to this unit for moving around board.
	 *\return True if successfull.
	 */
	bool init(Board *b, Point loc, GameObjectPtr ptr);

	//Physics methods

	/**
	 * Move unit to Point p. Checks if p is unoccupied and next to current location.
	 *\param p = Point to move to.
	 *\return True if unit can move to p.
	 */
	bool moveTo(Point p);

	/**
	 * Instantly move unit to Point p. Checks if p is unoccupied.
	 *\param p = Point to move to.
	 *\return True if unit can move to p.
	 */
	bool teleportTo(Point p);


	/**
	 * Set new pathfinding target
	 *\param p = Point to pathfind to
	 *\return True if p is valid
	 */
	bool setRallyPoint(Point p,CostGrid *grid);

	/**
	 * Get the team
	 *\return team
	 */
	int getTeam();

	Point getLocation();

	Pathfinder* getPathfinder();

	//inherited funtions
	/**
	 * Kills the unit. Cleans up some pointers and waits for outside pointers
	 * to stop referencing this unit.
	 */
	void kill();

	bool handleEvents();

	/**
	 * Update function.
	 * \param world = Pointer to a World Object.
	 */
	virtual void update();

	/**
	 * Render function.
	 * \param world = Pointer to a World Object.
	 */
	virtual void render();
	virtual int* getHealth();
	virtual void setHealth(int);

	friend class UnitPhysics;
	friend class UnitAI;
	friend class TowerAI;
	friend class GameObjectFactory;
	friend class Player;
	friend class Squad;
};

#endif