#include "ObjectsManager.h"
#include <iostream>

ObjectsManager::~ObjectsManager(){
	for(int i = 0; i < objectsVec.size(); i++){
		objectsVec[i].reset();
	}
}

bool ObjectsManager::init(World *world){
	for(int i = 0; i < objectsVec.size(); i++){
	}
	return true;
}

void ObjectsManager::update(){
	for(int i = 0; i < objectsVec.size(); i++){
		GameObject *cur = objectsVec[i].getPtr();
		if(cur == NULL)
			std::cout << "Error, object not removed\n";
		else
			cur->update();
	}
}

void ObjectsManager::render(){
	for(int i = 0; i < objectsVec.size(); i++){
		GameObject *cur = objectsVec[i].getPtr();
		if(cur == NULL)
			std::cout << "Error, object not removed\n";
		else
			cur->render();
	}
}

GameObjectPtr ObjectsManager::addObject(GameObject *gameObject){
	GameObjectPtr add(gameObject);
	objectsVec.push_back(add);
	return add;
}

GameObjectPtr ObjectsManager::addObject(GameObjectPtr gameObject){
	objectsVec.push_back(gameObject);
	return gameObject;
}

std::vector<GameObjectPtr> ObjectsManager::getObjectsVec(){
	return objectsVec;
}

int ObjectsManager::removeObject(GameObject *removed){
	int counter = 0;
	for(int i = 0; i < objectsVec.size(); i++){
		if( objectsVec[i].ptr == removed || objectsVec[i].getPtr() == NULL){
			objectsVec[i] = objectsVec.back();
			objectsVec.pop_back();
			i--;
			counter++;
		}
	}
	return counter;
}