#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "GameObject.h"
#include "Point.h"
#include "Board.h"
#include "../Components/Graphics_Components/UnitGraphics.h"

class Projectile : public GameObject {
private:
	Point target;

	Event event;
	Board *board;

	GLfloat x,y;
	GLfloat speed;
	
	GLfloat deltaX, deltaY;
	GLfloat targetX, targetY;

	UnitGraphics *sprite;

	void atTarget();
	GLfloat trueDistance(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2);
public:

	Projectile(Board *b, Point target, Event event,GLfloat x, GLfloat y, GLfloat speed, UnitGraphics *g);

	~Projectile();
	
	virtual void update();

	virtual void render();
};

#endif