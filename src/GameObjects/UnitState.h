#ifndef UNITSTATE_H
#define UNITSTATE_H

enum UnitState {
	N			= 0,
	NE			= 1,
	E			= 2,
	SE 			= 3,
	S 			= 4,
	SW			= 5,
	W			= 6,
	NW			= 7,
	STANDING	= 8,
	ATTACK		= 10,
	HIT			= 11,
	DIE			= 12,
	DEAD		= 13
};

#endif