#include "Projectile.h"
#include "GameObjectFactory.h"
#include <cmath>

Projectile::Projectile(Board *board, Point target, Event event, GLfloat x, GLfloat y, GLfloat speed, UnitGraphics *g) : GameObject(){
	this->board = board;
	this->target = target;
	this->event = event;
	this->x = x;
	this->y = y;
	this->speed = speed;
	sprite = g;
	if(target == Point())
		kill();
	world = board;
	board->pointToDisplay(target,targetX,targetY);
	GLfloat dist = trueDistance(x,y,targetX,targetY);
	deltaX = (targetX - x)/dist * speed;
	deltaY = (targetY - y)/dist * speed;
}

Projectile::~Projectile() {

	delete sprite;
	sprite = NULL;
}
void Projectile::update() {
	if(trueDistance(x,y,targetX,targetY) <= speed) {
		x = targetX;
		y = targetY;
		atTarget();
		return;
	} else {
		x += deltaX;
		y += deltaY;
	}
	sprite->update();
	sprite->setPosition(x,y);
}

void Projectile::atTarget() {
	board->passEvent(event,target);
	GameObject::kill();
}

GLfloat Projectile::trueDistance(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2) {
	return sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
}

void Projectile::render() {
	sprite->render(world);
}
