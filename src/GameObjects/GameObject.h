/**
 *\class GameObject
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Base class, mostly abstract, that is intended to be inherited by all game objects.
 *
 */
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

class World;

class GameObject{
protected:
	bool collision;
	bool alive;
	int team;

	/**
	 * Kill object
	 */
	void kill();
	World* world;
public:
	/**
	 * GameObject constructor.
	 */
	GameObject();

	/**
	 * Virtual Deconstructor.
	 */
	virtual ~GameObject() = 0;

	virtual bool init( World *world, int team );

	void setWorld( World *world );
	/**
	 * Update function.
	 *
	 * \param world = Pointer to world object.
	 */
	virtual void update() = 0;


	/**
	 * Living status.
	 *
	 * \return True if GameObject can be safely used.
	 */
	bool isAlive() {
		return alive;
	}

	int getTeam() {
		return team;
	}

	friend class GameObjectFactory;
	virtual void render() = 0;
};

#endif