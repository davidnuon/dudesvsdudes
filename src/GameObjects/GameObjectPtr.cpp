#include "GameObjectPtr.h"
#include "GameObject.h"
#include <cstdlib>

GameObjectPtr::GameObjectPtr() {
	ptr = NULL;
	count = NULL;
}

GameObjectPtr::GameObjectPtr(const GameObjectPtr &copy) {
	if(copy.ptr == NULL) {
		ptr = NULL;
		count = NULL;
	} else {
		ptr = copy.ptr;
		count = copy.count;
		(*count)++;
	}
}

GameObjectPtr::GameObjectPtr(GameObject *object) {
	if(object == NULL) {
		ptr = NULL;
		count = NULL;
	} else {
		ptr = object;
		count = new int;
		(*count) = 1;
	}
}

GameObjectPtr::~GameObjectPtr() {
	if(ptr != NULL) {
		(*count)--;
		if(*count == 0) {
			delete ptr;
			delete count;
		}
	}
}

GameObjectPtr& GameObjectPtr::operator = (const GameObjectPtr &other) {
	if(this == &other) return *this;

	if(ptr != NULL) {
		(*count)--;
		if(*count == 0) {
			delete ptr;
			delete count;
		}
	}
	if(&other == NULL) {
		//why is this happening
		ptr = NULL;
		count = NULL;
		return *this;
	}
	if(other.ptr == NULL) {
		ptr = NULL;
		count = NULL;
	} else {
		ptr = other.ptr;
		count = other.count;
		(*count) ++;
	}
	return *this;
}

GameObject* GameObjectPtr::getPtr() {
	if(ptr != NULL) {
		if(ptr->isAlive()) {
			return ptr;
		} else {
			(*count)--;
			if(*count == 0) {
				delete ptr;
				delete count;
			}
			ptr = NULL;
			count = NULL;
		}
	}
	return NULL;
}

void GameObjectPtr::reset() {
	if(ptr != NULL) {
		(*count) --;
		if(*count == 0) {
			delete ptr;
			delete count;
		}
	}
	ptr = NULL;
	count = NULL;
}

GameObject& GameObjectPtr::operator * () {
	return *ptr;
}

GameObject& GameObjectPtr::operator -> () {
	return *ptr;
}