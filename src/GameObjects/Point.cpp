#include "Point.h"
#include <cstdlib>
#include <cmath>
#include <sstream>

Point::Point() {
	x = 0;
	y = 0;
}
Point::Point(int x, int y) {
	this->x = x;
	this->y = y;
}

bool Point::operator== (const Point &other) const {
	return x == other.x && y == other.y;
} 

bool Point::operator!= (const Point &other) const {
	return x != other.x || y != other.y;
} 

int Point::distance(Point other) {
	return abs(x-other.x) + abs(y-other.y);
}

int Point::trueDistance(Point other) {
	return sqrt((x-other.x)*(x-other.x) + (y-other.y)*(y-other.y));
}

bool Point::nextTo(Point other) {
	return (abs(other.x - x) <= 1) && (abs(other.y - y) <= 1);
}

std::string Point::toString() {
	std::stringstream result;
	result << "(" << x << ", " << y << ")";
	return result.str();
}
