#include "Unit.h"
#include "Board.h"
#include "Point.h"
#include "../System/LOpenGL.h"
#include "../System/Events/Events.h"
#include "../System/Events/ExtendedEvent.h"
#include "../Components/AI_Components/UnitAI.h"
#include "../Components/Graphics_Components/Graphics.h"
#include "../Components/Physics_Components/Physics.h"
#include "../Components/Physics_Components/UnitPhysics.h"
#include "../Components/AI_Components/TargetFinder.h"
#include "UnitState.h"
#include "GameObjectFactory.h"

Unit::Unit(UnitAI *aiComponent, UnitGraphics *graphicsComponent, UnitPhysics *physicsComponent){
	ai = aiComponent;
	graphics = graphicsComponent;
	physics = physicsComponent;

	if( ai == NULL || graphics == NULL || physics == NULL ){
		printf("The components of this Unit object are incorrect.\n");
	}
	board = NULL;
}

Unit::Unit(int team, int type, UnitAI *aiComponent, UnitGraphics *graphicsComponent) {
	//Create components
	ai = aiComponent;
	graphics = graphicsComponent;
	physics = new UnitPhysics();

	//initialize components
	graphics->init();

	//intialize variables
	speed = 5;
	board = NULL;
	stationary = false;

	if(team == Constants::ENEMY_TEAM) {
		health = 70;
	} else {
		health = 40;
	}
	maxHealth = 100;

	attackRange = 1;
	attackDamage = 2;
	attackCooldown = 200;

	attackRangeBuff = 1;
	attackDamageBuff = 1;
	attackCooldownBuff = 1;
	speedBuff = 1;

	attackRangeBuffTimer = 0;
	attackDamageBuffTimer = 0;
	attackCooldownBuffTimer = 0;
	speedBuffTimer = 0;

	this->team = team;
	this->type = type;

	lastLocation = Point(-1,-1);
	location = Point(-1,-1);
	nextLocation = Point(-1,-1);
	
}

bool Unit::init(Board *b, Point loc, GameObjectPtr me) {
	this->me = me;

	board = b;
	world = b;
	ai->init(b,team,this);	

	location = loc;
	nextLocation = Point(-1,-1);
	teleportTo(loc);

	if(stationary == true) {
		board->setWall(location);
	}
	return true;
}

Unit::~Unit(){
	if( ai != NULL ){
		delete ai;
		ai = NULL;
	}
	if( graphics != NULL ){
		delete graphics;
		graphics = NULL;
	}
	if( physics != NULL ){
		delete physics;
		physics = NULL;
	}
}

void Unit::kill() {
	board->af.draw( "res/Animations/bubble.anim", 2, this->location.x * 31 + 32, this->location.y * 31 + 32);
	board->removeFromBoard(location,me);
	board->removeFromBoard(nextLocation,me);

	board->setOpen(location);
	Event dead(UNIT_DEAD);
	int *data = (int*) &(dead.data);
	
	*data = team;
	*(data+1) = 30 + ((rand() % 10) * 6);

	board->subject.push_event(dead);
	//std::cout << "Unit dead\n";
	me.reset();
	ai->cleanUp();
	GameObject::kill();
}

bool Unit::attack(Point p) {
	if(p == Point(-1,-1))
		return false;
	Event e(DAMAGE);
	e.data = attackDamage * attackDamageBuff;
	if(attackRange == 1) {
		if(!(p.nextTo(location) || (p.nextTo(nextLocation) && nextLocation != Point(-1,-1))))
			return false;
		board->passEvent(e ,p);
		return true;
	} else if(attackRange >1 && (p.trueDistance(location) <= attackRange || 
		(p.trueDistance(nextLocation) <= attackRange && nextLocation != Point()) )) {

		board->UnitFactory.makeProjectile(p,e,x,y,5);
		return true;
	}
	//std::cout << "Unit attacking" << DAMAGE << "\n";
	return false;
}

//Physics methods
//move from current position to p
bool Unit::moveTo(Point p) {
	if(stationary)
		return false;
	//Make sure p is open
	if(board->getGameObject(p).getPtr() != NULL) 
		return false;
	if(board->putInBoard(p, me) == false)
		return false;
	//Make sure p is next to location
	if(location.nextTo(p) == false)
		return false;

	//start movement
	atTarget = false;
	nextLocation = p;
	lastLocation = location;
	GLfloat xDest, yDest;
	board->pointToDisplay(p, xDest, yDest);
	physics->moveTo(xDest,yDest, this);

	//update direction for animation
	int dX = nextLocation.x - location.x;
	int dY = nextLocation.y - location.y;
	if(dX == 0 && dY < 0)
		graphics->setState(N);
	else if(dX > 0 && dY< 0)
		graphics->setState(NE);
	else if(dX > 0 && dY == 0)
		graphics->setState(E);
	else if(dX > 0 && dY > 0)
		graphics->setState(SE);
	else if(dX == 0 && dY > 0)
		graphics->setState(S);
	else if(dX < 0 && dY > 0)
		graphics->setState(SW);
	else if(dX < 0 && dY == 0)
		graphics->setState(W);
	else if(dX < 0 && dY < 0)
		graphics->setState(NW);

	return true;
}

bool Unit::teleportTo(Point p) {
	//Make sure p is open
	if(board->getGameObject(p).getPtr() != NULL) 
		return false;
	if(board->putInBoard(p, me) == false)
		return false;

	//Move to p
	atTarget = true;
	nextLocation = Point(-1,-1);
	lastLocation = location;
	GLfloat xDest, yDest;
	board->pointToDisplay(p, xDest, yDest);
	physics->teleportTo(xDest,yDest, this);
	return true;
}

void Unit::atNextLocation() {
	atTarget = true;
	clearOldLocation();
	graphics->setState(STANDING);
}

//pathifinding methods
void Unit::clearOldLocation() {
	if(nextLocation == Point(-1,-1)) return;
	board->removeFromBoard(location);
	board->setOpen(location);
	location = nextLocation;
	nextLocation = Point(-1,-1);
}

bool Unit::setRallyPoint(Point p,CostGrid *grid) {
	//make sure point is not error case
	if(p == Point(-1,-1)) return false;
	ai->setRallyPoint(p,grid);
	return true;
}

int Unit::getTeam() {
	return team;
}

Point Unit::getLocation() {
	return location;
}

void Unit::update(){
	if(handleEvents() == false) return;
	//check if dead
	if(health < 0) {
		kill();
		return;
	}

	if(attackRangeBuffTimer > 0) {
		attackRangeBuffTimer--;
	} else {
		attackRangeBuff = 1;
	}
	if(attackDamageBuffTimer > 0) {
		attackDamageBuffTimer--;
	} else {
		attackDamageBuff = 1;
	}
	if(attackCooldownBuffTimer > 0) {
		attackCooldownBuffTimer--;
	} else {
		attackCooldownBuff = 1;
	}
	if(speedBuffTimer > 0) {
		speedBuffTimer--;
	} else {
		speedBuff = 1;
	}
	physics->update(this, board);
	ai->update();
	graphics->update();

	//Move sprite
	graphics->setPosition(x,y);

	//cheating heal
	//if(health < maxHealth)
	//	health += 1;
}

bool Unit::handleEvents() {
	while(eventHeap->getSize() > 0) {
		Event e = pop_event();
		int *data;
		switch(e.getType()) {
		case KILL:
			// Obtained from Board.cpp - Board::init
			board->af.draw( "res/Animations/square.anim", 1, this->location.x * 31 + 32, this->location.y * 31 + 32);
			health = 0;
			kill();
			return false;
			break;
		case DAMAGE:
				health -= e.data;
				if (this->type == Constants::UNIT_TYPE_BASE) {
					board->af.draw( "res/Animations/bubble.anim", 7, this->location.x * 31 + 32, this->location.y * 31 + 32);
				} else {
						board->af.draw( "res/Animations/square.anim", 1, this->location.x * 31 + 32, this->location.y * 31 + 32);
				}
			break;
		case WORLD_DAMAGE:
				if (this->team == Constants::PLAYER_TEAM) {
					health -= e.data;
					board->af.draw( "res/Animations/bubble.anim", 2, this->location.x * 31 + 32, this->location.y * 31 + 32);
				}
			break;
		case HEAL:
			health += e.data;
			if(health > maxHealth)
				health = maxHealth;
				board->af.draw( "res/Animations/bubble.anim", 3, this->location.x * 31 + 32, this->location.y * 31 + 32);

			break;
		case DAMAGE_BUFF:
			if(e.data1.capacity() < 2)
				break;
			attackDamageBuff = *((float*) &e.data1[0]);
			attackDamageBuffTimer = e.data1[1];
			break;
		case RANGE_BUFF:
			if(e.data1.capacity() < 2)
				break;
			attackRangeBuff = *((float*) &e.data1[0]);
			attackRangeBuffTimer = e.data1[1];
			break;
		case ATTACK_COOLDOWN_BUFF:
			if(e.data1.capacity() < 2)
				break;
			attackCooldownBuff = *((float*) &e.data1[0]);
			attackCooldownBuffTimer = e.data1[1];
			break;
		case FRIENDLY_SPEED_BUFF:
			if(e.data1.capacity() < 2)
				break;
			
			if(getTeam() == Constants::PLAYER_TEAM) {
				speedBuff = *((float*) &e.data1[0]);
				speedBuffTimer = e.data1[1];
				board->af.draw( "res/Animations/bubble.anim", 3, this->location.x * 31 + 32, this->location.y * 31 + 32);
			}
			break;
		case FRIENDLY_HEAL:
			if(e.data1.capacity() < 2)
				break;
			
			if(getTeam() == Constants::PLAYER_TEAM) {
				health += *((float*) &e.data1[0]);
				board->af.draw( "res/Animations/bubble.anim", 4, this->location.x * 31 + 32, this->location.y * 31 + 32);
			}
			break;

		case FRIENDLY_SPECIAL:
			if(getTeam() == Constants::ENEMY_TEAM && this->type != Constants::UNIT_TYPE_BASE) {
				board->af.draw( "res/Animations/square.anim", 1, this->location.x * 31 + 32, this->location.y * 31 + 32);
				health -= 20;
				return false;
			}
			break;
		}

	}
	return true;
}

void Unit::render(){
	if(x!=0&&y!=0)
	graphics->render(board);
}

Pathfinder* Unit::getPathfinder() {
	return static_cast<Pathfinder*>(&(ai->pathfinder));
}

int* Unit::getHealth(){
	return &health;
}

void Unit::setHealth(int n) {
	health = n;
}