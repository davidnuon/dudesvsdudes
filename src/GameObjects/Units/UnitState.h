#ifndef UNITSTATE_H
#define UNITSTATE_H

enum UnitState {
	UP,
	DOWN,
	LEFT,
	RIGHT
}

#endif