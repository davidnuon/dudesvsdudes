#ifndef MENUBUTTONS_H
#define MENUBUTTONS_H

#include <stdio.h>
#include "../System/Events/Clickable.h"
#include "../System/Events/Hoverable.h"
#include "../System/World.h"
class MenuButtons: public Hoverable{
private:
	bool clicked;
protected:
	virtual void onClick();
	virtual void onHover();
public:
	MenuButtons();
	bool isClicked();
	void reset();
};

#endif