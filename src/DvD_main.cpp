/**
\class DvD_Main

\author Shane Satterfield

\date 2013/03/02

\brief

This is the main of the Caffine Complex project.

*/
#include "System/LOpenGL.h"
#include "System/Constants.h"
#include "System/Events/KeyInput.h"
#include "System/Events/MouseInput.h"
#include "System/LTime.h"
#include "Screens/ScreenManager.h"

#include "System/Factory/MusicPlayer.h"
#include "System/Factory/AnimFactory.h"
#include <time.h>

bool init();
bool initGL();
void setView(int width, int height);
bool gameLoop();
void handleEvents(bool &quit, bool &focus, bool &visible, SDL_Event &event, ScreenManager *sm);
void cleanup();

/**
The main function.

\param argc = The number of command line arguments.
\param argv = The command line arguments.
\return 0 if executed properly.
*/
int main(int argc, char* argv[]){
	if(init() == false)
		return 1;

	if(gameLoop() == false)
		return 1;

	cleanup();
	return 0;
}

/**
The game loop function.

\return False if there were errors.
*/
bool gameLoop(){
	
	ScreenManager screenMan;
	
	bool quit = false;
	SDL_Event event;
	LTime fps;
	bool focus = true;
	bool visible = true;
	while(!quit){

		fps.start();

		handleEvents(quit, focus, visible, event, &screenMan);

		//Checks to see if the screen is in focus. If not, it doesn't update.
		//if(/*focus &&*/ Constants::getFrames() % 2 == 0)
			screenMan.update();
		
		//Checks if the screen is visible and only renders when it is.
		//Currently doesn't work.
		if(visible)
			screenMan.render();

		fps.delay();
		Constants::updateFrames();
	}

	return quit;
}

/**
This function handles the events.

\param quit = If the program needs to exit, this value will be set to true.
\param focus = Sets whether the screen is in focus or not.
\param visible = Sets whether the screen is visible or not.
\param event = This is the SDL_Event object that is being searched through.
*/
void handleEvents(bool &quit, bool &focus, bool &visible, SDL_Event &event, ScreenManager *sm){
	while(SDL_PollEvent(&event)){
		if(event.type == SDL_QUIT){
			quit = true;
		}
		else if(event.type == SDL_VIDEORESIZE){
			SDL_SetVideoMode(event.resize.w, event.resize.h, Constants::SCREEN_BPP, SDL_OPENGL | SDL_RESIZABLE);
			setView(event.resize.w, event.resize.h);
		}
		else if(event.type == SDL_ACTIVEEVENT){

			//TODO: Fix this. It doesn't set visible to false when the app is not in focus.
			if(event.active.state & SDL_APPACTIVE){
				visible = event.active.gain;
			}
			//This works fine. Checks if the screen is in keyboard focus. If not, it doesn't update.
			/*
			else if(event.active.state & SDL_APPINPUTFOCUS){
				focus = event.active.gain;
				if( focus == false ){
					MusicPlayer::pauseAll();
				}
				else{
					MusicPlayer::resumeAll();
				}
			}
			*/
		}
		else if(event.type == SDL_KEYDOWN){
			KeyInput::handle_events(event.key.keysym.sym);
			Event tmpEvent( Events( int( event.key.keysym.sym ) ) );
			sm->subject.push_event( tmpEvent );
		}
		else if( event.type == SDL_MOUSEBUTTONDOWN || event.type == SDL_MOUSEBUTTONUP || event.type == SDL_MOUSEMOTION ){
			MouseInput::handleEvents( event );
			Event tmpEvent = MouseInput::determineMouseEvent( event );
			if( tmpEvent.getType() != NONE )
				sm->subject.push_event( tmpEvent );
		}
	}
}

/**
This function initializes the window. It initializes SDL and OpenGL.

\return False if initialization failed.
*/
bool init(){
	srand( time(NULL) );
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
		return false;
	if(SDL_SetVideoMode(Constants::SCREEN_WIDTH, Constants::SCREEN_HEIGHT, Constants::SCREEN_BPP, SDL_OPENGL | SDL_RESIZABLE) == NULL)
		return false;
	SDL_EnableUNICODE(SDL_TRUE);
	if(initGL() == false)
		return false;
	if( MusicPlayer::init() == false )
		return false;
	SDL_WM_SetCaption("Dudes vs Dudes", NULL);
	return true;
}

/**
Sets the size of the screen.

\param width = The width dimension to change the screen to.
\param height = The height dimension to change the screen to.
*/
void setView(int width, int height){
	glViewport(0.f, 0.f, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, width, height, 0.0, 1.0, -1.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Constants::screenScale.w = width;
	Constants::screenScale.h = height;

	Constants::screenScale.x = (GLfloat)width / Constants::SCREEN_WIDTH;
	Constants::screenScale.y = (GLfloat)height / Constants::SCREEN_HEIGHT;
}

/**
Initializes OpenGL.

\return False if there were errors in initialization.
*/
bool initGL(){
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK){
		printf("Error initializing glew: %s\n", glewGetErrorString(glewError));
		return false;
	}
	if( !GLEW_VERSION_2_1 ){
		printf("OpenGL Version 2.1 not supported.\n");
		return false;
	}

	setView(Constants::SCREEN_WIDTH, Constants::SCREEN_HEIGHT);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glClearColor(0.f, 0.f, 0.f, 1.f);

	GLenum error = glGetError();
	if(error != GL_NO_ERROR){
		printf("Error initializing OpenGL: %s\n", gluErrorString(error));
		return false;
	}

	ilInit();
	iluInit();
	ilClearColor(255,255,255,0);

	ILenum ilError = ilGetError();
	if(ilError != IL_NO_ERROR){
		printf("Error initializing DevIL: %s\n", iluErrorString(ilError));
		return false;
	}
	return true;
}

/**
Clean up function. Frees the screen surface and exits SDL.
*/
void cleanup(){
	MusicPlayer::cleanup();
	SDL_Quit();
}