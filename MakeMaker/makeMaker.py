import sys

CC = "g++"
FLAGS = "-Wall -w -g"
linkers="-lSDL -lSDL_mixer -lGLEW -lGL -lGLU -lIL -lILU"
ExeName="DvD"
MainFolder="src/"

dependencies = []

with open('Makefile', 'w') as f:
	f.write( 'all: {}\n'.format(ExeName) )
	
	for line in sys.stdin.readlines():

		line = '/'.join(line.split('/')[1:])

		compiled = line[:-4]
		compiled += 'o'


		#lineToWrite = "{}: {}\n\t{} {} -o {} {}".format( compiled, line, CC, FLAGS,  )
		#lineToWrite = '{}: {}\n\t{} {} -c {}\n'.format(compiled, line, CC, FLAGS, line)
		finalThing = "ObjectFolder/" + compiled.split('/')[-1]
		dependencies.append( finalThing )
		lineToWrite = '{}: {}'.format(finalThing, line)
		f.write( lineToWrite )
		lineToWrite = '\t{} {} -c {} -o {}\n\n'.format(CC, FLAGS, line[:-1], finalThing)
		f.write( lineToWrite )

	f.write( '{}: {}\n\t{} {} -o {} {} {}'.format(ExeName, ' '.join(dependencies), CC, FLAGS, ExeName, ' '.join(dependencies), linkers) )