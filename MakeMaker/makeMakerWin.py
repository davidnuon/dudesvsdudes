import sys

CC = "g++"
FLAGS = "-Wall -w -g"
linkers="-lmingw32 -lSDLmain -lSDL -lSDL_mixer -lglew32 -lopengl32 -lglu32 -lDevIL -lILU"
ExeName="DvD.exe"
MainFolder="src/"
MakeFileName="Makefile.win"
ObjectFolder='ObjectFolder/'
dependencies = []

with open(MakeFileName, 'w') as f:
	f.write( 'all: {}\n'.format(ExeName) )

	for origLine in sys.stdin.readlines():

		line = ""
		for i in xrange( 0, len(origLine) ):
			if origLine[i] == '\\':
				line += '/'
			else:
				line += origLine[i]
				
		splitted = line.split('/')
		index = 0
		for n in splitted:
			if n == MainFolder[:-1]:
				break
			index+=1

		line = '/'.join(line.split('/')[1:])

		line = '/'.join( splitted[index:] )
		line =  line

		compiled = line[:-4]
		compiled += 'o'

		finalThing = ObjectFolder + compiled.split('/')[-1]
		dependencies.append( finalThing )
		lineToWrite = '{}: {}'.format(finalThing, line)
		f.write( lineToWrite )
		lineToWrite = '\t{} {} -c {} -o {}\n\n'.format(CC, FLAGS, line[:-1], finalThing)
		f.write( lineToWrite )

	f.write( '{}: {}\n\t{} {} -o {} {} {}'.format(ExeName, ' '.join(dependencies), CC, FLAGS, ExeName, ' '.join(dependencies), linkers) )