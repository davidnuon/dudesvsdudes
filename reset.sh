#! /bin/bash

OBJDIR=ObjectFolder
SCRIPTDIR=MakeMaker
EXENAME=DvD
SCRIPT=MakeMakerScript.sh

if [ -d $OBJDIR ]; then
	rm -rf $OBJDIR;
fi

mkdir $OBJDIR

if [ -f $EXENAME ]; then
	rm DvD
fi

cd $SCRIPTDIR
sh $SCRIPT
cd ..
make -s
