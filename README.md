Collaborators:
	Shane Satterfield
	David Nuon
	David Martel
	
Description:
	Basic Game Engine, written by Shane Satterfield, to be used to make the game Caffeine Complex.

Copyright:
	All resources except the following (c) Copyright Two Davids and Shane.
	
	Music:

	001 - NICOCO - Quezac.mp3
	002 - NICOCO - Quiberon.mp3
	003 - NICOCO - Livetest.mp3
	004 - NICOCO - Golden Superstar.mp3
	005 - Mister Electric Demon - Funkastar.mp3
	006 - NICOCO - Barrette.mp3
	007 - Mister Electric Demon - Le papillon.mp3
	008 - Mister Electric Demon - Salamandre _sunlikamelo-d_.mp3
	009 - NICOCO - Strange.mp3
	010 - NICOCO - Smart view.mp3
	011 - NICOCO - Babillages.mp3	
	Track      |   License URL
	----------------------------------------------
	 001        |   http://creativecommons.org/licenses/by-nc-sa/3.0/
	 002        |   http://creativecommons.org/licenses/by-nc-sa/3.0/
	 003        |   http://creativecommons.org/licenses/by-nc-sa/3.0/
	 004        |   http://creativecommons.org/licenses/by-nc-sa/3.0/
	 005        |   http://creativecommons.org/licenses/by-nc-sa/3.0/
	 006        |   http://creativecommons.org/licenses/by-nc-sa/3.0/
	 007        |   http://creativecommons.org/licenses/by-nc-sa/3.0/
	 008        |   http://creativecommons.org/licenses/by-nc-sa/3.0/
	 009        |   http://creativecommons.org/licenses/by-nc-sa/3.0/
	 010        |   http://creativecommons.org/licenses/by-nc-sa/3.0/
	 011        |   http://creativecommons.org/licenses/by-nc-sa/3.0/


	

How-to-use:
	If you are on windows, you can run the .exe without compiling the source code. As of now, you cannot compile the source code on windows. If you are on linux, you currently have to have the dependencies. Once you have the dependencies, run the make command.

Dependecies:
	If you want to be able to compile the source code, you will need to have SDL, SDL_Mixer, OpenGL, GLEW, DevIL, and your C++ compiler of choice.

Compiling Fast:
	Run reset.sh if you are on linux. It will set up the Makefile and compile once. From there, you can just call make to compile. If you are on windows, then you will need to acquire the dependencies listed. Once you do that, you can run reset.bat to create the executable and Makefile.win. Once that is done, you can compile by simply running that makefile with the command make -f Makefile.win.

TODO::
	-Game logic.
	-Write the prototype.
